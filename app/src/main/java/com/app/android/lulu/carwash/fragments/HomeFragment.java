package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends CarWashFragmentBaseClass {

    private LinearLayout linearLayoutEmployees;
    private LinearLayout linearLayoutCustomers;
    private LinearLayout linearLayoutSupervisor;
    private LinearLayout linearLayoutSubscription;
    private LinearLayout linearLayoutJobs;
    private TextView textViewWelcomeUser;

    public HomeFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_home, container, false);

        linearLayoutEmployees                   = (LinearLayout) view.findViewById(R.id.home_linear_employee);
        linearLayoutCustomers                   = (LinearLayout) view.findViewById(R.id.home_linear_customers);
        linearLayoutJobs                        = (LinearLayout) view.findViewById(R.id.home_linear_jobs);
        linearLayoutSupervisor                  = (LinearLayout) view.findViewById(R.id.linearLayout_home_supervisor);
        linearLayoutSubscription                = (LinearLayout) view.findViewById(R.id.home_linear_subscription);
        textViewWelcomeUser                     = (TextView) view.findViewById(R.id.textView_home_user_name_welcome);

        return view;
    }

    @Override
    public void initListeners() {
        linearLayoutCustomers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCustomerListFragment();
            }
        });

        linearLayoutEmployees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEmployeeListFragment();
            }
        });

        linearLayoutSupervisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPlotListFragment();
            }
        });

        linearLayoutSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSubscriptionFragment();
            }
        });

        linearLayoutJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToJobsFragment();
            }
        });
    }

    @Override
    public void initData() {
        AppPrefs appPrefs                       = new AppPrefs(getCurrentContext());
        String fullName                         = appPrefs.getFullName();
        String welcomeString                    = "Welcome, " + fullName;
        textViewWelcomeUser.setText(welcomeString);

        linearLayoutJobs.setVisibility(View.VISIBLE);
    }

    @Override
    public void toDoOnDetach() {

    }

    private void goToEmployeeListFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("superadmin_employee_list");//superadmin_employee_list//employee_list
        }
    }

    private void goToSubscriptionFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("supervisor_subscriptions");//jobs//employee_jobs//supervisor_jobs
        }
    }

    private void goToPlotListFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("plot_list");//jobs//employee_jobs//supervisor_jobs
        }
    }

    private void goToCustomerListFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("customer_list");//superadmin_employee_list//employee_list
        }
    }

    private void goToJobsFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("supervisor_jobs");
        }
    }

}
