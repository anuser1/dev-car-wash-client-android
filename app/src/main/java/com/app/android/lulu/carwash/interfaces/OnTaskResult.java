package com.app.android.lulu.carwash.interfaces;

public interface OnTaskResult {
    void onSuccess(Object o);
    void onError(Object o);
}
