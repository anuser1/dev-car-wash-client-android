package com.app.android.lulu.carwash.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Customer {
    public static final String PARKING_LOT_DELIMITER = "rr!@#rr";

    private String name;
    private String parkingLot;
    private Plot plotDetails;
    private String phone;
    private String email;
    private String subscription;
    private Vehicle vehicle;
    private int id;
    private String flatNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Plot getPlotDetails() {
        return plotDetails;
    }

    public void setPlotDetails(Plot plot) {
        this.plotDetails = plot;
    }

    public String getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    @Override
    public String toString() {
        JSONObject object                       = new JSONObject();
        try {
            object.put("name", getName());
            object.put("emailAddress", getEmail());
            object.put("phoneNumber", getPhone());
            object.put("parkingLotNumber", getParkingLot());
            object.put("plotId", getPlotDetails().getId());
            object.put("plotName", getPlotDetails().getName());

            String parkingLot                   = getParkingLot();
            parkingLot                          += " " + PARKING_LOT_DELIMITER + " ";
            if (getFlatNumber() != null) {
                parkingLot                      += getFlatNumber();
            }
            object.put("parkingLotNumber", parkingLot);

            JSONObject jsonObjectVehicle        = new JSONObject();
            if (getVehicle() != null) {
                jsonObjectVehicle.put("make", getVehicle().getMake());
                jsonObjectVehicle.put("model", getVehicle().getModel());
                jsonObjectVehicle.put("bodyStyle", getVehicle().getType());
                if (getVehicle().getCustomerVehicleId() != 0)
                    jsonObjectVehicle.put("customerVehicleId", getVehicle().getCustomerVehicleId());
            }

            object.put("vehicleInformation", jsonObjectVehicle);

            if (getId() != 0) {
                object.put("id", getId());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }
}
