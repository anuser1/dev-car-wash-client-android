package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.model.Plot;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePlotTask extends CarwashAsyncTaskBaseClass {

    private Plot plot;

    public UpdatePlotTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        plot                                    = (Plot) o[0];
    }

    @Override
    public Object tasksInBackground() {
        ServiceController controller            = new ServiceController();
        return controller.updatePlot(getContext(), plot);
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        boolean isSuccess                       = o != null && o instanceof Plot;
        if (!isSuccess) {
            try {
                JSONObject errorResponseJson    = AppServiceErrorHelper.getJsonObjectError();
                if (errorResponseJson != null) {
                    JSONObject errorJsonObject  = errorResponseJson.getJSONObject("error");
                    String error2String         = errorResponseJson.getString("message");

                    if (errorJsonObject != null && errorJsonObject.has("message")) {
                        String errorMessage     = errorJsonObject.getString("message");
                        AppServiceErrorHelper.setErrorString(errorMessage);
                    } else if (error2String != null) {
                        AppServiceErrorHelper.setErrorString(error2String);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
