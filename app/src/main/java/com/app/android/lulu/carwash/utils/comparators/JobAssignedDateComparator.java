package com.app.android.lulu.carwash.utils.comparators;

import com.app.android.lulu.carwash.model.Job;

import java.util.Comparator;

public class JobAssignedDateComparator implements Comparator<Job> {
    @Override
    public int compare(Job o1, Job o2) {
        return o1.getAssignedDate().compareTo(o2.getAssignedDate());
    }
}
