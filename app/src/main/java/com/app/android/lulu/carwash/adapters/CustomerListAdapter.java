package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.interfaces.OnElementClick;
import com.app.android.lulu.carwash.model.Customer;

import java.util.ArrayList;
import java.util.Collections;


public class CustomerListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Customer> customerArrayList;
    private ArrayList<Customer> oCustomerArrayList;
    private ArrayList<Boolean> customerDeleteArrayList;

    private OnElementClick onDeleteClick;

    private LayoutInflater inflater;

    public CustomerListAdapter(Context context, ArrayList<Customer> customerArrayList) {
        this.customerArrayList                  = customerArrayList;
        oCustomerArrayList                      = new ArrayList<>();
        oCustomerArrayList.addAll(customerArrayList);
        inflater                                = LayoutInflater.from(context);

        customerDeleteArrayList                 = new ArrayList<>(customerArrayList.size());
        customerDeleteArrayList.addAll(Collections.nCopies(customerArrayList.size(), false));
        //Collections.fill(customerDeleteArrayList, false);
    }

    public void showDeleteItemButton(int index) {
        boolean b                               = customerDeleteArrayList.get(index);
        customerDeleteArrayList.set(index, !b);

        notifyDataSetChanged();
    }

    public void setOnDeleteClick(OnElementClick onDeleteClick) {
        this.onDeleteClick                      = onDeleteClick;
    }

    @Override
    public int getCount() {
        return customerArrayList != null ? customerArrayList.size() : 0;
    }

    @Override
    public Customer getItem(int position) {
        return (customerArrayList != null && customerArrayList.size() > position)? customerArrayList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_customer, null);
            holder.textViewPerson               = (TextView) convertView.findViewById(R.id.item_customer_textview_name);
            holder.buttonDelete                 = (Button) convertView.findViewById(R.id.item_customer_button_delete);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        Customer customer                       = customerArrayList.get(position);
        holder.textViewPerson.setText(customer.getName());
        holder.textViewPerson.setTextColor(0xff000000);

        if (!customerDeleteArrayList.get(position))
            holder.buttonDelete.setVisibility(View.INVISIBLE);
        else
            holder.buttonDelete.setVisibility(View.VISIBLE);

        holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onDeleteClick != null) {
                    onDeleteClick.onClick(position);
                }
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Customer> filteredCust= new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredCust.addAll(oCustomerArrayList);
                } else {
                    for (int i = 0; i < oCustomerArrayList.size(); i++) {
                        Customer customer       = oCustomerArrayList.get(i);
                        String model            = customer.getName();
                        if (model.toLowerCase().contains(constraint.toString())) {
                            filteredCust.add(customer);
                        }
                    }
                }

                results.count                   = filteredCust.size();
                results.values                  = filteredCust;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                customerArrayList               = (ArrayList<Customer>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        TextView textViewPerson;
        Button buttonDelete;
    }
}
