package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class EmployeeHomeFragment extends CarWashFragmentBaseClass {

    private LinearLayout linearLayoutJobs;
    private LinearLayout linearLayoutCustomers;
    private LinearLayout linearLayoutSupervisor;
    private LinearLayout linearLayoutSubscription;
    private TextView textViewJobs;
    private TextView textViewWelcomeUser;
    private ImageView imageViewJobs;

    public EmployeeHomeFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_home, container, false);

        linearLayoutJobs                        = (LinearLayout) view.findViewById(R.id.home_linear_employee);
        linearLayoutCustomers                   = (LinearLayout) view.findViewById(R.id.home_linear_customers);
        linearLayoutSupervisor                  = (LinearLayout) view.findViewById(R.id.linearLayout_home_supervisor);
        linearLayoutSubscription                = (LinearLayout) view.findViewById(R.id.home_linear_subscription);
        textViewJobs                            = (TextView) view.findViewById(R.id.textView_home_employee);
        imageViewJobs                           = (ImageView) view.findViewById(R.id.imageView_home_employee);
        textViewWelcomeUser                     = (TextView) view.findViewById(R.id.textView_home_user_name_welcome);

        return view;
    }

    @Override
    public void initListeners() {
        linearLayoutJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToJobsFragment();
            }
        });

        linearLayoutCustomers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCustomerListFragment();
            }
        });

        linearLayoutSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEmployeeSubscriptionListFragment();
            }
        });
    }

    @Override
    public void initData() {
        linearLayoutSupervisor.setVisibility(View.GONE);

        textViewJobs.setText("Jobs");
        imageViewJobs.setImageResource(R.mipmap.ic_job);

        AppPrefs appPrefs                       = new AppPrefs(getCurrentContext());
        String fullName                         = appPrefs.getFullName();
        String welcomeString                    = "Welcome, " + fullName;
        textViewWelcomeUser.setText(welcomeString);
    }

    @Override
    public void toDoOnDetach() {

    }

    private void goToCustomerListFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("customer_list");//superadmin_employee_list//employee_list
        }
    }

    private void goToJobsFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("employee_jobs");//jobs//employee_jobs//supervisor_jobs
        }
    }

    //employee_subscription_list
    private void goToEmployeeSubscriptionListFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            //oFIL.onChangingFragment("employee_subscription_list");
        }
    }
}
