package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.model.JobStatus;

import java.util.ArrayList;


public class JobStatusSpinnerAdapter extends BaseAdapter {
    private ArrayList<JobStatus> jobStatusArrayList;

    private LayoutInflater inflater;
    private Context context;

    public JobStatusSpinnerAdapter(Context context, ArrayList<JobStatus> jobStatusArrayList) {
        this.jobStatusArrayList                 = jobStatusArrayList;
        this.context                            = context;
        inflater                                = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return jobStatusArrayList != null ? jobStatusArrayList.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return (jobStatusArrayList != null && jobStatusArrayList.get(i) != null) ? jobStatusArrayList.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_text, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_text_value);
            holder.status                       = (ImageView) convertView.findViewById(R.id.imageView_spinner_text_status);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.timeSlot.setVisibility(View.GONE);
        JobStatus jobStatus                     = jobStatusArrayList.get(position);

        if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_DONE)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_done));
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_PENDING)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_pending));
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_in_progress));
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_ASSIGNED)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_assigned));
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_UNASSIGNED)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_unassigned));
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_TODAY)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_today));
        } else {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_all));
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_drop_down, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_drop_down_value);
            holder.status                       = (ImageView) convertView.findViewById(R.id.imageView_spinner_drop_down_status);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        JobStatus jobStatus                     = jobStatusArrayList.get(position);

        if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_DONE)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_done));
            holder.timeSlot.setText(R.string.done);
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_ALL)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_all));
            holder.timeSlot.setText(R.string.all);
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_PENDING)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_pending));
            holder.timeSlot.setText(R.string.pending);
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_in_progress));
            holder.timeSlot.setText(R.string.in_progress);
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_ASSIGNED)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_assigned));
            holder.timeSlot.setText(R.string.assigned);
        } else if (jobStatus.getStatus().equals(AppConstants.Job.STATUS_TODAY)) {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_today));
            holder.timeSlot.setText(R.string.today);
        } else {
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_unassigned));
            holder.timeSlot.setText(R.string.unassigned);
        }

        holder.timeSlot.setTextColor(0xff000000);

        return convertView;
    }

    private static class ViewHolder {
        TextView timeSlot;
        ImageView status;
    }
}
