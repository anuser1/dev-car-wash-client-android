package com.app.android.lulu.carwash.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.constants.PreferenceConstants;

public class AppPrefs {

    private SharedPreferences sharedPreferences;

    public AppPrefs(Context context) {
        sharedPreferences                   = context.getSharedPreferences(
                PreferenceConstants.PREF_NAME,
                Context.MODE_PRIVATE);
    }

    public String getToken() {
        return sharedPreferences.getString(PreferenceConstants.KEY_TOKEN, "");
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_TOKEN, token);
        editor.apply();
    }

    public String getRole() {
        return sharedPreferences.getString(PreferenceConstants.KEY_ROLE, AppConstants.User.EMPLOYEE);
    }

    public void setRole(String role) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_ROLE, role);
        editor.apply();
    }

    public String getPassword() {
        return sharedPreferences.getString(PreferenceConstants.KEY_PASSWORD, "");
    }

    public void setPassword(String password) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_PASSWORD, password);
        editor.apply();
    }

    public String getUsername() {
        return sharedPreferences.getString(PreferenceConstants.KEY_USERNAME, "");
    }

    public void setUsername(String password) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_USERNAME, password);
        editor.apply();
    }

    public String getEmail() {
        return sharedPreferences.getString(PreferenceConstants.KEY_EMAIL, "");
    }

    public void setEmail(String email) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_EMAIL, email);
        editor.apply();
    }

    public String getPhone() {
        return sharedPreferences.getString(PreferenceConstants.KEY_PHONE, "");
    }

    public void setPhone(String phone) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_PHONE, phone);
        editor.apply();
    }

    public String getFullName() {
        return sharedPreferences.getString(PreferenceConstants.KEY_FULL_NAME, "");
    }

    public void setFullName(String fullName) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_FULL_NAME, fullName);
        editor.apply();
    }

    public String getEmployeeId() {
        return sharedPreferences.getString(PreferenceConstants.KEY_EMPLOYEE_ID, "");
    }

    public void setEmployeeId(String employeeId) {
        SharedPreferences.Editor editor     = sharedPreferences.edit();
        editor.putString(PreferenceConstants.KEY_EMPLOYEE_ID, employeeId);
        editor.apply();
    }
}
