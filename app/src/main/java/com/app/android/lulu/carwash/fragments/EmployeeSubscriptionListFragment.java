package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.adapters.SubscriptionListAdapter;
import com.app.android.lulu.carwash.background.GetSubscriptionListTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Employee;
import com.app.android.lulu.carwash.model.Subscription;

import java.util.ArrayList;


public class EmployeeSubscriptionListFragment extends CarWashFragmentBaseClass {

    private TextView textViewLoading;
    private TextView textViewTitle;
    private ListView listViewSubscriptions;
    private FloatingActionButton floatingActionButtonAddSubscription;
    private SearchView searchViewSubscriptions;
    private ImageView imageViewTitle;

    private SubscriptionListAdapter listAdapterSubscriptions;

    private ArrayList<Subscription> subscriptionArrayList;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        listViewSubscriptions                   = (ListView) view.findViewById(R.id.persons_list_persons);
        floatingActionButtonAddSubscription     = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);
        textViewLoading                         = (TextView) view.findViewById(R.id.textView_persons_loading);
        searchViewSubscriptions                 = (SearchView) view.findViewById(R.id.searchView_persons_search);
        textViewTitle                           =  (TextView) view.findViewById(R.id.textView_persons_title);
        imageViewTitle                          = (ImageView) view.findViewById(R.id.imageView_persons_title);

        return view;
    }

    @Override
    public void initListeners() {
        floatingActionButtonAddSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSubscriptionAddFragment();
            }
        });

        searchViewSubscriptions.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                listAdapterSubscriptions.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchViewSubscriptions.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listAdapterSubscriptions.getFilter().filter("");
                return false;
            }
        });
    }

    @Override
    public void initData() {
        getAllSubscriptionList();

        textViewLoading.setTextColor(0xff006094);
        textViewTitle.setText(R.string.subscription);
        imageViewTitle.setImageResource(R.mipmap.ic_subscribe);
    }

    @Override
    public void toDoOnDetach() {

    }

    @Override
    public void onTaskResult() {
        super.onTaskResult();

        Employee employee                       = (Employee) getData();

        ArrayList<Subscription> actual          = listAdapterSubscriptions.getActualSubscriptionList();
        if (actual != null) {
            ArrayList<Subscription> selected    = getSelectedItems(actual);
            for (Subscription item : selected) {
                item.setAssignedUserName(employee.getName());
                item.setAssignedUserId(employee.getId());
            }
        }
    }

    private void getAllSubscriptionList() {
        GetSubscriptionListTask task            = new GetSubscriptionListTask(getCurrentContext());
        task.setLoadingMessage("Getting subscription list");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList)
                    //noinspection unchecked
                    populateListViewWithServerData((ArrayList<Subscription>) o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        //noinspection unchecked
        task.execute();
    }

    private void populateListViewWithServerData(ArrayList<Subscription> subscriptions) {
        subscriptionArrayList                   = subscriptions;
        listAdapterSubscriptions                = new SubscriptionListAdapter(getCurrentContext(), subscriptionArrayList);
        listViewSubscriptions.setAdapter(listAdapterSubscriptions);
    }

    /*private void addItems() {
        setLoadingVisible(true);

        Handler handler                         = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                addItemsToList();
                onListAddingTaskSuccess();
            }
        }, 5000);
    }

    private void addItemsToList() {
        int startIndex                          = subscriptionArrayList.size();
        int endIndex                            = startIndex + 12;

        for (int i = startIndex; i < endIndex; i++) {
            Subscription subscription           = new Subscription();
            Vehicle vehicle                     = new Vehicle();
            Plot plot                           = new Plot();
            Customer customer                   = new Customer();

            customer.setName("Cust --" + (i + 1));
            customer.setParkingLot("Park lot " + i + ", Near XYZ, Dubai");

            plot.setName("Dubai central mall");
            plot.setId(i % 3);
            customer.setPlotDetails(plot);
            subscription.setPlot(plot);

            vehicle.setMake("Volvo ");
            vehicle.setModel("Model " + (i + "D"));

            customer.setVehicle(vehicle);


            customer.setVehicle(vehicle);
            subscription.setCustomer(customer);

            subscriptionArrayList.add(subscription);
        }
    }

    private void onListAddingTaskSuccess() {
        setLoadingVisible(false);
        flag_loading                            = false;
        int currentPosition                     = listViewSubscriptions.getFirstVisiblePosition();

        listAdapterSubscriptions                = new SubscriptionListAdapter(getCurrentContext(), subscriptionArrayList);
        listViewSubscriptions.setAdapter(listAdapterSubscriptions);

        listViewSubscriptions.setSelection(currentPosition);
    }*/

    private void goToSubscriptionAddFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_subscription");
        }
    }

    private ArrayList<Subscription> getSelectedItems(ArrayList<Subscription> subscriptions) {
        ArrayList<Subscription> selectedItems   = new ArrayList<>();
        for(Subscription subscription : subscriptions) {
            if (subscription.isSelected()) {
                selectedItems.add(subscription);
            }
        }

        return selectedItems;
    }
}
