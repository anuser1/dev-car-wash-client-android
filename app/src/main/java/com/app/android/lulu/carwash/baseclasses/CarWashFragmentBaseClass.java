package com.app.android.lulu.carwash.baseclasses;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;

/**
 * Created by Santhosh on 15-09-2017.
 */

public abstract class CarWashFragmentBaseClass extends Fragment {

    private Context mContext;
    private Activity mActivity;
    private Object data;

    private OnFragmentInteractionListener onFragmentInteractionListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        init();
        View rootView                       = initViews(inflater, container, savedInstanceState);
        initListeners();
        initData();
        return rootView;//super.onCreateView(inflater, container, savedInstanceState)
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setCurrentContext(mContext);
    }

    @Override
    public void onDetach() {
        toDoOnDetach();
        super.onDetach();
    }

    public void setCurrentContext(Context context) {
        mContext                            = context;
    }

    public Context getCurrentContext() {
        if(mContext != null)
            return mContext;
        else
            return getActivity().getApplication().getApplicationContext();
            //throw new NullPointerException("Context is not initialized");
    }

    public Activity getActivityParent() {
        if(mActivity == null)
            mActivity                       = getActivity();

        return mActivity;
    }

    public void setOnFragmentInteractionListener(OnFragmentInteractionListener l) {
        onFragmentInteractionListener               = l;
    }

    public OnFragmentInteractionListener getOnFragmentInteractionListener() {
        return onFragmentInteractionListener;
    }

    public void setData(Object obj) {
        data                                        = obj;
    }

    public Object getData() {
        return data;
    }

    public void onTaskResult() {

    }

    public abstract void init();
    public abstract View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
    public abstract void initListeners();
    public abstract void initData();
    public abstract void toDoOnDetach();
}