package com.app.android.lulu.carwash.interfaces;

public interface OnDrawerItemClickListener {
    void onItemClick(int position, String params);
}
