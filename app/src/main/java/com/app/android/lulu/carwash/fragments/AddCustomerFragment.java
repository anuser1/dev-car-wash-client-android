package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.VehicleTypeSpinnerAdapter;
import com.app.android.lulu.carwash.background.AddCustomerTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Plot;
import com.app.android.lulu.carwash.model.Vehicle;


public class AddCustomerFragment extends CarWashFragmentBaseClass {

    private EditText editTextName;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private EditText editTextFlatNumber;
    private EditText editTextVehicleModel;
    private EditText editTextVehicleMake;
    private EditText editTextParkingLot;
    private Spinner spinnerVehicleType;
    private TextView textViewPlotName;
    private ImageView imageViewPlotNameSelect;
    private Button buttonSave;

    private Customer customerToBeAdded;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_customer_add, container, false);

        editTextName                            = (EditText) view.findViewById(R.id.editText_customer_add_name);
        editTextPhone                           = (EditText) view.findViewById(R.id.editText_customer_add_phone);
        editTextEmail                           = (EditText) view.findViewById(R.id.editText_customer_add_email);
        editTextFlatNumber                      = (EditText) view.findViewById(R.id.editText_customer_add_flat_num);
        editTextVehicleMake                     = (EditText) view.findViewById(R.id.editText_customer_add_vehicle_make);
        editTextVehicleModel                    = (EditText) view.findViewById(R.id.editText_customer_add_vehicle_model);
        editTextParkingLot                      = (EditText) view.findViewById(R.id.editText_customer_add_parking_lot);
        spinnerVehicleType                      = (Spinner) view.findViewById(R.id.spinner_customer_add_vehicle_type);
        imageViewPlotNameSelect                 = (ImageView) view.findViewById(R.id.imageView_customer_add_plot_name);
        textViewPlotName                        = (TextView) view.findViewById(R.id.textView_customer_add_plot_name);
        buttonSave                              = (Button) view.findViewById(R.id.button_customer_add_save);

        return view;
    }

    @Override
    public void initListeners() {
        imageViewPlotNameSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPlotDetails();
            }
        });

        textViewPlotName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPlotDetails();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
    }

    @Override
    public void initData() {
        setVehicleTypeSpinnerAdapter();

        customerToBeAdded                       = (Customer) getData();

        if (customerToBeAdded == null) {
            customerToBeAdded                   = new Customer();
        }
        editTextName.setText(customerToBeAdded.getName());
        editTextPhone.setText(customerToBeAdded.getPhone());
        editTextEmail.setText(customerToBeAdded.getEmail());
        editTextFlatNumber.setText(customerToBeAdded.getFlatNumber());

        Object dataObject                   = ((MainActivity) getActivity()).getObjectDataToPassBetweenFragments();
        if (dataObject != null && dataObject instanceof Plot) {
            customerToBeAdded.setPlotDetails((Plot) dataObject);
            ((MainActivity) getActivity()).setObjectDataToPassBetweenFragments(null);
        }

        Plot plot                           = customerToBeAdded.getPlotDetails();
        if (plot != null)
            textViewPlotName.setText(plot.getName());

        editTextParkingLot.setText(customerToBeAdded.getParkingLot());

        Vehicle vehicle                     = customerToBeAdded.getVehicle();
        if (vehicle != null) {
            editTextVehicleModel.setText(vehicle.getModel());
            editTextVehicleMake.setText(vehicle.getMake());
            String carName                  = vehicle.getType();// insert code here
            int index = -1;
            for (int i = 0; i < AppConstants.Vehicle.BODY_TYPES.length; i++) {
                if (AppConstants.Vehicle.BODY_TYPES[i].equals(carName)) {
                    index                   = i;
                    break;
                }
            }

            onVehicleTypeSpinnerItemSelected(index);
        } else {
            onVehicleTypeSpinnerItemSelected(0);
        }

    }

    @Override
    public void toDoOnDetach() {

    }

    private void setVehicleTypeSpinnerAdapter() {
        VehicleTypeSpinnerAdapter spinnerAdapter= new VehicleTypeSpinnerAdapter(getCurrentContext());
        spinnerVehicleType.setAdapter(spinnerAdapter);
    }

    private void onVehicleTypeSpinnerItemSelected(int position) {
        Object tag                              = spinnerVehicleType.getTag();
        int taggedPosition                      = tag != null ? (int) tag : -1;
        if(taggedPosition != position) {
            spinnerVehicleType.setSelection(position);
            spinnerVehicleType.setTag(position);
        }
    }

    private void getPlotDetails() {
        if (customerToBeAdded == null) {
            customerToBeAdded                    = new Customer();
        }
        customerToBeAdded.setEmail(editTextEmail.getText().toString());
        customerToBeAdded.setName(editTextName.getText().toString());
        customerToBeAdded.setPhone(editTextPhone.getText().toString());
        customerToBeAdded.setFlatNumber(editTextFlatNumber.getText().toString());
        customerToBeAdded.setParkingLot(editTextParkingLot.getText().toString());
        Vehicle vehicle                         = customerToBeAdded.getVehicle();
        if (vehicle == null) {
            vehicle                             = new Vehicle();
        }

        vehicle.setModel(editTextVehicleModel.getText().toString());
        vehicle.setMake(editTextVehicleMake.getText().toString());
        vehicle.setType((String) spinnerVehicleType.getSelectedItem());

        customerToBeAdded.setVehicle(vehicle);

        ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(AppConstants.DataPass.KEY_ADD_CUSTOMER_PLOT);
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"get_plot_for_user", customerToBeAdded};
            oFIL.onFragmentInteraction(data);
        }
    }

    private void onSaveButtonClick() {
        String customerName                     = editTextName.getText().toString();
        String customerEmail                    = editTextEmail.getText().toString();
        String customerPhone                    = editTextPhone.getText().toString();
        String vehicleMake                      = editTextVehicleMake.getText().toString();
        String vehicleModel                     = editTextVehicleModel.getText().toString();
        String parkingLot                       = editTextParkingLot.getText().toString();
        String flatNum                          = editTextFlatNumber.getText().toString();
        int selectedPos                         = (int) spinnerVehicleType.getTag();
        String vehicleType                      = "";
        if (selectedPos != -1) {
            vehicleType                         = AppConstants.Vehicle.BODY_TYPES[selectedPos];
        }

        if (customerToBeAdded == null) {

        } else if (TextUtils.isEmpty(customerName)) {
            editTextName.setError("Enter name");
        } else if (TextUtils.isEmpty(customerPhone)) {
            editTextPhone.setError("Enter phone number");
        } else if (TextUtils.isEmpty(flatNum)) {
            editTextFlatNumber.setError("Enter flat number");
        } else if (TextUtils.isEmpty(vehicleMake)) {
            editTextVehicleMake.setError("Enter vehicle");
        } else if (TextUtils.isEmpty(vehicleModel)) {
            editTextVehicleModel.setError("Enter vehicle model");
        } else if (TextUtils.isEmpty(customerPhone)) {
            editTextPhone.setError("Enter phone number");
        } else if (TextUtils.isEmpty(parkingLot)) {
            editTextParkingLot.setError("Enter parking lot");
        } else if(customerToBeAdded.getPlotDetails() == null) {
            Toast.makeText(getCurrentContext(), "Select plot of customer", Toast.LENGTH_SHORT).show();
        } else if (selectedPos == -1) {

        } else {
            customerToBeAdded.setName(customerName);
            customerToBeAdded.setPhone(customerPhone);
            customerToBeAdded.setEmail(customerEmail);
            customerToBeAdded.setFlatNumber(flatNum);
            customerToBeAdded.setParkingLot(parkingLot);

            Vehicle vehicle                     = new Vehicle();
            vehicle.setType(vehicleType);
            vehicle.setMake(vehicleMake);
            vehicle.setModel(vehicleModel);

            customerToBeAdded.setVehicle(vehicle);

            addCustomer();
        }
    }

    private void addCustomer() {
        AddCustomerTask task                    = new AddCustomerTask(getCurrentContext(), customerToBeAdded);
        task.setLoadingMessage("Saving details...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                showSuccessMessage();
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                                                        "Added",
                                                        "Customer have been added successfully.");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess();
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
