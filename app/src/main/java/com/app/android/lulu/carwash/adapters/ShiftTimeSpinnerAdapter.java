package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.model.ShiftTime;

import java.util.ArrayList;

/**
 * Created by U47736 on 9/22/2017.
 */

public class ShiftTimeSpinnerAdapter extends BaseAdapter {

    private ArrayList<ShiftTime> shiftTimeArrayList;

    private LayoutInflater inflater;

    public ShiftTimeSpinnerAdapter(Context context, ArrayList<ShiftTime> shiftTimeArrayList) {
        this.shiftTimeArrayList                 = shiftTimeArrayList;
        inflater                                = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return shiftTimeArrayList != null ? shiftTimeArrayList.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return (shiftTimeArrayList != null && shiftTimeArrayList.get(i) != null) ? shiftTimeArrayList.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_text, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_text_value);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.timeSlot.setText(shiftTimeArrayList.get(position).getPeriod());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_drop_down, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_drop_down_value);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.timeSlot.setText(shiftTimeArrayList.get(position).getPeriod());
        holder.timeSlot.setTextColor(0xff000000);

        return convertView;
    }

    private static class ViewHolder {
        TextView timeSlot;
    }
}
