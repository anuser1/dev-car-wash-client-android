package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.model.EmployeeJob;

import org.json.JSONException;
import org.json.JSONObject;

public class AddEmployeeJobTakenTask extends CarwashAsyncTaskBaseClass {

    private EmployeeJob employeeJob;

    public AddEmployeeJobTakenTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        employeeJob                             = (EmployeeJob) o[0];
    }

    @Override
    public Object tasksInBackground() {
        ServiceController controller            = new ServiceController();
        return controller.addEmployeeJobTaken(getContext(), employeeJob);
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        boolean isSuccess                       = o != null && o instanceof EmployeeJob;
        if (!isSuccess) {
            try {
                JSONObject errorResponseJson    = AppServiceErrorHelper.getJsonObjectError();
                if (errorResponseJson != null) {
                    JSONObject errorJsonObject  = errorResponseJson.getJSONObject("error");

                    if (errorJsonObject != null && errorJsonObject.has("message")) {
                        String errorMessage     = errorJsonObject.getString("message");
                        AppServiceErrorHelper.setErrorString(errorMessage);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
