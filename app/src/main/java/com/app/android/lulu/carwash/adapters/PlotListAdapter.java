package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.model.Plot;

import java.util.ArrayList;


public class PlotListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Plot> plotArrayList;
    private ArrayList<Plot> oPlotArrayList;

    private LayoutInflater inflater;

    public PlotListAdapter(Context context, ArrayList<Plot> plotArrayList) {
        this.plotArrayList                      = plotArrayList;
        inflater                                = LayoutInflater.from(context);

        oPlotArrayList                          = new ArrayList<>();
        oPlotArrayList.addAll(plotArrayList);
    }

    @Override
    public int getCount() {
        return plotArrayList != null ? plotArrayList.size() : 0;
    }

    @Override
    public Plot getItem(int position) {
        return (plotArrayList != null && plotArrayList.size() > position)? plotArrayList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_person, null);
            holder.textViewPlot                 = (TextView) convertView.findViewById(R.id.item_person_textview_name);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        Plot plot                               = plotArrayList.get(position);
        holder.textViewPlot.setText(plot.getName());
        holder.textViewPlot.setTextColor(0xff676767);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Plot> filteredPlots   = new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredPlots.addAll(oPlotArrayList);
                } else {
                    for (int i = 0; i < oPlotArrayList.size(); i++) {
                        Plot plot               = oPlotArrayList.get(i);
                        String model            = plot.getName();
                        if (model.toLowerCase().contains(constraint.toString())) {
                            filteredPlots.add(plot);
                        }
                    }
                }

                results.count                   = filteredPlots.size();
                results.values                  = filteredPlots;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                plotArrayList                   = (ArrayList<Plot>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        TextView textViewPlot;
    }
}
