package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.PlotListAdapter;
import com.app.android.lulu.carwash.background.GetPlotListTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Plot;

import java.util.ArrayList;


public class PlotListFragment extends CarWashFragmentBaseClass {

    private TextView textViewTitle;
    private ImageView imageViewTitle;
    private FloatingActionButton floatingActionButtonAddPlot;
    private ListView listViewPlots;

    private ArrayList<Plot> plotArrayList;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        textViewTitle                           = (TextView) view.findViewById(R.id.textView_persons_title);
        imageViewTitle                          = (ImageView) view.findViewById(R.id.imageView_persons_title);
        listViewPlots                           = (ListView) view.findViewById(R.id.persons_list_persons);
        floatingActionButtonAddPlot             = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);

        return view;
    }

    @Override
    public void initListeners() {
        floatingActionButtonAddPlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddPlotFragment();
            }
        });

        listViewPlots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                checkIfDataIsToBeReturned(position);
            }
        });
    }

    @Override
    public void initData() {
        textViewTitle.setText("Plots");

        imageViewTitle.setImageResource(R.mipmap.ic_plot);

        getPlots();

        AppPrefs appPrefs                   = new AppPrefs(getCurrentContext());
        if (appPrefs.getRole().equals(AppConstants.User.SUPERVISOR)
                || appPrefs.getRole().equals(AppConstants.User.SUPER_ADMIN)) {
            floatingActionButtonAddPlot.setVisibility(View.VISIBLE);
        } else {
            floatingActionButtonAddPlot.setVisibility(View.GONE);
        }
    }

    @Override
    public void toDoOnDetach() {

    }

    private void populateListView(ArrayList<Plot> plots) {
        plotArrayList                       = plots;
        PlotListAdapter adapter                 = new PlotListAdapter(getCurrentContext(), plots);
        listViewPlots.setAdapter(adapter);
    }

    private void getPlots() {
        GetPlotListTask task                    = new GetPlotListTask(getCurrentContext());
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList) {
                    //noinspection unchecked
                    populateListView((ArrayList<Plot>) o);

                }
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        task.setLoadingMessage("Getting plots...");
        //noinspection unchecked
        task.execute();
    }

    private void goToAddPlotFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_plot");
        }
    }

    private void checkIfDataIsToBeReturned(int position) {
        Object dataToPassBetweenFragments       = ((MainActivity) getActivity()).getObjectDataToPassBetweenFragments();

        if (dataToPassBetweenFragments != null
                && dataToPassBetweenFragments instanceof String
                && (dataToPassBetweenFragments.equals(AppConstants.DataPass.KEY_EDIT_CUSTOMER_PLOT)
                    || dataToPassBetweenFragments.equals(AppConstants.DataPass.KEY_ADD_CUSTOMER_PLOT)
                    || dataToPassBetweenFragments.equals(AppConstants.DataPass.KEY_ADD_SUBSCRIPTION_PLOT))) {
            Plot plot                               = plotArrayList.get(position);
            ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(plot);
            getActivity().onBackPressed();
        } else {
            goToViewPlotFragment(position);
        }
    }

    private void goToViewPlotFragment(int position) {
        Plot plot                               = plotArrayList.get(position);
        Object data                             = new Object[] {"view_plot", plot};
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onFragmentInteraction(data);
        }
    }
}
