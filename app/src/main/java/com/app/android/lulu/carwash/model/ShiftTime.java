package com.app.android.lulu.carwash.model;

/**
 * Created by U47736 on 9/22/2017.
 */

public class ShiftTime {
    private String period;
    private int id;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
