package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.adapters.ShiftTimeSpinnerAdapter;
import com.app.android.lulu.carwash.background.AddEmployeeTask;
import com.app.android.lulu.carwash.background.AddSupervisorTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.controllers.AppController;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Employee;
import com.app.android.lulu.carwash.model.ShiftTime;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddEmployeeFragment extends CarWashFragmentBaseClass {

    private EditText editTextName;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private TextView textViewEmpId;
    private Spinner spinnerTimeslot;
    private Button buttonSave;
    private LinearLayout linearLayoutIsSupervisorContainer;
    private CheckBox checkBoxIsSupervisor;

    private Employee employeeToBeAdded;
    private ShiftTimeSpinnerAdapter spinnerAdapter;

    private ArrayList<ShiftTime> shiftTimeArrayList;

    public AddEmployeeFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_person_details, container, false);

        editTextName                            = (EditText) view.findViewById(R.id.editText_emp_details_emp_name);
        editTextEmail                           = (EditText) view.findViewById(R.id.editText_emp_details_emp_email);
        editTextPhone                           = (EditText) view.findViewById(R.id.editText_emp_details_emp_phone);
        textViewEmpId                           = (TextView) view.findViewById(R.id.textView_emp_details_emp_id);
        spinnerTimeslot                         = (Spinner) view.findViewById(R.id.spinner_emp_details_time_slot);
        buttonSave                              = (Button) view.findViewById(R.id.button_person_details_save);
        linearLayoutIsSupervisorContainer       = (LinearLayout) view.findViewById(R.id.linearLayout_person_details_is_supervisor_container);
        checkBoxIsSupervisor                    = (CheckBox) view.findViewById(R.id.checkBox_person_details_is_supervisor);

        return view;
    }

    @Override
    public void initListeners() {
        spinnerTimeslot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onTimeSlotItemSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
    }

    @Override
    public void initData() {
        getAllTimeSlotsAndPopulate();

        AppPrefs appPrefs                       = new AppPrefs(getCurrentContext());
        if (appPrefs.getRole().equals(AppConstants.User.SUPER_ADMIN)) {
            linearLayoutIsSupervisorContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void toDoOnDetach() {

    }

    private void getAllTimeSlotsAndPopulate() {
        int length                              = AppConstants.User.TIME_SLOTS.length;
        shiftTimeArrayList                      = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            ShiftTime shiftTime                 = new ShiftTime();
            int timeSlotId                      = i + 1;
            shiftTime.setId(timeSlotId);
            shiftTime.setPeriod(AppConstants.User.getTimeSlot(timeSlotId));

            shiftTimeArrayList.add(shiftTime);
        }

        spinnerAdapter                          = new ShiftTimeSpinnerAdapter(getCurrentContext(), shiftTimeArrayList);
        spinnerTimeslot.setAdapter(spinnerAdapter);
    }

    private void onTimeSlotItemSelected(int position) {
        int currentSelection                    = spinnerTimeslot.getTag() != null ? (int) spinnerTimeslot.getTag() : -1;
        if (currentSelection != position) {
            spinnerTimeslot.setSelection(position);
            spinnerTimeslot.setTag(position);
        }
    }

    private void onSaveButtonClick() {
        String name                             = editTextName.getText().toString();
        String phone                            = editTextPhone.getText().toString();
        String email                            = editTextEmail.getText().toString();

        if (TextUtils.isEmpty(name)) {
            editTextName.setError("Enter name");
        } else if (TextUtils.isEmpty(phone)) {
            editTextPhone.setError("Enter phone number");
        } else if (TextUtils.isEmpty(email)) {
            editTextEmail.setError("Enter email");
        } else if (!AppController.isEmailValid(email)) {
            editTextEmail.setError("Not valid");
            Toast.makeText(getCurrentContext(), "Not a valid email address", Toast.LENGTH_SHORT).show();
        } else {
            employeeToBeAdded                   = new Employee();

            employeeToBeAdded.setEmailAddress(email);
            employeeToBeAdded.setPhoneNumber(phone);
            employeeToBeAdded.setTimeSlotId(((Integer) spinnerTimeslot.getTag()) + 1);

            String fullName                     = name.trim();
            String[] splitNames                 = fullName.split("\\s+");

            if (splitNames.length > 1) {
                String surName                  = "";
                for (int i = 1; i < splitNames.length; i++) {
                    surName                     += splitNames[i] + " ";
                }

                surName                         = surName.trim();
                employeeToBeAdded.setSurName(surName);
                employeeToBeAdded.setName(splitNames[0]);
            } else {
                employeeToBeAdded.setName(fullName);
            }

            boolean isSupervisor                = checkBoxIsSupervisor.isChecked();
            employeeToBeAdded.setSupervisor(isSupervisor);

            if (isSupervisor) {
                addSupervisorDetails(employeeToBeAdded);
            } else {
                addEmployeeDetails(employeeToBeAdded);
            }
        }

    }

    private void addEmployeeDetails(Employee employee) {
        AddEmployeeTask task                 = new AddEmployeeTask(getCurrentContext(), employee);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof Employee)
                    showSuccessMessage((Employee)o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not add employee. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        task.setLoadingMessage("Adding...");
        //noinspection unchecked
        task.execute();
    }

    private void addSupervisorDetails(Employee employee) {
        AddSupervisorTask task                 = new AddSupervisorTask(getCurrentContext(), employee);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof Employee)
                    showSuccessMessage((Employee)o);
            }

            @Override
            public void onError(Object o) {

            }
        });
        task.setLoadingMessage("Adding...");
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage(Employee employee) {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                                                        "Added",
                                                        "Employee details have " +
                                                                "been added successfully.\n" +
                                                                "Username is " + employee.getEmpId() +
                                                                "\nDefault login password is 123456.\n" +
                                                                "Employee can change this in the " +
                                                                "profile page");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess();
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
