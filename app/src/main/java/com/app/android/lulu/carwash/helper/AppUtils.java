package com.app.android.lulu.carwash.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class AppUtils {

    public static File compressBitmapAndSaveToFile(File inputFile, Context context){
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o             = new BitmapFactory.Options();
            o.inJustDecodeBounds                = true;
            o.inSampleSize                      = 4;
            // factor of downsizing the image

            FileInputStream inputStream         = new FileInputStream(inputFile);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE             = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale                           = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale                           *= 2;
            }

            BitmapFactory.Options o2            = new BitmapFactory.Options();
            o2.inSampleSize                     = scale;
            inputStream                         = new FileInputStream(inputFile);

            Bitmap selectedBitmap               = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            String filesImageDir                = context.getFilesDir().getAbsolutePath();
            File mFolder                        = new File(filesImageDir + File.pathSeparator + "images");
            if (!mFolder.exists()) {
                //noinspection ResultOfMethodCallIgnored
                mFolder.mkdir();
            }

            String imageFileName                = "carServiceImage.png";
            File outputFile                     = new File(mFolder.getAbsolutePath(), imageFileName);

            // here i override the original image file
            //boolean haFileCreated               = file.createNewFile();
            FileOutputStream outputStream       = new FileOutputStream(outputFile);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            outputStream.flush();
            outputStream.close();

            return outputFile;
        } catch (Exception e) {
            return null;
        }
    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath){
        int rotate                              = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile                      = new File(imagePath);

            ExifInterface exif                  = new ExifInterface(imageFile.getAbsolutePath());
            int orientation                     = exif.getAttributeInt(
                                                        ExifInterface.TAG_ORIENTATION,
                                                        ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate                      = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate                      = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate                      = 90;
                    break;
            }

            Log.i("skt", "[RotateImage] Exif orientation: " + orientation);
            Log.i("skt", "[RotateImage] Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }
}
