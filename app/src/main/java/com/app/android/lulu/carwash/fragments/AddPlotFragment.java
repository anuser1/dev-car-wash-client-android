package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.background.AddPlotTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Plot;


public class AddPlotFragment extends CarWashFragmentBaseClass {

    private EditText editTextName;
    private EditText editTextDescription;
    private Button buttonSave;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_plot_add, container, false);

        editTextDescription                     = (EditText) view.findViewById(R.id.editText_plot_add_description);
        editTextName                            = (EditText) view.findViewById(R.id.editText_plot_add_name);
        buttonSave                              = (Button) view.findViewById(R.id.button_plot_add_save);

        return view;
    }

    @Override
    public void initListeners() {
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
    }

    @Override
    public void initData() {

    }

    @Override
    public void toDoOnDetach() {

    }

    private void onSaveButtonClick() {
        String plotName                         = editTextName.getText().toString();
        String plotDesc                         = editTextDescription.getText().toString();

        if (TextUtils.isEmpty(plotName)) {
            editTextName.setError("Mandatory");
        } else {
            Plot plot                           = new Plot();
            plot.setDescription(plotDesc);
            plot.setName(plotName);

            savePlot(plot);
        }
    }

    private void savePlot(Plot plot) {
        AddPlotTask task                        = new AddPlotTask(getCurrentContext(), plot);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                showSuccessMessage();
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        task.setLoadingMessage("Saving plot details...");
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Added",
                "Plot added successfully.");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess();
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
