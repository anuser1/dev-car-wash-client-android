package com.app.android.lulu.carwash.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoggedInUser {
    /*"result": {
        "token": "Cz5kw3gJtgO4k8JnGguaotAeoow98yFC19Pin4m9Y2Snrk0AqPHnfReIa7sGVYsindPg4erhl8dm0fEFboejsAvgn1kd4k4_nsWRF9kWDkWaBcvhp111X_XxJj8BWao_buwgi5J-2bMsQHbd-NDLxlQbWiEOC5YjY5PzlIiqG05zZ5QEel_qzPRjYIWZsg7BhHXAJm0veY7FmaJwGVGDCsKWyuYHExqsUMhFq8iYDYm04pf1ccO61OJuuiACvzTxDcATO5p5nrbqcPPZYxI4AvxpBFLRfA5Xkvwici4r2sYsI_ImgdoenMGJrpj0O3J-83sim6EK8pOFJU375rUE4x8fEUfSqJF2HovJS-0DKwvGb9ZDOQFLBt7aTmGl_2nA13zO5t_Xo6iVXinaVQDPnR426LdkH0OrN14ewqazzY2vlW4RuPH6KX_YU4xEHQsNlsHx7DXUTGiGoiaN00awQO_9mluD8B75XmrFzq-EbStwjzXny6Eb2kNqAq3VjeF1MImy9mWx7vmWT11yaZF12w",
        "userInfo": {
            "userName": "spa",
            "name": "Super",
            "surname": "Admin",
            "emailAddress": "spa@defaulttenant.com",
            "isActive": true,
            "fullName": "Super Admin",
            "lastLoginTime": "2017-09-21T22:38:23.6757034+05:30",
            "creationTime": "2017-09-05T11:38:52",
            "roles": [
                "Superadmin"
            ],
            "permissions": [],
            "employeeId": null,
            "timeSlotId": 0,
            "id": 5
        }
    }*/

    private String token;
    private String userName;
    private String name;
    private String surName;
    private String email;
    private boolean isActive;
    private String fullName;
    private String lastLoginTime;
    private String creationTime;
    private String[] role;
    private String[] permissions;
    private String employeeId;
    private String phoneNumber;
    private int timeSlotId;
    private long id;

    public LoggedInUser(String response) {
        try {
            JSONObject object                   = new JSONObject(response);
            JSONObject resultJsonObject         = object.getJSONObject("result");
            JSONObject userInfoJsonObject       = resultJsonObject.getJSONObject("userInfo");

            setToken(resultJsonObject.getString("token"));
            setUserName(userInfoJsonObject.getString("userName"));
            setName(userInfoJsonObject.getString("name"));
            setSurName(userInfoJsonObject.getString("surname"));
            setEmail(userInfoJsonObject.getString("emailAddress"));
            setActive(userInfoJsonObject.getBoolean("isActive"));
            setFullName(userInfoJsonObject.getString("fullName"));
            setLastLoginTime(userInfoJsonObject.getString("lastLoginTime"));
            setCreationTime(userInfoJsonObject.getString("creationTime"));

            JSONArray arrRoles                  = userInfoJsonObject.getJSONArray("roles");
            String[] rolesArr                   = new String[arrRoles.length()];
            for(int i = 0; i < arrRoles.length(); i++)
                rolesArr[i] = arrRoles.getString(i);

            setRole(rolesArr);

            JSONArray arrPermissions            = userInfoJsonObject.getJSONArray("permissions");
            String[] permissionsArr             = new String[arrPermissions.length()];
            for(int i = 0; i < arrPermissions.length(); i++)
                permissionsArr[i]               = arrPermissions.getString(i);

            setPermissions(permissionsArr);

            setEmployeeId(userInfoJsonObject.getString("employeeId"));
            setTimeSlotId(userInfoJsonObject.getInt("timeSlotId"));
            setId(userInfoJsonObject.getLong("id"));
            setPhoneNumber(userInfoJsonObject.getString("phoneNumber"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public LoggedInUser() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String[] getRole() {
        return role;
    }

    public void setRole(String[] role) {
        this.role = role;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public void setPermissions(String[] permissions) {
        this.permissions = permissions;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
