package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnTaskProgress;
import com.app.android.lulu.carwash.model.Subscription;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignEmployeeSubscriptionTask extends CarwashAsyncTaskBaseClass {

    private ArrayList<Subscription> subscriptionArrayList;

    private OnTaskProgress onTaskProgress;

    public AssignEmployeeSubscriptionTask(Context context, Object... objects) {
        super(context, objects);
    }

    public void setOnTaskProgress(OnTaskProgress onTaskProgress) {
        this.onTaskProgress                         = onTaskProgress;
    }

    @Override
    public void initTask(Context context, Object... o) {
        if (o[0] != null && o[0] instanceof ArrayList && ((ArrayList<?>)o[0]).get(0) instanceof  Subscription)
            //noinspection unchecked
            subscriptionArrayList                   = (ArrayList<Subscription>) o[0];
    }

    @Override
    public ArrayList<Subscription> tasksInBackground() {
        ServiceController controller            = new ServiceController();
        ArrayList<Subscription> result          = null;
        int len                                 = subscriptionArrayList.size();

        for (int i = 0; i < len; i++) {
            Subscription res                    = controller.assignEmployeeToSubscription(
                                                        getContext(), subscriptionArrayList.get(i));
            //noinspection unchecked
            publishProgress(res, i);

            if (result == null)
                result                          = new ArrayList<>();
            result.add(res);
        }
        return result;
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        int completedPos                        = (int) values[1];
        int progress                            = completedPos / subscriptionArrayList.size();

        if (values[1] == null) {
            onTaskProgress.onTriedTaskFailure(subscriptionArrayList.get(completedPos));
        } else {
            onTaskProgress.onTriedTaskSuccess(values[0]);
        }
        //noinspection unchecked
        super.onProgressUpdate(progress);
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        boolean isSuccess                       = o != null && o instanceof ArrayList;
        if (!isSuccess) {
            try {
                JSONObject errorResponseJson    = AppServiceErrorHelper.getJsonObjectError();
                if (errorResponseJson != null) {
                    JSONObject errorJsonObject  = errorResponseJson.getJSONObject("error");

                    if (errorJsonObject != null && errorJsonObject.has("message")) {
                        String errorMessage     = errorJsonObject.getString("message");
                        AppServiceErrorHelper.setErrorString(errorMessage);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
