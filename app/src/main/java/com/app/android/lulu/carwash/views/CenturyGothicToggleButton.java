package com.app.android.lulu.carwash.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ToggleButton;

/**
 * Created by Santhosh on 13-09-2017.
 */

@SuppressLint("AppCompatCustomView")
public class CenturyGothicToggleButton extends ToggleButton {
    private static Typeface mTypeface;

    public CenturyGothicToggleButton(Context context) {
        super(context);

        init(context);
    }

    public CenturyGothicToggleButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public CenturyGothicToggleButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CenturyGothicToggleButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(context);
    }

    private void init(Context context) {
        if (isInEditMode()) {
            return;
        }
        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
        }
        setTypeface(mTypeface);
    }
}
