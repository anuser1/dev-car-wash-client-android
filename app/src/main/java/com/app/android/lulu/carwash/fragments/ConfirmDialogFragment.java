package com.app.android.lulu.carwash.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.app.android.lulu.carwash.interfaces.OnDialogAction;

public class ConfirmDialogFragment extends DialogFragment {

    private OnDialogAction onDialogAction;

    public ConfirmDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    public static ConfirmDialogFragment newInstance(String title, String message) {
        ConfirmDialogFragment frag                = new ConfirmDialogFragment();
        Bundle args                             = new Bundle();

        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);

        return frag;
    }

    public void setOnDialogAction(OnDialogAction onDialogAction) {
        this.onDialogAction                     = onDialogAction;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title                            = getArguments().getString("title");
        String message                          = getArguments().getString("message");
        AlertDialog.Builder alertDialogBuilder  = new AlertDialog.Builder(getActivity());

        if (title != null && title.length() > 0)
            alertDialogBuilder.setTitle(title);

        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                onDialogAction.onYes();
            }
        });
        alertDialogBuilder.setNegativeButton("No",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                onDialogAction.onNo();
            }
        });

        return alertDialogBuilder.create();
    }
}
