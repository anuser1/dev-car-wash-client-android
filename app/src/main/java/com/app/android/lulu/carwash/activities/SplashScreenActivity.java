package com.app.android.lulu.carwash.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.constants.AppConstants;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent                   = new Intent(SplashScreenActivity.this, LoginActivity.class);//LoginActivity
                intent.putExtra(AppConstants.DataPass.KEY_LOGIN_ROLE, AppConstants.User.EMPLOYEE);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
