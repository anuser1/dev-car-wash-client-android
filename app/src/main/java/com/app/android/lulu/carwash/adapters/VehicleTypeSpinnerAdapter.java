package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.constants.AppConstants;

/**
 * Created by U47736 on 9/22/2017.
 */

public class VehicleTypeSpinnerAdapter extends BaseAdapter {
    private String[] vehicleTypes;

    private LayoutInflater inflater;
    private Context context;

    public VehicleTypeSpinnerAdapter(Context context) {
        vehicleTypes                            = AppConstants.Vehicle.BODY_TYPES;
        this.context                            = context;
        inflater                                = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return vehicleTypes != null ? vehicleTypes.length : 0;
    }

    @Override
    public Object getItem(int i) {
        return vehicleTypes != null ? vehicleTypes[i] : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_text, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_text_value);

            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.timeSlot.setText(vehicleTypes[position]);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_drop_down, null);
            holder.timeSlot                     = (TextView) convertView.findViewById(R.id.textView_spinner_drop_down_value);
            holder.status                       = (ImageView) convertView.findViewById(R.id.imageView_spinner_drop_down_status);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.timeSlot.setText(vehicleTypes[position]);
        holder.timeSlot.setTextColor(0xff000000);

        return convertView;
    }

    private static class ViewHolder {
        TextView timeSlot;
        ImageView status;
    }
}
