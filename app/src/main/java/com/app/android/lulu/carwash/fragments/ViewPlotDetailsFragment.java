package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.background.UpdatePlotTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Plot;


public class ViewPlotDetailsFragment extends CarWashFragmentBaseClass {

    private TextView textViewTitle;
    private EditText editTextName;
    private EditText editTextDescription;
    private Button buttonSave;

    private Plot plotSelected;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_plot_add, container, false);

        textViewTitle                           = (TextView) view.findViewById(R.id.textView_plot_add_title);
        editTextDescription                     = (EditText) view.findViewById(R.id.editText_plot_add_description);
        editTextName                            = (EditText) view.findViewById(R.id.editText_plot_add_name);
        buttonSave                              = (Button) view.findViewById(R.id.button_plot_add_save);

        return view;
    }

    @Override
    public void initListeners() {
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
    }

    @Override
    public void initData() {
        textViewTitle.setText("Plot details");

        plotSelected                            = (Plot) getData();

        if (plotSelected != null) {
            editTextName.setText(plotSelected.getName());
            editTextDescription.setText(plotSelected.getDescription());
        }
    }

    @Override
    public void toDoOnDetach() {

    }

    private void onSaveButtonClick() {
        String plotName                         = editTextName.getText().toString();
        String plotDesc                         = editTextDescription.getText().toString();

        if (TextUtils.isEmpty(plotName)) {
            editTextName.setError("Mandatory");
        } else {
            plotSelected.setDescription(plotDesc);
            plotSelected.setName(plotName);

            saveEditedPlot(plotSelected);
        }
    }

    private void saveEditedPlot(Plot plot) {
        UpdatePlotTask task                     = new UpdatePlotTask(getCurrentContext(), plot);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                showSuccessMessage();
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        task.setLoadingMessage("Updating plot details...");
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Added",
                "Plot updated successfully.");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess();
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
