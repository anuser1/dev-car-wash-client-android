package com.app.android.lulu.carwash.interfaces;

public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Object data);
    void onChangingFragment(String tag);
}
