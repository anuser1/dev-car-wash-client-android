package com.app.android.lulu.carwash.controllers;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.fragments.AlertDialogFragment;
import com.app.android.lulu.carwash.helper.AppUtils;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.model.Job;
import com.app.android.lulu.carwash.utils.comparators.JobAssignedDateComparator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppController {

    public static HashMap<String, ArrayList<Job>> sortAndGroupJobsArrayList(ArrayList<Job> jobArrayList) {
        HashMap<String, ArrayList<Job>> hashMap = new HashMap<>();

        if (jobArrayList != null) {
            int totalArrLen                     = jobArrayList.size();

            for (int i = 0; i < totalArrLen; i++) {
                Job job                         = jobArrayList.get(i);
                String status                   = job.getStatus();

                if (!hashMap.containsKey(status)) {
                    ArrayList<Job> list         = new ArrayList<>();
                    list.add(job);

                    hashMap.put(status, list);
                } else {
                    //Add the job to existing list
                    hashMap.get(status).add(job);
                }
            }

            hashMap.put(AppConstants.Job.STATUS_ALL, jobArrayList);
            hashMap.put(AppConstants.Job.STATUS_TODAY, todayJobList(jobArrayList));
        } else {
            hashMap.put("failed", new ArrayList<Job>());
        }

        if (hashMap.size() > 0) {
            Set<String> keySet                  = hashMap.keySet();

            for (String key: keySet) {
                ArrayList<Job> list             = hashMap.get(key);
                Collections.sort(list, new JobAssignedDateComparator());
            }
        }

        return hashMap;
    }

    public static void showSuccessMessage(Context context, String title, String message, OnDialogAction onDialogAction) {
        FragmentManager fm                      = ((AppCompatActivity)context).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(title, message);
        alertDialogFragment.setOnDialogAction(onDialogAction);
        alertDialogFragment.show(fm, "fragment_alert");
    }

    public static Bitmap decodeSampledBitmapFromCamera(byte[] byteArray, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options     = new BitmapFactory.Options();
        options.inJustDecodeBounds              = true;
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        // Calculate inSampleSize
        options.inSampleSize                    = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds              = false;

        // convert byte array to Bitmap
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
    }

    public static Bitmap detectImageOrientationAndRotate(Context context, Uri uri, Bitmap bitmap, String filePath) {
        int angleOfOrientation                  = AppUtils.getCameraPhotoOrientation(context, uri, filePath);
        Log.d("skt", "angle of rotation = " + angleOfOrientation);
        if (angleOfOrientation > 0) {
            bitmap                              = rotateImage(angleOfOrientation, bitmap);
        }

        return bitmap;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes             = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path                             = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor                           = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int idx                             = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            String realPath                     = cursor.getString(idx);

            cursor.close();

            return realPath;
        } else {
            return "";
        }
    }

    public static File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir                            = Environment.getExternalStorageDirectory();
        tempDir                                 = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public static String[] getDatesBetweenRange(Date startDate, Date endDate, String[] selectedDays) {
        if (selectedDays == null || selectedDays.length == 0) {
            return null;
        }

        ArrayList<String> selectedDates         = new ArrayList<>();
        SimpleDateFormat serverDateFormat       = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());//'T'hh:mm:ss

        ArrayList<Integer> selectedWeekDays     = new ArrayList<>();
        for (String day : selectedDays) {
            switch (day) {
                case "Mon":
                    selectedWeekDays.add(Calendar.MONDAY);
                    break;
                case "Tue":
                    selectedWeekDays.add(Calendar.TUESDAY);
                    break;
                case "Wed":
                    selectedWeekDays.add(Calendar.WEDNESDAY);
                    break;
                case "Thu":
                    selectedWeekDays.add(Calendar.THURSDAY);
                    break;
                case "Fri":
                    selectedWeekDays.add(Calendar.FRIDAY);
                    break;
                case "Sat":
                    selectedWeekDays.add(Calendar.SATURDAY);
                    break;
                case "Sun":
                    selectedWeekDays.add(Calendar.SUNDAY);
                    break;
            }
        }

        int selectedWeekDaysLen                 = selectedWeekDays.size();
        Calendar calendar                       = Calendar.getInstance();
        calendar.setTime(startDate);

        Date date                               = calendar.getTime();

        while (date.before(endDate)) {
            for (int i = 0; i < selectedWeekDaysLen; i++) {
                //noinspection WrongConstant
                if (calendar.get(Calendar.DAY_OF_WEEK) == selectedWeekDays.get(i) ){
                    String dateStringToAdd      = serverDateFormat.format(date);
                    selectedDates.add(dateStringToAdd);

                    Log.d("skt", "Date = " + dateStringToAdd);
                }
            }
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            date                                = calendar.getTime();
        }

        String[] datesArr                       = new String[selectedDates.size()];
        datesArr                                = selectedDates.toArray(datesArr);

        return datesArr;
    }

    public static boolean isEmailValid(String email) {
        String expression                       = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern                         = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher                         = pattern.matcher(email);
        return matcher.matches();
    }

    public static String[] getTimeDurationBetweenDates(Date startDate,  Date endDate) {
        long duration                           = endDate.getTime() - startDate.getTime();

        long diffInSeconds                      = TimeUnit.MILLISECONDS.toSeconds(duration);
        long diffInMinutes                      = TimeUnit.MILLISECONDS.toMinutes(duration);
        long diffInHours                        = TimeUnit.MILLISECONDS.toHours(duration);
        long diffInDays                         = TimeUnit.MILLISECONDS.toDays(duration);

        diffInHours                             = diffInHours % 24;
        diffInMinutes                           = diffInMinutes % 60;
        diffInSeconds                           = diffInSeconds % 60;

        String durationString                   = "";
        if (diffInDays > 0) {
            durationString                      += diffInDays + " " + ((diffInDays > 1) ? "days " : "day ");
        }

        if (diffInHours > 0) {
            durationString                      += diffInHours + " " + ((diffInHours > 1) ? "hours " : "hour ");
        }

        if (diffInMinutes > 0) {
            durationString                      += diffInMinutes + " " + ((diffInMinutes > 1) ? "minutes " : "minute ");
        }

        if (diffInSeconds > 0) {
            durationString                      += diffInSeconds + " " + ((diffInSeconds > 1) ? "seconds " : "second ");
        }

        String formattedString                  = checkDigit(diffInHours)
                                                    + " : " + checkDigit(diffInMinutes)
                                                    + " : " + checkDigit(diffInSeconds);

        return new String[] {durationString, formattedString};
    }

    private static ArrayList<Job> todayJobList(ArrayList<Job> jobArrayList) {
        if (jobArrayList == null) {
            return null;
        }

        Calendar calendar                       = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date dateStart                          = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        Date dateEnd                            = calendar.getTime();

        ArrayList<Job> arrayListTodayJob        = new ArrayList<>();

        for (Job job: jobArrayList) {
            Date jobDate                        = job.getAssignedDate();
            if (jobDate != null) {
                if (jobDate.after(dateStart) && jobDate.before(dateEnd))
                    arrayListTodayJob.add(job);
                else if (jobDate.equals(dateStart))
                    arrayListTodayJob.add(job);
            }
        }

        return arrayListTodayJob;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height                        = options.outHeight;
        final int width                         = options.outWidth;
        int inSampleSize                        = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight                = height / 2;
            final int halfWidth                 = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize                    *= 2;
            }
        }

        return inSampleSize;
    }


    private static Bitmap rotateImage(int angle, Bitmap bitmapImageOfVehicle) {
        Matrix mat                              = new Matrix();
        mat.postRotate(angle);//===>angle to be rotated

        return Bitmap.createBitmap(bitmapImageOfVehicle, 0, 0,
                bitmapImageOfVehicle.getWidth(),
                bitmapImageOfVehicle.getHeight(),
                mat, true);
    }

    private static String checkDigit(long number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }
}
