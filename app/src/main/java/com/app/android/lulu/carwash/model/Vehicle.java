package com.app.android.lulu.carwash.model;

/**
 * Created by U47736 on 9/18/2017.
 */

public class Vehicle {
    private String make;
    private String type;
    private String model;
    private int customerVehicleId;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCustomerVehicleId() {
        return customerVehicleId;
    }

    public void setCustomerVehicleId(int customerVehicleId) {
        this.customerVehicleId = customerVehicleId;
    }
}
