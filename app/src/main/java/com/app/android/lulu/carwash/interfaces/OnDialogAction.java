package com.app.android.lulu.carwash.interfaces;


public interface OnDialogAction {
    void onOk();
    void onYes();
    void onNo();
    void onCancel();
}
