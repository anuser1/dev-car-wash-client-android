package com.app.android.lulu.carwash.model;

import com.app.android.lulu.carwash.constants.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UpdatedEmployee extends Employee{
    private String username;
    private String[] permissions;

    public UpdatedEmployee(String response) {
        try {
            JSONObject object                   = new JSONObject(response);

            JSONObject userInfo                 = object.getJSONObject("result");

            setEmailAddress(userInfo.getString("emailAddress"));
            setEmpId(userInfo.getString("employeeId"));
            setUsername(userInfo.getString("userName"));
            setName(userInfo.getString("name"));
            setSurName(userInfo.getString("surname"));
            setFullName(userInfo.getString("fullName"));
            setTimeSlotId(userInfo.getInt("timeSlotId"));
            setId(userInfo.getInt("id"));

            JSONArray roleJsonArray             = userInfo.getJSONArray("roles");
            if (roleJsonArray.toString().contains(AppConstants.User.SUPERVISOR)) {
                setSupervisor(true);
            } else {
                setSupervisor(false);
            }

            JSONArray permissionsJsonArray      = userInfo.getJSONArray("permissions");
            List<String> list                   = new ArrayList<String>();
            for(int i = 0; i < permissionsJsonArray.length(); i++){
                list.add(permissionsJsonArray.getString(i));
            }

            setPermissions(list.toArray(new String[list.size()]));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public void setPermissions(String[] permissions) {
        this.permissions = permissions;
    }
}
