package com.app.android.lulu.carwash.constants;


public class AppConstants {
    public static final class Employee {

    }
    public static final class DataPass {
        public static final String KEY_LOGIN_ROLE           = "login_role";
        public static final int TAKE_START_JOB_PICTURE      = 63;
        public static final int TAKE_STOP_JOB_PICTURE       = 66;
        public static final String KEY_EDIT_CUSTOMER_PLOT   = "from_edit_customer_to_get_plot";
        public static final String KEY_ADD_CUSTOMER_PLOT    = "from_add_customer_to_get_plot";
        public static final String KEY_ADD_SUBSCRIPTION_PLOT        = "from_add_subscription_to_get_plot";
        public static final String KEY_ADD_SUBSCRIPTION_CUSTOMER    = "from_add_subscription_to_get_customer";
        public static final String KEY_ASSIGN_SUBSCRIPTION_EMPLOYEE = "from_subscription_to_get_employee";
    }
    public static final class Job {
        public static final String STATUS_ALL           = "Complete";
        public static final String STATUS_DONE          = "Completed";//3
        public static final String STATUS_IN_PROGRESS   = "InProgress";//2
        public static final String STATUS_ASSIGNED      = "Assigned";//1
        public static final String STATUS_UNASSIGNED    = "Created";//0
        public static final String STATUS_PENDING       = "Pending";
        public static final String STATUS_TODAY         = "Today";
        /*
        Created    = 0,
        Assigned   = 1,
        InProgress = 2,
        Completed  = 3
        */
    }

    public static final class User {
        public static final String EMPLOYEE             = "Employee";
        public static final String SUPERVISOR           = "Supervisor";
        public static final String SUPER_ADMIN          = "Superadmin";

        public static final String[] TIME_SLOTS         = {"09:00AM to 12:00PM", "12:00PM to 03:00PM", "03:00PM to 06:00PM"};
        public static String getTimeSlot(int id) {
            return TIME_SLOTS[id - 1];
        }
    }

    public static final class Subscription {
        public static final String WEEKLY_1_MONTH_12    = "Weekly once for 12 months";
        public static final String WEEKLY_2_MONTH_12    = "Weekly twice for 12 months";
        public static final String WEEKLY_3_MONTH_12    = "Weekly thrice for 12 months";
        public static final String WEEKLY_1_MONTH_6     = "Weekly once for 6 months";
        public static final String WEEKLY_3_MONTH_6     = "Weekly thrice for 6 months";
        public static final String WEEKLY_2_MONTH_6     = "Weekly twice for 6 months";
        public static final String WEEKLY_1_MONTH_3     = "Weekly once for 3 months";
        public static final String WEEKLY_2_MONTH_3     = "Weekly twice for 3 months";
        public static final String WEEKLY_3_MONTH_3     = "Weekly thrice for 3 months";
        public static final String WEEKLY_1_MONTH_2     = "Weekly once for 2 months";
        public static final String WEEKLY_2_MONTH_2     = "Weekly twice for 2 months";
        public static final String WEEKLY_3_MONTH_2     = "Weekly thrice for 2 months";
        public static final String WEEKLY_1_MONTH_1     = "Weekly once for 1 month";
        public static final String WEEKLY_2_MONTH_1     = "Weekly twice for 1 month";
        public static final String WEEKLY_3_MONTH_1     = "Weekly thrice for 1 month";
        public static final String SINGLE_WASH          = "One time wash";
        public static final String WEEKLY_1             = "Once a week";
        public static final String WEEKLY_2             = "Twice a week";
        public static final String WEEKLY_3             = "Thrice a week";
        public static final String WEEKLY_4             = "4 times a week";
        public static final String WEEKLY_5             = "5 times a week";
        public static final String WEEKLY_6             = "6 times a week";
        public static final String WEEKLY_7             = "Complete week";

        public static String[] getAllPackages() {
            String[] packages                           = {WEEKLY_7, WEEKLY_6, WEEKLY_5, WEEKLY_4,
                                                                WEEKLY_3, WEEKLY_2, WEEKLY_1,
                                                                SINGLE_WASH};
            return packages;
        }
    }

    public static final class Vehicle {
        public static final String SEDAN                = "Sedan";
        public static final String HATCH_BACK           = "Hatchback";
        public static final String MINI_WAGON           = "Mini Wagon";
        public static final String COUPE                = "Coupe";
        public static final String SUV                  = "SUV";
        public static final String PICK_UP              = "Pick up";
        public static final String CONVERTIBLE          = "Convertible";

        public static final String[] BODY_TYPES         = {SEDAN, HATCH_BACK, MINI_WAGON, COUPE, SUV, PICK_UP, CONVERTIBLE};
    }
}
