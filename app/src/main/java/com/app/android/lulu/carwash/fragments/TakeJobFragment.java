package com.app.android.lulu.carwash.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.background.AddEmployeeJobTakenTask;
import com.app.android.lulu.carwash.background.GetCapturedBitmapTask;
import com.app.android.lulu.carwash.background.UpdateJobStatusTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.controllers.AppController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.helper.AppUtils;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.EmployeeJob;
import com.app.android.lulu.carwash.model.Job;

import java.io.File;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A placeholder fragment containing a simple view.
 */
public class TakeJobFragment extends CarWashFragmentBaseClass {

    private static final int GET_PERMISSION_FOR_STORAGE = 99;

    private ImageView imageViewVehicle;
    private ImageView imageViewDeleteImage;
    private Button buttonStartStop;
    private TextView textViewVehicleName;
    private TextView textViewParkingLot;
    private TextView textViewCustomerName;
    private TextView textViewContactNum;
    private TextView textViewJobStatus;
    private TextView textViewJobTime;

    private String capturedImagePath;
    private Uri capturedImageUri;
    private boolean hasJobAlreadyStarted        = false;

    Calendar calendarTimer;
    Timer timer;
    TimerTask timerTask;

    private Job jobPassedFromList;

    public TakeJobFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_take_job, container, false);

        imageViewVehicle                        = (ImageView) view.findViewById(R.id.imageView_take_job_vehicle);
        imageViewDeleteImage                    = (ImageView) view.findViewById(R.id.imageView_take_job_delete);
        buttonStartStop                         = (Button) view.findViewById(R.id.button_take_job_start_stop);
        textViewCustomerName                    = (TextView) view.findViewById(R.id.textView_take_job_customer_name);
        textViewParkingLot                      = (TextView) view.findViewById(R.id.textView_take_job_parking_lot);
        textViewVehicleName                     = (TextView) view.findViewById(R.id.textView_take_job_vehicle_name);
        textViewContactNum                      = (TextView) view.findViewById(R.id.textView_take_job_customer_phone);
        textViewJobStatus                       = (TextView) view.findViewById(R.id.textView_take_job_job_status_indicator);
        textViewJobTime                         = (TextView) view.findViewById(R.id.textView_take_job_time);

        return view;
    }

    @Override
    public void initListeners() {
        imageViewVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoOfVehicle(AppConstants.DataPass.TAKE_START_JOB_PICTURE);
            }
        });

        imageViewDeleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteImage();
            }
        });

        buttonStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartStopButtonClick();
            }
        });
    }

    @Override
    public void initData() {
        jobPassedFromList                       = (Job) getData();

        if (jobPassedFromList != null) {
            textViewVehicleName.setText(jobPassedFromList.getCustomer().getVehicle().getModel());
            textViewCustomerName.setText(jobPassedFromList.getCustomer().getName());
            textViewContactNum.setText(jobPassedFromList.getCustomer().getPhone());
            textViewParkingLot.setText(jobPassedFromList.getCustomer().getParkingLot());

            switch (jobPassedFromList.getStatus()) {
                case AppConstants.Job.STATUS_IN_PROGRESS:
                    buttonStartStop.setText(R.string.stop);
                    textViewJobStatus.setText(R.string.job_in_progress);
                    break;
                case AppConstants.Job.STATUS_ASSIGNED:
                    buttonStartStop.setText(R.string.start);
                    break;
                case AppConstants.Job.STATUS_DONE:
                    buttonStartStop.setVisibility(View.INVISIBLE);
                    textViewJobStatus.setText(R.string.job_completed);
                    break;
            }
        }

        startStopJobTimer();
    }

    @Override
    public void toDoOnDetach() {

    }

    @Override
    public void onDestroy() {
        stopTimer();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.DataPass.TAKE_START_JOB_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                GetCapturedBitmapTask task      = new GetCapturedBitmapTask(getCurrentContext(), capturedImageUri);
                task.setLoadingMessage("Processing image...");
                task.setOnTaskResult(new OnTaskResult() {
                    @Override
                    public void onSuccess(Object o) {
                        Object[] objects        = (Object[]) o;
                        capturedImagePath       = (String) objects[1];

                        setImageInImageView((Bitmap) objects[0]);
                    }

                    @Override
                    public void onError(Object o) {
                        Toast.makeText(getCurrentContext(), "Could not process. Please try again", Toast.LENGTH_SHORT).show();
                    }
                });
                //noinspection unchecked
                task.execute();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case GET_PERMISSION_FOR_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takePhotoOfVehicle(AppConstants.DataPass.TAKE_START_JOB_PICTURE);

                } else {
                    Toast.makeText(getCurrentContext(), "Permission denied. Cannot take picture", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private boolean checkIfStoragePermissionGranted() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getCurrentContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                showPermissionDialog();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        GET_PERMISSION_FOR_STORAGE);
            }

            return false;
        }

        return true;
    }

    private void showPermissionDialog() {
        final AlertDialogFragment fragment      = AlertDialogFragment.newInstance("Permission", "Grant permission to take photos and save it in phone storage");
        fragment.show(getFragmentManager(), "fragment_alert");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fragment.dismiss();
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        GET_PERMISSION_FOR_STORAGE);
            }
        }, 2000);
    }

    private void takePhotoOfVehicle(int requestCode) {
        if (hasJobAlreadyStarted) {
            requestCode                         = AppConstants.DataPass.TAKE_STOP_JOB_PICTURE;
        }

        if (!checkIfStoragePermissionGranted()) {
            return;
        }

        try {
            // place where to store camera taken picture
            File photo                          = AppController.createTemporaryFile("picture", ".jpg");
            //capturedImageUri                    = Uri.fromFile(photo);
            capturedImageUri                    = FileProvider.getUriForFile(
                    getCurrentContext(), getCurrentContext().getPackageName()
                            + ".provider",
                    photo);
            //start camera intent
            Intent intent                       = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getCurrentContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_LONG).show();
        }
    }

    private void setImageInImageView(Bitmap bitmap) {
        imageViewVehicle.setImageBitmap(bitmap);
        imageViewDeleteImage.setVisibility(View.VISIBLE);
    }

    private void deleteImage() {
        capturedImagePath                       = null;
        capturedImageUri                        = null;
        imageViewVehicle.setImageResource(R.mipmap.ic_image);
        imageViewDeleteImage.setVisibility(View.INVISIBLE);
    }

    private void onStartStopButtonClick() {
        if (capturedImageUri == null || capturedImagePath == null) {
            Log.d("skt", "Bitmap is null");
            Toast.makeText(getCurrentContext(), "Take image of vehicle", Toast.LENGTH_SHORT).show();
        } else {
            compressAndUploadToServer();
        }
    }

    private void compressAndUploadToServer() {
        File fileOfImage                        = new File(capturedImagePath);
        File fileToUpload                       = AppUtils.compressBitmapAndSaveToFile(fileOfImage, getCurrentContext());

        Log.d("skt", "File size of taken image = " + fileOfImage.length() + ", image path = "
                + capturedImagePath + ", file exists-" + fileOfImage.exists());
        if (fileToUpload != null) {
            Log.d("skt", "File size after compression = " + fileToUpload.length() + ", has job started =" + hasJobAlreadyStarted);

            EmployeeJob employeeJob             = new EmployeeJob();
            employeeJob.setFileName("test");
            employeeJob.setFileOfVehicleImage(fileToUpload);
            employeeJob.setJobId(jobPassedFromList.getId());

            uploadToServer(employeeJob);
        }
    }

    private void uploadToServer(EmployeeJob employeeJob) {
        AddEmployeeJobTakenTask task            = new AddEmployeeJobTakenTask(getCurrentContext(), employeeJob);
        task.setLoadingMessage("Uploading image...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                Log.d("skt", "Success");
                updateJobStatus();
            }

            @Override
            public void onError(Object o) {
                Log.d("skt", "Error--" + AppServiceErrorHelper.getErrorString());

                Toast.makeText(getCurrentContext(), "Could not save. " + AppServiceErrorHelper.getErrorString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //noinspection unchecked
        task.execute();
    }

    private void updateJobStatus() {
        updateJobStatusOfObject();

        UpdateJobStatusTask task                = new UpdateJobStatusTask(getCurrentContext(), jobPassedFromList);
        task.setLoadingMessage("Updating job ...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                Log.d("skt", "Success");
                updateJobStatusOfObject();
                startStopJobTimer();
                AppController.showSuccessMessage(getCurrentContext(), "Uploaded", "Image sent successfully and status updated", onDialogAction);
            }

            @Override
            public void onError(Object o) {
                Log.d("skt", "Error--" + AppServiceErrorHelper.getErrorString());

                Toast.makeText(getCurrentContext(), "Could not update. " + AppServiceErrorHelper.getErrorString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //noinspection unchecked
        task.execute();
    }

    private void updateJobStatusOfObject() {
        if (jobPassedFromList.getStatus().equals(AppConstants.Job.STATUS_ASSIGNED)) {
            jobPassedFromList.setStatus(AppConstants.Job.STATUS_IN_PROGRESS);
            jobPassedFromList.setStartDate(Calendar.getInstance().getTime());
        } else if (jobPassedFromList.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS)) {
            jobPassedFromList.setStatus(AppConstants.Job.STATUS_DONE);
            jobPassedFromList.setEndDate(Calendar.getInstance().getTime());
        }
    }

    private void startStopJobTimer() {
        if (jobPassedFromList.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS)
                && jobPassedFromList.getStartDate() != null) {

            calendarTimer                       = Calendar.getInstance();

            if (timer == null) {
                timer                           = new Timer();
            }

            if (timerTask == null)
                timerTask                       = new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String duration     = AppController.getTimeDurationBetweenDates(
                                        jobPassedFromList.getStartDate(), calendarTimer.getTime())[1];
                                textViewJobTime.setText(duration);
                                calendarTimer.add(Calendar.SECOND, 1);
                            }
                        });
                    }
                };

            timer.schedule(timerTask, 0, 1000);
        } else if (jobPassedFromList.getStatus().equals(AppConstants.Job.STATUS_DONE)
                && jobPassedFromList.getStartDate() != null
                && jobPassedFromList.getEndDate() != null) {
            stopTimer();
            long startTime                      = jobPassedFromList.getStartDate().getTime();
            long endTime                        = jobPassedFromList.getEndDate().getTime();
            Calendar startCalendar              = Calendar.getInstance();
            Calendar endCalendar                = Calendar.getInstance();
            startCalendar.setTimeInMillis(startTime);
            endCalendar.setTimeInMillis(endTime);

            String duration                     = AppController.getTimeDurationBetweenDates(
                    startCalendar.getTime(), endCalendar.getTime())[1];
            textViewJobTime.setText(duration);
        }
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    private OnDialogAction onDialogAction       = new OnDialogAction() {
        @Override
        public void onOk() {
            onTaskSuccess();
        }

        @Override
        public void onYes() {

        }

        @Override
        public void onNo() {

        }

        @Override
        public void onCancel() {

        }
    };

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
