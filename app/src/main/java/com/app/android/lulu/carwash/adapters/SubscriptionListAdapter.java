package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Plot;
import com.app.android.lulu.carwash.model.Subscription;
import com.app.android.lulu.carwash.model.Vehicle;

import java.util.ArrayList;

public class SubscriptionListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Subscription> customerSubscriptions;
    private ArrayList<Subscription> oCustomerSubscriptions;

    private boolean showSelectCheckBox;
    private Context context;
    private LayoutInflater inflater;

    public SubscriptionListAdapter(Context context, ArrayList<Subscription> customerSubscriptions) {
        this.customerSubscriptions              = customerSubscriptions;
        this.context                            = context;
        inflater                                = LayoutInflater.from(context);

        oCustomerSubscriptions                  = new ArrayList<>();
        oCustomerSubscriptions.addAll(customerSubscriptions);
    }

    public void toggleSelect() {
        showSelectCheckBox                      = !showSelectCheckBox;
        notifyDataSetChanged();
    }

    public boolean isShowSelectCheckBox() {
        return showSelectCheckBox;
    }

    public ArrayList<Subscription> getActualSubscriptionList() {
        return oCustomerSubscriptions;
    }

    @Override
    public int getCount() {
        return customerSubscriptions != null ? customerSubscriptions.size() : 0;
    }

    @Override
    public Subscription getItem(int i) {
        return (customerSubscriptions != null && customerSubscriptions.get(i) != null) ? customerSubscriptions.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if(view == null) {
            holder                              = new ViewHolder();

            view                                = inflater.inflate(R.layout.list_item_subscription, null);

            holder.firstCharacter               = (TextView) view.findViewById(R.id.textView_item_subscription_select);
            holder.vehicleName                  = (TextView) view.findViewById(R.id.textView_item_subscription_vehicle_name);
            holder.customerName                 = (TextView) view.findViewById(R.id.textView_item_subscription_customer_name);
            holder.plotName                     = (TextView) view.findViewById(R.id.textView_item_subscription_plot_name);
            holder.selectItem                   = (CheckBox) view.findViewById(R.id.checkBox_item_subscription_select);

            view.setTag(holder);
        } else {
            holder                              = (ViewHolder) view.getTag();
        }

        final Subscription subscription         = customerSubscriptions.get(pos);
        Customer customer                       = subscription != null ? subscription.getCustomer() : null;
        Vehicle vehicle                         = customer != null ? customer.getVehicle() : null;
        Plot plot                               = subscription != null ? subscription.getPlot() : null;

        if (vehicle != null)
            holder.vehicleName.setText(vehicle.getModel());

        if (plot != null) {
            holder.plotName.setText(plot.getName());
        }

        if (customer != null) {
            String nameOfCustomer               = customer.getName();
            holder.customerName.setText(customer.getName());

            try {
                String firstChar                = String.valueOf(nameOfCustomer.toUpperCase().charAt(0));
                holder.firstCharacter.setText(firstChar);
            } catch (Exception e) {
                Log.d("skt", "Error" + e);
            }
        }

        holder.customerName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.plotName.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.vehicleName.setTextColor(ContextCompat.getColor(context, R.color.black));

        holder.selectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isCheckBoxSelected      = ((CheckBox) v).isChecked();
                Log.d("skt", "Is check box selected--" + isCheckBoxSelected);

                subscription.setSelected(isCheckBoxSelected);

                changeItemFromOriginalList(subscription);
            }
        });

        long assignedUserId                   = subscription.getAssignedUserId();
        Log.d("skt", "Assigned user id = " + assignedUserId);

        if (assignedUserId !=  0)
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.green_mint));
        else
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.white));


        if (showSelectCheckBox) {
            holder.firstCharacter.setVisibility(View.INVISIBLE);
            holder.selectItem.setVisibility(View.VISIBLE);
        } else {
            holder.firstCharacter.setVisibility(View.VISIBLE);
            holder.selectItem.setVisibility(View.INVISIBLE);
        }

        holder.selectItem.setChecked(subscription.isSelected());

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Subscription> filteredSubscriptions = new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredSubscriptions.addAll(oCustomerSubscriptions);
                } else {
                    for (int i = 0; i < oCustomerSubscriptions.size(); i++) {
                        Subscription subscription = oCustomerSubscriptions.get(i);
                        String customerName     = subscription.getCustomer().getName();
                        if (customerName.toLowerCase().contains(constraint.toString())) {
                            filteredSubscriptions.add(subscription);
                        }
                    }
                }

                results.count                   = filteredSubscriptions.size();
                results.values                  = filteredSubscriptions;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                customerSubscriptions           = (ArrayList<Subscription>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private void changeItemFromOriginalList(Subscription selectedItem) {
        for(Subscription subscription : oCustomerSubscriptions) {
            if (subscription.getId() == selectedItem.getId()) {
                subscription.setSelected(selectedItem.isSelected());
                break;
            }
        }
    }

    private static class ViewHolder {
        private TextView customerName;
        private TextView vehicleName;
        private TextView plotName;
        private CheckBox selectItem;
        private TextView firstCharacter;
    }
}
