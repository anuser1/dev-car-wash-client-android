package com.app.android.lulu.carwash.model;

import com.app.android.lulu.carwash.constants.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Santhosh on 16-09-2017.
 */

public class Employee {
    private String name;
    private String empId;
    private String fullName;
    private String surName;
    private String emailAddress;
    private String phoneNumber;
    private int timeSlotId;
    private int id;
    private boolean isSupervisor;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        if (!surName.equals("null"))
            this.surName = surName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        if (!emailAddress.equals("null"))
            this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if (!phoneNumber.equals("null"))
            this.phoneNumber = phoneNumber;
    }

    public int getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public void setSupervisor(boolean supervisor) {
        isSupervisor = supervisor;
    }

    @Override
    public String toString() {
        JSONObject object                       = new JSONObject();
        try {
            object.put("name", getName());

            {
                //Null checks
                if (getSurName() != null && !getSurName().equals("null"))
                    object.put("surname", getSurName());
                else
                    object.put("surname", null);

                if (getEmailAddress() != null && !getEmailAddress().equals("null"))
                    object.put("emailAddress", getEmailAddress());
                else
                    object.put("emailAddress", null);

                if (getPhoneNumber() != null && !getPhoneNumber().equals("null"))
                    object.put("phoneNumber", getPhoneNumber());
                else
                    object.put("phoneNumber", null);

            }

            JSONArray roleJsonArray         = new JSONArray();

            if (isSupervisor()) {
                roleJsonArray.put(AppConstants.User.SUPERVISOR);
            } else {
                roleJsonArray.put(AppConstants.User.EMPLOYEE);
            }

            object.put("roleNames", roleJsonArray);
            object.put("userName", getEmpId());
            object.put("timeSlotId", getTimeSlotId());
            object.put("id", getId());
            object.put("isActive", true);

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
