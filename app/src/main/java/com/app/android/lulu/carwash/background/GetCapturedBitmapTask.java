package com.app.android.lulu.carwash.background;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.AppController;

import java.io.ByteArrayOutputStream;

public class GetCapturedBitmapTask extends CarwashAsyncTaskBaseClass {

    private String capturedImagePath;
    private Uri capturedImageUri;

    public GetCapturedBitmapTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        //noinspection unchecked
        capturedImageUri                        = (Uri) o[0];
    }

    @Override
    public Object tasksInBackground() {
        Bitmap bmp                              = grabImage();
        if (bmp != null) {
            bmp                                 = getCapturedBitmap(bmp);
        }

        return new Object[] {bmp, capturedImagePath};
}

    @Override
    public boolean onTaskCompletion(Object o) {
        return o != null;
    }

    private Bitmap grabImage() {
        getContext().getContentResolver().notifyChange(capturedImageUri, null);
        ContentResolver cr                      = getContext().getContentResolver();
        Bitmap bitmap;
        try {
            bitmap                              = android.provider.MediaStore.Images.Media.getBitmap(cr, capturedImageUri);
        } catch (Exception e) {
            bitmap                              = null;
        }

        return bitmap;
    }

    private Bitmap getCapturedBitmap(Bitmap bmp) {
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri                             = AppController.getImageUri(getContext(), bmp);
        capturedImagePath                       = AppController.getRealPathFromURI(getContext(), tempUri);

        ByteArrayOutputStream stream            = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        final byte[] byteArray                  = stream.toByteArray();

        // convert byte array to Bitmap
        Bitmap bitmapImageOfVehicle             = AppController.decodeSampledBitmapFromCamera(byteArray, 20, 20);
        try {
            bitmapImageOfVehicle                = AppController.detectImageOrientationAndRotate(
                    getContext(), capturedImageUri,
                    bitmapImageOfVehicle, capturedImagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmapImageOfVehicle;
    }
}
