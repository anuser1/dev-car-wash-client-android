package com.app.android.lulu.carwash.model;

import org.json.JSONException;
import org.json.JSONObject;


public class Plot {
    public static final String DESCRIPTION_DELIMITER = "tt!@#tt";
    private String name;
    private String description;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        JSONObject object                       = new JSONObject();
        try {

            object.put("name", getName());

            if (!getDescription().equals("")) {
                String desc                     = getDescription();

                object.put("description", desc);
            }
            if (getId() != 0) {
                object.put("id", getId());
            }

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }
}
