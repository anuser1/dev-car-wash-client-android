package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.model.Package;

import java.util.ArrayList;

/**
 * Created by Santhosh on 22-09-2017.
 */

public class PackagesSpinnerAdapter extends BaseAdapter {
    private ArrayList<Package> packageArrayList;

    private LayoutInflater inflater;

    public PackagesSpinnerAdapter(Context context, ArrayList<Package> packageArrayList) {
        this.packageArrayList                   = packageArrayList;
        inflater                                = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return packageArrayList != null ? packageArrayList.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return (packageArrayList != null && packageArrayList.get(i) != null) ? packageArrayList.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_text, null);
            holder.packageText                  = (TextView) convertView.findViewById(R.id.textView_spinner_text_value);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.packageText.setText(packageArrayList.get(position).getPackageText());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_spinner_drop_down, null);
            holder.packageText                  = (TextView) convertView.findViewById(R.id.textView_spinner_drop_down_value);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        holder.packageText.setText(packageArrayList.get(position).getPackageText());
        holder.packageText.setTextColor(0xff000000);

        return convertView;
    }

    private static class ViewHolder {
        TextView packageText;
    }
}
