package com.app.android.lulu.carwash.controllers;


import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.android.lulu.carwash.activities.LoginActivity;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.constants.ServiceConstants;
import com.app.android.lulu.carwash.constants.UrlConstants;
import com.app.android.lulu.carwash.constants.enums.RequestMethod;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.libs.ServiceCall;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Employee;
import com.app.android.lulu.carwash.model.EmployeeJob;
import com.app.android.lulu.carwash.model.Job;
import com.app.android.lulu.carwash.model.LoggedInUser;
import com.app.android.lulu.carwash.model.LoginUser;
import com.app.android.lulu.carwash.model.Plot;
import com.app.android.lulu.carwash.model.Subscription;
import com.app.android.lulu.carwash.model.UpdatedEmployee;
import com.app.android.lulu.carwash.model.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServiceController {

    private int responseCode;

    private int getResponseCode() {
        return responseCode;
    }

    private void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public LoggedInUser authenticateUser(Context context, LoginUser loginUser) {
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.LOGIN_URL);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(loginUser.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            if (getResponseCode() == 200 || getResponseCode() == 201) {
                LoggedInUser loggedInUser       = new LoggedInUser(response);
                if (loggedInUser.getToken() != null && loggedInUser.getToken().length() > 0) {
                    onUserLogin(context, loggedInUser, loginUser.getUsername(), loginUser.getPassword());
                } else {
                    loggedInUser                = null;
                }
                return loggedInUser;
            } else {
                AppServiceErrorHelper.setErrorResponseJson(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Employee> getEmployees(Context context) {
       AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_EMPLOYEES);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return getEmployeeArrayFromResponse(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return getEmployeeArrayFromResponse(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Employee> getSupervisors(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_SUPERVISORS);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return getSupervisorsArrayFromResponse(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return getEmployeeArrayFromResponse(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public UpdatedEmployee updateEmployee(Context context, Employee employee) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.UPDATE_EMPLOYEE);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(employee.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return new UpdatedEmployee(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return new UpdatedEmployee(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Plot> getPlotList(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_PLOTS);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return getPlotArrayFromResponse(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return getPlotArrayFromResponse(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Customer> getCustomerList(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_CUSTOMERS);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);

        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return getCustomerArrayFromResponse(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return getCustomerArrayFromResponse(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Customer addCustomer(Context context, Customer customer) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ADD_CUSTOMER);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(customer.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onCustomerAddition(response, customer);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onCustomerAddition(response, customer);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Customer deleteCustomer(Context context, Customer customer) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.DELETE_CUSTOMER + customer.getId());
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onCustomerDelete(response, customer);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onCustomerDelete(response, customer);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Customer editCustomer(Context context, Customer customer) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.EDIT_CUSTOMER);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(customer.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onCustomerAddition(response, customer);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onCustomerAddition(response, customer);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Plot addPlot(Context context, Plot plot) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ADD_PLOT);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(plot.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onPlotAddition(response, plot);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onPlotAddition(response, plot);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Plot updatePlot(Context context, Plot plot) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.UPDATE_PLOT);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(plot.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onPlotAddition(response, plot);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onPlotAddition(response, plot);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Employee addEmployee(Context context, Employee employee) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ADD_EMPLOYEE);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(employee.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onEmployeeAddition(response, employee);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onEmployeeAddition(response, employee);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Employee addSupervisor(Context context, Employee employee) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ADD_SUPERVISOR);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(employee.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onEmployeeAddition(response, employee);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onEmployeeAddition(response, employee);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String updatePassword(Context context, String password) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.CHANGE_PASSWORD);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams("{password : " + password + "}");
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onPasswordUpdate(response, password);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onPasswordUpdate(response, password);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Subscription addSubscription(Context context, Subscription subscription) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ADD_SUBSCRIPTION);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(subscription.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onSubscriptionAddition(response, subscription);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onSubscriptionAddition(response, subscription);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Subscription editSubscription(Context context, Subscription subscription) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.UPDATE_SUBSCRIPTION);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(subscription.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onSubscriptionUpdation(response, subscription);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onSubscriptionUpdation(response, subscription);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Job> getAllJobs(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_ALL_JOBS);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);

        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onGettingAllJobs(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onGettingAllJobs(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Subscription> getAllSubscription(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_ALL_SUBSCRIPTION);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);

        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onGettingSubscriptionListFromServer(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onGettingSubscriptionListFromServer(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Subscription getSubscriptionDetails(Context context, Subscription subscription) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_SUBSCRIPTION_DET + subscription.getId());
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);

        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onGettingSubscriptionDetails(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onGettingSubscriptionDetails(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Job> getAssignedJobs(Context context) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.GET_ASSIGNED_JOBS);
        serviceCall.setRequestMethod(RequestMethod.GET);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);

        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return getAssignedJobsArrayFromResponse(response);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return getAssignedJobsArrayFromResponse(response);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @SuppressWarnings("ConstantConditions")
    public EmployeeJob addEmployeeJobTaken(Context context, EmployeeJob employeeJob) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();

        String BOUNDARY                         = "----WebKitFormBoundary7MA4YWxkTrZu0gW";

        OkHttpClient okHttpClient               = new OkHttpClient();

        final MediaType MEDIA_TYPE              = employeeJob.getFileOfVehicleImage().
                                                        getName().toLowerCase().endsWith("png") ?
                                                            MediaType.parse("image/png") :
                                                            MediaType.parse("image/jpeg");

        RequestBody body                        = new MultipartBody.Builder()
                                                    .setType(MultipartBody.FORM)
                                                    .addFormDataPart("test", employeeJob.getFileOfVehicleImage().getName(),
                                                            RequestBody.create(MEDIA_TYPE, employeeJob.getFileOfVehicleImage()))
                                                    .addFormDataPart("jobId", String.valueOf(employeeJob.getJobId()))
                                                    .build();
        Request request                         = new Request.Builder()
                                                        .url(UrlConstants.UPLOAD_IMAGE_JOB)
                                                        .post(body)
                                                        .addHeader(ServiceConstants.HeaderKeys.contentType,
                                                                ServiceConstants.HeaderValues.multiPartFormData + "; boundary=" + BOUNDARY)
                                                        .addHeader(ServiceConstants.HeaderKeys.authorization, token)
                                                        .addHeader(ServiceConstants.HeaderKeys.cacheControl, ServiceConstants.HeaderValues.noCache)
                                                        .build();

        try {
            Response response                   = okHttpClient.newCall(request).execute();
            int responseCode                    = response.code();
            setResponseCode(responseCode);

            Log.d("skt", "Response code = " + getResponseCode());

            String responseStr                  = "";
            if (response.body() != null) {
                responseStr                     = response.body().string();
            }

            Log.d("skt", "Response = " + responseStr);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201) {
                    return onSavingImageResponse(responseStr, employeeJob);

                } else {
                    AppServiceErrorHelper.setErrorResponseJson(responseStr);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    request                     = new Request.Builder()
                                                        .url(UrlConstants.UPLOAD_IMAGE_JOB)
                                                        .post(body)
                                                        .addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.multiPartFormData + "; boundary=" + BOUNDARY)
                                                        .addHeader(ServiceConstants.HeaderKeys.authorization, token)
                                                        .addHeader(ServiceConstants.HeaderKeys.cacheControl, ServiceConstants.HeaderValues.noCache)
                                                        .build();

                    response                    = okHttpClient.newCall(request).execute();
                    responseCode                = response.code();
                    setResponseCode(responseCode);

                    if (getResponseCode() == 200 || getResponseCode() == 201) {
                        responseStr             = "";
                        if (response.body() != null) {
                            responseStr         = response.body().string();
                        }

                        return onSavingImageResponse(responseStr, employeeJob);
                    } else {
                        String errorStr         = "";
                        if (response.body() != null)
                            errorStr = response.body().string();

                        AppServiceErrorHelper.setErrorResponseJson(errorStr);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Subscription assignEmployeeToSubscription(Context context, Subscription subscription) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.ASSIGN_SUBSCRIPTION);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(subscription.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onSubscriptionAssign(response, subscription);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onSubscriptionAssign(response, subscription);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Job updateJobStatus(Context context, Job job) {
        AppPrefs appPrefs                       = new AppPrefs(context);
        String token                            = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();

        JSONObject jsonObject                   = new JSONObject();
        try {
            jsonObject.put("id", job.getId());
            jsonObject.put("Status", job.getStatus());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceCall serviceCall                 = new ServiceCall(UrlConstants.UPDATE_JOB_STATUS);
        serviceCall.setRequestMethod(RequestMethod.POST);
        serviceCall.setParams(jsonObject.toString());
        serviceCall.addHeader(ServiceConstants.HeaderKeys.contentType, ServiceConstants.HeaderValues.applicationJson);
        serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);
        try {
            String response                     = serviceCall.execute();
            setResponseCode(serviceCall.getResponseCode());

            Log.d("skt", "response of update--" + response);

            if (validateToken()) {
                if (getResponseCode() == 200 || getResponseCode() == 201)
                    return onJobUpdate(response, job);
                else {
                    AppServiceErrorHelper.setErrorResponseJson(response);
                }
            } else {
                boolean haveAuthenticated       = tryToLoginAgain(context);
                if (haveAuthenticated) {
                    token                       = ServiceConstants.HeaderValues.bearer + " " + appPrefs.getToken();
                    serviceCall.addHeader(ServiceConstants.HeaderKeys.authorization, token);

                    response                    = serviceCall.execute();
                    setResponseCode(serviceCall.getResponseCode());

                    if (getResponseCode() == 200 || getResponseCode() == 201)
                        return onJobUpdate(response, job);
                    else {
                        AppServiceErrorHelper.setErrorResponseJson(response);
                    }
                } else
                    onFailureInAuthenticationAfterRepeatedTries(context);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void onUserLogin(Context context, LoggedInUser loggedInUser, String username, String password) {
        AppPrefs appPrefs                       = new AppPrefs(context);

        appPrefs.setToken(loggedInUser.getToken());

        if (loggedInUser.getRole() != null && loggedInUser.getRole().length > 0) {
            final Set<String> VALUES            = new HashSet<>(Arrays.asList(loggedInUser.getRole()));
            if (VALUES.contains(AppConstants.User.SUPER_ADMIN)) {
                appPrefs.setRole(AppConstants.User.SUPER_ADMIN);
            } else if (VALUES.contains(AppConstants.User.SUPERVISOR)) {
                appPrefs.setRole(AppConstants.User.SUPERVISOR);
            } else {
                appPrefs.setRole(AppConstants.User.EMPLOYEE);
            }
        }

        appPrefs.setPassword(password);
        appPrefs.setUsername(username);
        appPrefs.setEmployeeId(loggedInUser.getEmployeeId());
        appPrefs.setEmail(loggedInUser.getEmail());
        appPrefs.setPhone(loggedInUser.getPhoneNumber());
        appPrefs.setFullName(loggedInUser.getFullName());

        Log.d("skt", "token--" + loggedInUser.getToken());
    }

    @Nullable
    private ArrayList<Employee> getEmployeeArrayFromResponse(String response) {
        try {
            JSONObject object                       = new JSONObject(response);
            JSONArray employeeJsonArray             = object.getJSONObject("result").getJSONArray("items");

            Log.d("skt", "employee array = " + employeeJsonArray.toString());
            int len                                 = employeeJsonArray.length();
            ArrayList<Employee> employees           = new ArrayList<>();

            for (int i = 0; i < len; i++) {
                JSONObject empJsonObj               = employeeJsonArray.getJSONObject(i);
                Employee employee                   = new Employee();

                employee.setName(empJsonObj.getString("name"));
                employee.setFullName(empJsonObj.getString("fullName"));
                employee.setSurName(empJsonObj.getString("surname"));
                employee.setEmailAddress(empJsonObj.getString("emailAddress"));
                employee.setPhoneNumber(empJsonObj.getString("phoneNumber"));
                employee.setTimeSlotId(empJsonObj.getInt("timeSlotId"));
                employee.setId(empJsonObj.getInt("id"));
                employee.setEmpId(empJsonObj.getString("employeeId"));
                employee.setSupervisor(false);

                employees.add(employee);
            }

            return employees;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Nullable
    private ArrayList<Employee> getSupervisorsArrayFromResponse(String response) {
        try {
            JSONObject object                       = new JSONObject(response);
            JSONArray employeeJsonArray             = object.getJSONObject("result").getJSONArray("items");

            Log.d("skt", "supervisor array = " + employeeJsonArray.toString());

            int len                                 = employeeJsonArray.length();
            ArrayList<Employee> employees           = new ArrayList<>();

            for (int i = 0; i < len; i++) {
                JSONObject empJsonObj               = employeeJsonArray.getJSONObject(i);
                Employee employee                   = new Employee();

                employee.setName(empJsonObj.getString("name"));
                employee.setFullName(empJsonObj.getString("fullName"));
                employee.setSurName(empJsonObj.getString("surname"));
                employee.setEmailAddress(empJsonObj.getString("emailAddress"));
                employee.setPhoneNumber(empJsonObj.getString("phoneNumber"));
                employee.setTimeSlotId(empJsonObj.getInt("timeSlotId"));
                employee.setId(empJsonObj.getInt("id"));
                employee.setEmpId(empJsonObj.getString("employeeId"));
                employee.setSupervisor(true);

                employees.add(employee);
            }

            return employees;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Nullable
    private ArrayList<Plot> getPlotArrayFromResponse(String response) {
        try {
            JSONObject object                       = new JSONObject(response);
            JSONArray plotJsonArray                 = object.getJSONObject("result").getJSONArray("items");

            Log.d("skt", "employee array = " + plotJsonArray.toString());
            int len                                 = plotJsonArray.length();
            ArrayList<Plot> plots                   = new ArrayList<>();

            for (int i = 0; i < len; i++) {
                JSONObject plotJsonObj              = plotJsonArray.getJSONObject(i);
                Plot plot                           = new Plot();

                plot.setName(plotJsonObj.getString("name"));
                plot.setId(plotJsonObj.getInt("id"));

                String desc                         = plotJsonObj.getString("description");
                if (desc != null) {
                    String[] descriptionAndFlatNum  = desc.split(Plot.DESCRIPTION_DELIMITER);

                    plot.setDescription(descriptionAndFlatNum[0].trim());

                }


                plots.add(plot);
            }

            return plots;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Plot onPlotAddition(String response, Plot plot) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return plot;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private ArrayList<Customer> getCustomerArrayFromResponse(String response) {
        try {
            JSONObject object                       = new JSONObject(response);
            JSONArray customerJsonArray             = object.getJSONObject("result").getJSONArray("items");

            Log.d("skt", "employee array = " + customerJsonArray.toString());
            int len                                 = customerJsonArray.length();
            ArrayList<Customer> customers           = new ArrayList<>();

            for (int i = 0; i < len; i++) {
                JSONObject customerJsonObj          = customerJsonArray.getJSONObject(i);
                Customer customer                   = new Customer();

                customer.setName(customerJsonObj.getString("name"));
                customer.setEmail(customerJsonObj.getString("emailAddress"));
                customer.setPhone(customerJsonObj.getString("phoneNumber"));

                String parkingLot                   = customerJsonObj.getString("parkingLotNumber");
                if (parkingLot != null) {
                    String[] parkingLotAndFlatNum  = parkingLot.split(Customer.PARKING_LOT_DELIMITER);
                    customer.setParkingLot(parkingLotAndFlatNum[0].trim());

                    if (parkingLotAndFlatNum.length > 1) {
                        customer.setFlatNumber(parkingLotAndFlatNum[1].trim());
                    }
                }

                Plot plot                           = new Plot();
                plot.setId(customerJsonObj.getInt("plotId"));
                plot.setName(customerJsonObj.getString("plotName"));
                customer.setPlotDetails(plot);

                customer.setId(customerJsonObj.getInt("id"));

                JSONObject vehicleJsonObject        = customerJsonObj.getJSONObject("vehicleInformation");

                Vehicle vehicle                     = new Vehicle();

                vehicle.setCustomerVehicleId(vehicleJsonObject.getInt("customerVehicleId"));
                vehicle.setMake(vehicleJsonObject.getString("make"));
                vehicle.setModel(vehicleJsonObject.getString("model"));
                vehicle.setType(vehicleJsonObject.getString("bodyStyle"));

                customer.setVehicle(vehicle);

                customers.add(customer);
            }

            return customers;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Customer onCustomerAddition(String response, Customer customer) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return customer;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Customer onCustomerDelete(String response, Customer customer) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return customer;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Employee onEmployeeAddition(String response, Employee employee) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess) {
                JSONObject jsonObjectResult         = object.getJSONObject("result");
                String employeeId                   = jsonObjectResult.getString("employeeID");

                employee.setEmpId(employeeId);

                return employee;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String onPasswordUpdate(String response, String password) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess) {
                return password;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Subscription onSubscriptionAddition(String response, Subscription subscription) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return subscription;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Subscription onSubscriptionUpdation(String response, Subscription subscription) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return subscription;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Subscription onSubscriptionAssign(String response, Subscription subscription) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return subscription;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Job onJobUpdate(String response, Job job) {
        try {
            JSONObject object                       = new JSONObject(response);
            boolean isSuccess                       = object.getBoolean("success");

            if (isSuccess)
                return job;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Subscription> onGettingSubscriptionListFromServer(String response) {

        try {
            JSONObject jsonObject               = new JSONObject(response);
            if (!jsonObject.getBoolean("success")) {
                return null;
            }

            JSONArray jsonArrayItems            = jsonObject.getJSONObject("result").getJSONArray("items");

            int lengthOfRes                     = jsonArrayItems.length();

            ArrayList<Subscription> arrayList   = new ArrayList<>();

            for (int i = 0; i < lengthOfRes; i++) {
                JSONObject jsonObjectItem       = jsonArrayItems.getJSONObject(i);
                Subscription subscription       = new Subscription();

                Customer customer               = new Customer();
                Vehicle vehicle                 = new Vehicle();
                Plot plot                       = new Plot();

                customer.setId(jsonObjectItem.getInt("customerId"));
                customer.setName(jsonObjectItem.getString("customerName"));

                vehicle.setCustomerVehicleId(jsonObjectItem.getInt("customerVehicleId"));
                vehicle.setModel(jsonObjectItem.getString("vehilceName"));

                plot.setId(jsonObjectItem.getInt("plotId"));
                plot.setName(jsonObjectItem.getString("plotName"));

                if (!jsonObjectItem.isNull("assignedUserId")) {
                    subscription.setAssignedUserId(jsonObjectItem.getLong("assignedUserId"));
                    subscription.setAssignedUserName(jsonObjectItem.getString("assingedUserName"));
                }
                subscription.setId(jsonObjectItem.getLong("id"));

                customer.setVehicle(vehicle);
                customer.setPlotDetails(plot);

                subscription.setPlot(plot);
                subscription.setCustomer(customer);

                arrayList.add(subscription);
            }

            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Subscription onGettingSubscriptionDetails(String response) {

        try {
            JSONObject jsonObject               = new JSONObject(response);
            if (!jsonObject.getBoolean("success")) {
                return null;
            }

            JSONObject jsonObjectSubscription   = jsonObject.getJSONObject("result");
            Subscription subscription           = new Subscription();
            Customer customer                   = new Customer();
            Vehicle vehicle                     = new Vehicle();
            Plot plot                           = new Plot();

            customer.setId(jsonObjectSubscription.getInt("customerId"));
            customer.setName(jsonObjectSubscription.getString("customerName"));
            String parkingLotNumber             = jsonObjectSubscription.getString("parkingLotNumber");
            String[] parkingLotFlatNum          = parkingLotNumber.split(Customer.PARKING_LOT_DELIMITER);
            if (parkingLotFlatNum.length > 0)
                customer.setParkingLot(parkingLotFlatNum[0]);
            if (parkingLotFlatNum.length > 1)
                customer.setFlatNumber(parkingLotFlatNum[1]);

            vehicle.setCustomerVehicleId(jsonObjectSubscription.getInt("customerVehicleId"));
            vehicle.setModel(jsonObjectSubscription.getString("vehilceName"));

            plot.setId(jsonObjectSubscription.getInt("plotId"));
            plot.setName(jsonObjectSubscription.getString("plotName"));

            subscription.setWashPerWeek(jsonObjectSubscription.getInt("noOfWashPerWeek"));
            subscription.setNumberOfMonths((float) jsonObjectSubscription.getDouble("noOfMonth"));
            subscription.setActive(jsonObjectSubscription.getBoolean("isActive"));
            subscription.setNextPaymentDate(jsonObjectSubscription.getString("nextPaymentDate"));
            subscription.setStartDate(jsonObjectSubscription.getString("subscriptionStartDate"));
            subscription.setEndDate(jsonObjectSubscription.getString("subscriptionEndDate"));

            if (!jsonObjectSubscription.isNull("assignedUserId")) {
                subscription.setAssignedUserId(jsonObjectSubscription.getLong("assignedUserId"));
                subscription.setAssignedUserName(jsonObjectSubscription.getString("assingedUserName"));
            }
            subscription.setId(jsonObjectSubscription.getLong("id"));

            JSONArray jsonArrayDays             = jsonObjectSubscription.getJSONArray("subscribedDays");
            int lenOfDaysArray                  = jsonArrayDays.length();
            String[] daysArr                    = new String[lenOfDaysArray];
            for (int i = 0; i < lenOfDaysArray; i++) {
                daysArr[i]                      = jsonArrayDays.getString(i);
            }

            if (subscription.getNumberOfMonths() == 0) {
                daysArr                         = new String[0];
            }
            subscription.setSelectedDays(daysArr);

            customer.setVehicle(vehicle);
            customer.setPlotDetails(plot);

            subscription.setPlot(plot);
            subscription.setCustomer(customer);

            return subscription;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private ArrayList<Job> getAssignedJobsArrayFromResponse(String response) {
        try {
            JSONObject object                   = new JSONObject(response);
            JSONArray jsonArrayJob              = object.getJSONArray("result");

            Log.d("skt", "job array = " + jsonArrayJob.toString());
            int len                             = jsonArrayJob.length();
            ArrayList<Job> jobsAssigned         = new ArrayList<>();
            SimpleDateFormat serverDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());

            for (int i = 0; i < len; i++) {
                JSONObject jsonObjJob           = jsonArrayJob.getJSONObject(i);

                Job job                         = new Job();
                Customer customer               = new Customer();
                Vehicle vehicle                 = new Vehicle();

                customer.setName(jsonObjJob.getString("customerName"));
                customer.setEmail(jsonObjJob.getString("emailAddress"));
                customer.setPhone(jsonObjJob.getString("phoneNumber"));

                String parkingLot                = jsonObjJob.getString("parkingLotNumber");
                if (parkingLot != null) {
                    String[] parkingLotAndFlatNum  = parkingLot.split(Customer.PARKING_LOT_DELIMITER);
                    customer.setParkingLot(parkingLotAndFlatNum[0].trim());

                    if (parkingLotAndFlatNum.length > 1) {
                        customer.setFlatNumber(parkingLotAndFlatNum[1].trim());
                    }
                }

                vehicle.setModel(jsonObjJob.getString("vehicleName"));

                job.setStatus(jsonObjJob.getString("status"));
                job.setId(jsonObjJob.getLong("id"));

                try {
                    Date startDate              = serverDateFormat.parse(jsonObjJob.getString("jobStartDateTime"));
                    Date endDate                = serverDateFormat.parse(jsonObjJob.getString("jobEndDateTime"));

                    job.setStartDate(startDate);
                    job.setEndDate(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                customer.setVehicle(vehicle);

                job.setCustomer(customer);

                jobsAssigned.add(job);
            }

            return jobsAssigned;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private EmployeeJob onSavingImageResponse(String response, EmployeeJob employeeJob) {
        boolean success                             = false;

        try {
            JSONObject jsonObject                   = new JSONObject(response);
            success                                 = jsonObject.getBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (success)
            return employeeJob;

        return null;
    }

    private ArrayList<Job> onGettingAllJobs(String response) {
        ArrayList<Job> arrayList                = null;
        try {
            JSONObject jsonObject               = new JSONObject(response);
            JSONArray jsonArrayResult           = jsonObject.getJSONArray("result");

            int arrayLen                        = jsonArrayResult.length();
            SimpleDateFormat serverDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
            Calendar calendar                   = Calendar.getInstance();

            calendar.add(Calendar.DAY_OF_MONTH, -14);
            Date dateRangeStart                 = calendar.getTime();

            calendar.add(Calendar.DAY_OF_MONTH, 28);
            Date dateRangeEnd                   = calendar.getTime();

            for (int i = 0; i < arrayLen; i++) {
                JSONObject jsonObjectJob        = jsonArrayResult.getJSONObject(i);

                Job job                         = new Job();
                Customer customer               = new Customer();
                Plot plot                       = new Plot();
                Vehicle vehicle                 = new Vehicle();
                Employee employee               = new Employee();

                String serverDateString         = jsonObjectJob.getString("assignedDate");

                try {
                    Date date                   = serverDateFormat.parse(serverDateString);
                    job.setAssignedDate(date);
                } catch(ParseException e) {
                    Log.d("skt", "Error" + e.getMessage());
                }

                customer.setName(jsonObjectJob.getString("customerName"));
                customer.setEmail(jsonObjectJob.getString("emailAddress"));
                customer.setPhone(jsonObjectJob.getString("phoneNumber"));

                String parkingLot               = jsonObjectJob.getString("parkingLotNumber");
                if (parkingLot != null) {
                    String[] parkingLotAndFlatNum  = parkingLot.split(Customer.PARKING_LOT_DELIMITER);
                    customer.setParkingLot(parkingLotAndFlatNum[0].trim());

                    if (parkingLotAndFlatNum.length > 1) {
                        customer.setFlatNumber(parkingLotAndFlatNum[1].trim());
                    }
                }

                vehicle.setModel(jsonObjectJob.getString("vehicleName"));
                vehicle.setMake("");

                employee.setName(jsonObjectJob.getString("assingedUserName"));
                employee.setId(jsonObjectJob.getInt("assingedUserId"));

                Date dateToday                  = new Date();
                job.setStartDate(dateToday);
                job.setStatus(jsonObjectJob.getString("status"));

                if (job.getStatus().equals(AppConstants.Job.STATUS_ASSIGNED)
                        && job.getAssignedDate().before(dateToday)) {
                    job.setStatus(AppConstants.Job.STATUS_PENDING);
                }

                if (job.getStatus().equals(AppConstants.Job.STATUS_DONE)) {
                    try {
                        Date startDate          = serverDateFormat.parse(jsonObjectJob.getString("jobStartDateTime"));
                        Date endDate            = serverDateFormat.parse(jsonObjectJob.getString("jobEndDateTime"));

                        job.setStartDate(startDate);
                        job.setEndDate(endDate);

                        String durationString   = AppController.getTimeDurationBetweenDates(startDate, endDate)[0];
                        job.setTimeDuration(durationString.trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                job.setId(jsonObjectJob.getLong("id"));

                customer.setVehicle(vehicle);
                customer.setPlotDetails(plot);
                job.setCustomer(customer);
                job.setAssignedEmployee(employee);

                if (arrayList == null)
                    arrayList                   = new ArrayList<>();

                //Add for 2 weeks
                if (job.getAssignedDate() != null
                        && ( !job.getAssignedDate().before(dateRangeStart)
                                && !job.getAssignedDate().after(dateRangeEnd))) {
                    arrayList.add(job);
                } else {
                    arrayList.add(job);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrayList;
    }

    private boolean validateToken() {
        Log.d("skt", "[ServiceController] response code--" + getResponseCode());
        return getResponseCode() != 401;
    }

    private boolean tryToLoginAgain(Context context) {
        Log.d("skt", "[ServiceController] Trying to relogin");
        int MAX_RETRY_COUNT                     = 3;
        int currentCount                        = 0;

        boolean loginSuccess                    = false;
        while (currentCount < MAX_RETRY_COUNT) {
            currentCount++;
            loginSuccess                        = reLogin(context);

            Log.d("skt", "[ServiceController] Is login success in re login --" + loginSuccess);

            if (loginSuccess)
                break;
        }
        return loginSuccess;
    }

    private boolean reLogin(Context context) {
        Log.d("skt", "[ServiceController] Trying to re login inside while loop");
        AppPrefs appPrefs                       = new AppPrefs(context);
        LoginUser loginUser                     = new LoginUser();
        loginUser.setUsername(appPrefs.getUsername());
        loginUser.setPassword(appPrefs.getPassword());

        LoggedInUser loggedInUser               = authenticateUser(context, loginUser);

        boolean isSuccess                       = loggedInUser != null
                                                        && loggedInUser.getToken().length() > 0
                                                        && getResponseCode() != 401;

        if (isSuccess) {
            appPrefs.setToken(loggedInUser.getToken());
        }

        return isSuccess;
    }

    private void onFailureInAuthenticationAfterRepeatedTries(final Context context) {
        Log.d("skt", "[ServiceController] Could not re login hence signing out");
        //Time to handle results in the UI
        Handler handler                         = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent                   = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        }, 1000);
    }
}
