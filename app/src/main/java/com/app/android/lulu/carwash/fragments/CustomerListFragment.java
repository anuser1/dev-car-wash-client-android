package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.CustomerListAdapter;
import com.app.android.lulu.carwash.background.DeleteCustomerTask;
import com.app.android.lulu.carwash.background.GetCustomerListTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnElementClick;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Customer;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class CustomerListFragment extends CarWashFragmentBaseClass {

    private ListView listViewCustomers;
    private CustomerListAdapter adapter;
    private TextView textViewTitle;
    private ImageView imageViewTitle;
    private FloatingActionButton floatingActionButtonAddCustomer;

    private ArrayList<Customer> customerArrayList;

    public CustomerListFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        listViewCustomers                       = (ListView) view.findViewById(R.id.persons_list_persons);
        textViewTitle                           = (TextView) view.findViewById(R.id.textView_persons_title);
        imageViewTitle                          = (ImageView) view.findViewById(R.id.imageView_persons_title);
        floatingActionButtonAddCustomer         = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);

        return view;
    }

    @Override
    public void initListeners() {
        listViewCustomers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                checkIfDataIsToBeReturned(customerArrayList.get(position));
            }
        });

        listViewCustomers.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.showDeleteItemButton(position);
                return true;
            }
        });

        floatingActionButtonAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddCustomerFragment();
            }
        });
    }

    @Override
    public void initData() {
        textViewTitle.setText("Customers");
        imageViewTitle.setImageResource(R.mipmap.ic_customer);

        getCustomers();
    }

    @Override
    public void toDoOnDetach() {

    }

    private void checkIfDataIsToBeReturned(Customer customer) {
        Object dataToPassBetweenFragments       = ((MainActivity) getActivity()).getObjectDataToPassBetweenFragments();

        if (dataToPassBetweenFragments != null
                && dataToPassBetweenFragments instanceof String
                && dataToPassBetweenFragments.equals(AppConstants.DataPass.KEY_ADD_SUBSCRIPTION_CUSTOMER)) {
            ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(customer);
            getActivity().onBackPressed();
        } else {
            goToDetailsFragment(customer);
        }
    }


    private void goToDetailsFragment(Customer customer) {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"view_customer", customer};
            oFIL.onFragmentInteraction(data);
        }
    }

    private void goToAddCustomerFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_customer");
        }
    }

    private void getCustomers() {
        GetCustomerListTask task                = new GetCustomerListTask(getCurrentContext());
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList) {
                    //noinspection unchecked
                    customerArrayList           = (ArrayList<Customer>) o;

                    populateListView();
                }
            }

            @Override
            public void onError(Object o) {
                showConfirmToReload();
            }
        });
        task.setLoadingMessage("Getting customers...");
        //noinspection unchecked
        task.execute();
    }

    private void populateListView() {
        adapter                                 = new CustomerListAdapter(getCurrentContext(), customerArrayList);
        adapter.setOnDeleteClick(onElementClick);
        listViewCustomers.setAdapter(adapter);
    }

    private void showConfirmToReload() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        ConfirmDialogFragment cDialogFragment   = ConfirmDialogFragment.newInstance(
                "Error",
                "Could not get customer list. Do you want to try again?");
        cDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {

            }

            @Override
            public void onYes() {
                getCustomers();
            }

            @Override
            public void onNo() {
                onLoadTaskFailure();
            }

            @Override
            public void onCancel() {

            }
        });
        cDialogFragment.show(fm, "fragment_confirm");
    }

    private void onLoadTaskFailure() {
        getActivity().onBackPressed();
    }

    private void confirmDeleteCustomer(final int index) {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Delete?",
                "Are you sure to delete the customer?", "Yes", "No");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                deleteCustomerServiceCall(index);
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void deleteCustomerServiceCall(int index) {
        Customer customer                       = customerArrayList.get(index);
        DeleteCustomerTask task                 = new DeleteCustomerTask(getCurrentContext(), customer);
        task.setLoadingMessage("Deleting customer...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                getCustomers();
            }

            @Override
            public void onError(Object o) {
                showErrorInDeleteCustomerMessage();
            }
        });
        //noinspection unchecked
        task.execute();
    }

    private void showErrorInDeleteCustomerMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Failed",
                "Could not delete the customer. Try after some time (" + AppServiceErrorHelper.getErrorString() + ")");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {

            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private OnElementClick onElementClick       = new OnElementClick() {
        @Override
        public void onClick(int index) {
            confirmDeleteCustomer(index);
        }

        @Override
        public void onLongClick(int index) {

        }
    };
}
