package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Job;
import com.app.android.lulu.carwash.model.Vehicle;

import java.util.ArrayList;

/**
 * Created by U47736 on 9/18/2017.
 */

public class EmployeeJobListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Job> employeeJobs;
    private ArrayList<Job> oEmployeeJobs;

    private Context context;
    private LayoutInflater inflater;

    public EmployeeJobListAdapter(Context context, ArrayList<Job> employeeJobArrayList) {
        this.employeeJobs                       = employeeJobArrayList;
        this.context                            = context;
        inflater                                = LayoutInflater.from(context);

        oEmployeeJobs                           = new ArrayList<>();
        oEmployeeJobs.addAll(employeeJobArrayList);
    }

    @Override
    public int getCount() {
        return employeeJobs != null ? employeeJobs.size() : 0;
    }

    @Override
    public Job getItem(int i) {
        return (employeeJobs != null && employeeJobs.get(i) != null) ? employeeJobs.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if(view == null) {
            holder                              = new ViewHolder();

            view                                = inflater.inflate(R.layout.list_item_job, null);

            holder.vehicleName                  = (TextView) view.findViewById(R.id.textView_item_job_car_name);
            holder.parkingLot                   = (TextView) view.findViewById(R.id.textView_item_job_parking_lot);
            holder.plotDetails                  = (TextView) view.findViewById(R.id.textView_item_job_plot);

            view.setTag(holder);
        } else {
            holder                              = (ViewHolder) view.getTag();
        }

        Job job                                 = employeeJobs.get(pos);
        Customer customer                       = job != null ? job.getCustomer() : null;
        Vehicle vehicle                         = customer != null ? customer.getVehicle() : null;

        if (vehicle != null)
            holder.vehicleName.setText(vehicle.getModel());

        if (customer != null) {
            holder.plotDetails.setText(customer.getName());
            holder.parkingLot.setText(customer.getParkingLot());
        }

        holder.vehicleName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.plotDetails.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.parkingLot.setTextColor(ContextCompat.getColor(context, R.color.black));

        if (job.getStatus().equals(AppConstants.Job.STATUS_DONE))
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.green_mint));
        else if (job.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS))
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        else
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Job> filteredJobs     = new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredJobs.addAll(oEmployeeJobs);
                } else {
                    for (int i = 0; i < oEmployeeJobs.size(); i++) {
                        Job job                 = oEmployeeJobs.get(i);
                        String customerName     = job.getCustomer().getName();
                        if (customerName.toLowerCase().contains(constraint.toString())) {
                            filteredJobs.add(job);
                        }
                    }
                }

                results.count                   = filteredJobs.size();
                results.values                  = filteredJobs;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                employeeJobs                    = (ArrayList<Job>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        private TextView vehicleName;
        private TextView parkingLot;
        private TextView plotDetails;
    }
}
