package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.model.LoggedInUser;
import com.app.android.lulu.carwash.model.LoginUser;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginAsyncTask extends CarwashAsyncTaskBaseClass {

    private LoginUser loginUser;
    private Context context;
    public LoginAsyncTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        this.context                            = context;
        if (o[0] instanceof LoginUser) {
            this.loginUser                      = (LoginUser) o[0];
        }
    }

    @Override
    public Object tasksInBackground() {
        ServiceController controller            = new ServiceController();
        if (loginUser != null) {
            return controller.authenticateUser(context, loginUser);
        } else {
            return null;
        }
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        boolean isSuccess                       = o != null && o instanceof LoggedInUser;
        if (!isSuccess) {
            try {
                JSONObject errorResponseJson    = AppServiceErrorHelper.getJsonObjectError();
                if (errorResponseJson != null) {
                    JSONObject errorJsonObject  = errorResponseJson.getJSONObject("error");

                    if (errorJsonObject != null && errorJsonObject.has("message")) {
                        String errorMessage     = errorJsonObject.getString("message");
                        AppServiceErrorHelper.setErrorString(errorMessage);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
