package com.app.android.lulu.carwash.model;

import java.io.File;

public class EmployeeJob {
    private String fileName;
    private String employeeName;
    private long jobId;
    private long employeeId;
    private File fileOfVehicleImage;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public File getFileOfVehicleImage() {
        return fileOfVehicleImage;
    }

    public void setFileOfVehicleImage(File fileOfVehicleImage) {
        this.fileOfVehicleImage = fileOfVehicleImage;
    }
}
