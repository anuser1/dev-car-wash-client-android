package com.app.android.lulu.carwash.baseclasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;


public abstract class CarwashAsyncTaskBaseClass extends AsyncTask {

    private ProgressDialog progressDialog;

    private Context context;

    private String message;

    private OnTaskResult onTaskResult;

    public CarwashAsyncTaskBaseClass(Context context, Object... objects) {
        this.context                            = context;
        initTask(context, objects);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog                          = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        return tasksInBackground();
    }

    @Override
    protected void onPostExecute(Object o) {
        boolean isSuccess                       = onTaskCompletion(o);
        progressDialog.dismiss();

        if (onTaskResult != null) {
            if (isSuccess) {
                onTaskResult.onSuccess(o);
            } else {
                onTaskResult.onError(o);
            }
        }
        //noinspection unchecked
        super.onPostExecute(o);
    }

    public abstract void initTask(Context context, Object... o);

    public abstract Object tasksInBackground();
    public abstract boolean onTaskCompletion(Object o);
    public void setLoadingMessage(String message) {
        this.message                            = message;
    }

    public void setOnTaskResult(OnTaskResult onTaskResult) {
        this.onTaskResult                       = onTaskResult;
    }

    public Context getContext() {
        return context;
    }
}
