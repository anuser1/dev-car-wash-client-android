package com.app.android.lulu.carwash.constants;


public class UrlConstants {
    private static final String BASE_URL            = "http://8a3c62e0.ngrok.io/";//http://192.168.225.237:8070/--//http://cleanz.azurewebsites.net/--//http://e1656893.ngrok.io/

    public static final String LOGIN_URL            = BASE_URL + "api/account/authenticate";
    public static final String GET_EMPLOYEES        = BASE_URL + "api/services/app/user/employees";
    public static final String GET_SUPERVISORS      = BASE_URL + "api/services/app/user/supervisors";
    public static final String UPDATE_EMPLOYEE      = BASE_URL + "api/services/app/user/Update";
    public static final String GET_PLOTS            = BASE_URL + "api/services/app/plot/getall";
    public static final String ADD_PLOT             = BASE_URL + "api/services/app/plot/add";
    public static final String UPDATE_PLOT          = BASE_URL + "api/services/app/plot/update";
    public static final String GET_CUSTOMERS        = BASE_URL + "api/services/app/customer/getall";
    public static final String ADD_CUSTOMER         = BASE_URL + "api/services/app/customer/add";
    public static final String EDIT_CUSTOMER        = BASE_URL + "api/services/app/customer/update";
    public static final String ADD_EMPLOYEE         = BASE_URL + "api/services/app/user/addemployee";
    public static final String ADD_SUPERVISOR       = BASE_URL + "api/services/app/user/addsupervisor";
    public static final String CHANGE_PASSWORD      = BASE_URL + "api/services/app/user/changepassword";
    public static final String ADD_SUBSCRIPTION     = BASE_URL + "api/services/app/subscription/add";
    public static final String GET_ALL_JOBS         = BASE_URL + "api/services/app/job/getall";
    public static final String GET_ALL_SUBSCRIPTION = BASE_URL + "api/services/app/subscription/getall";
    public static final String ASSIGN_SUBSCRIPTION  = BASE_URL + "api/services/app/subscription/assign";
    public static final String GET_ASSIGNED_JOBS    = BASE_URL + "api/services/app/job/myjobs";
    public static final String UPLOAD_IMAGE_JOB     = BASE_URL + "api/services/app/job/uploadimage";
    public static final String UPDATE_JOB_STATUS    = BASE_URL + "api/services/app/job/updatestatus";
    public static final String DELETE_CUSTOMER      = BASE_URL + "api/services/app/customer/delete?Id=";
    public static final String GET_SUBSCRIPTION_DET = BASE_URL + "api/services/app/subscription/get?Id=";
    public static final String UPDATE_SUBSCRIPTION  = BASE_URL + "api/services/app/subscription/update";
}
