package com.app.android.lulu.carwash.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.background.LoginAsyncTask;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.LoggedInUser;
import com.app.android.lulu.carwash.model.LoginUser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LoginActivity extends AppCompatActivity {

    private Button buttonLogin;
    private TextView textViewLogo;
    private EditText editTextUsername;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        initListeners();
        initData();
    }

    private void initViews() {
        buttonLogin                             = (Button) findViewById(R.id.button_login_login);
        textViewLogo                            = (TextView) findViewById(R.id.textView_login_logo);
        editTextUsername                        = (EditText) findViewById(R.id.editText_login_username);
        editTextPassword                        = (EditText) findViewById(R.id.editText_login_password);
    }

    private void initListeners() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginButtonClick();
            }
        });
    }

    private void initData() {
        textViewLogo.setTextColor(ContextCompat.getColor(this, R.color.blue_text));

        editTextUsername.setText("spa");//spa -- Admin //cz0106 - Supervisor //cz0103 - Employee
        editTextPassword.setText("123456");//123456
    }

    private void onLoginButtonClick() {
        String username                         = editTextUsername.getText().toString();
        String password                         = editTextPassword.getText().toString();

        if (TextUtils.isEmpty(username)) {
            editTextUsername.setError("Enter username");
        } else if (TextUtils.isEmpty(password)) {
            editTextPassword.setError("Enter password");
        } else {
            login(username, password);
        }
    }

    private void login(String username, String password) {
        LoginUser loginUser                     = new LoginUser();
        loginUser.setUsername(username);
        loginUser.setPassword(password);

        LoginAsyncTask loginAsyncTask           = new LoginAsyncTask(this, loginUser);
        loginAsyncTask.setLoadingMessage("Signing in...");
        loginAsyncTask.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o instanceof LoggedInUser)
                    showHomePage((LoggedInUser) o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(LoginActivity.this, "Could not login. Please check your credentials", Toast.LENGTH_LONG).show();
            }
        });
        //noinspection unchecked
        loginAsyncTask.execute();

    }

    private void showHomePage(LoggedInUser loggedInUser) {
        final Set<String> VALUES                = new HashSet<>(Arrays.asList(loggedInUser.getRole()));
        String role                             = AppConstants.User.EMPLOYEE;
        if (VALUES.contains(AppConstants.User.SUPER_ADMIN)) {
            role                                = AppConstants.User.SUPER_ADMIN;
        } else if (VALUES.contains(AppConstants.User.SUPERVISOR)) {
            role                                = AppConstants.User.SUPERVISOR;
        }

        Intent intent                           = new Intent(this, MainActivity.class);
        intent.putExtra(AppConstants.DataPass.KEY_LOGIN_ROLE, role);
        startActivity(intent);
        finish();
    }

}
