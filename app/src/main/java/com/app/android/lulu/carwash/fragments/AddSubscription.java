package com.app.android.lulu.carwash.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.PackagesSpinnerAdapter;
import com.app.android.lulu.carwash.background.AddSubscriptionTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.controllers.AppController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Package;
import com.app.android.lulu.carwash.model.Plot;
import com.app.android.lulu.carwash.model.Subscription;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class AddSubscription extends CarWashFragmentBaseClass {

    private Spinner spinnerPackages;
    private ImageView imageViewDateSelect;
    private TextView textViewDate;
    private Switch switchActive;
    private TextView textViewActive;
    private TextView textViewCustomerName;
    private TextView textViewPlot;
    private ImageView imageViewCustomerSelect;
    private ImageView imageViewPlotSelect;
    private EditText editTextNumberOfWeeks;
    private EditText editTextParkingLot;
    private ToggleButton toggleButtonSunday;
    private ToggleButton toggleButtonMonday;
    private ToggleButton toggleButtonTuesday;
    private ToggleButton toggleButtonWednesday;
    private ToggleButton toggleButtonThursday;
    private ToggleButton toggleButtonFriday;
    private ToggleButton toggleButtonSaturday;
    private CheckBox checkBoxPayment;
    private Button buttonSave;

    private Subscription subscriptionToBeCreated;

    private Date dateStart;
    private boolean isInitialUiSetup            = true;

    private PackagesSpinnerAdapter spinnerAdapter;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_subscription_add, container, false);

        spinnerPackages                         = (Spinner) view.findViewById(R.id.spinner_subscription_add_packages);
        imageViewDateSelect                     = (ImageView) view.findViewById(R.id.imageView_subscription_add_date_select);
        textViewDate                            = (TextView) view.findViewById(R.id.textView_subscription_add_date);
        textViewActive                          = (TextView) view.findViewById(R.id.textView_subscription_add_active);
        switchActive                            = (Switch) view.findViewById(R.id.switch_subscription_add_active);
        textViewCustomerName                    = (TextView) view.findViewById(R.id.textView_subscription_add_customer_name);
        imageViewCustomerSelect                 = (ImageView) view.findViewById(R.id.imageView_subscription_add_select_customer);
        imageViewPlotSelect                     = (ImageView) view.findViewById(R.id.imageView_subscription_add_select_plot);
        textViewPlot                            = (TextView) view.findViewById(R.id.textView_subscription_add_plot);
        checkBoxPayment                         = (CheckBox) view.findViewById(R.id.checkBox_subscription_add_payment);
        editTextNumberOfWeeks                   = (EditText) view.findViewById(R.id.editText_subscription_add_weeks);
        editTextParkingLot                      = (EditText) view.findViewById(R.id.editText_subscription_add_parking_lot);
        toggleButtonSunday                      = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_sunday);
        toggleButtonMonday                      = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_monday);
        toggleButtonTuesday                     = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_tuesday);
        toggleButtonWednesday                   = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_wednesday);
        toggleButtonThursday                    = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_thursday);
        toggleButtonFriday                      = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_friday);
        toggleButtonSaturday                    = (ToggleButton) view.findViewById(R.id.toggleButton_add_subscription_saturday);
        buttonSave                              = (Button) view.findViewById(R.id.button_add_subscription_save);

        return view;
    }

    @Override
    public void initListeners() {
        editTextParkingLot.setKeyListener(null);
        imageViewDateSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        switchActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onSwitchActiveCheckedChange();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });

        toggleButtonMonday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonMonday, isChecked);
            }
        });

        toggleButtonTuesday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonTuesday, isChecked);
            }
        });

        toggleButtonWednesday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonWednesday, isChecked);
            }
        });
        toggleButtonThursday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonThursday, isChecked);
            }
        });
        toggleButtonFriday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonFriday, isChecked);
            }
        });
        toggleButtonSaturday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonSaturday, isChecked);
            }
        });
        toggleButtonSunday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectToggle(toggleButtonSunday, isChecked);
            }
        });

        spinnerPackages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onSpinnerSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imageViewPlotSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPlotDetails();
            }
        });

        imageViewCustomerSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCustomerDetails();
            }
        });

    }

    @Override
    public void initData() {
        getPackageList();

        Package packageOfWash                   = (Package) spinnerAdapter.getItem(0);
        spinnerPackages.setTag(getTaggingPositionForSpinner(packageOfWash.getPackageText()));

        if (subscriptionToBeCreated == null) {
            subscriptionToBeCreated             = new Subscription();
        }

        textViewDate.setText(changeDateFormat(subscriptionToBeCreated.getStartDate()));
        if (subscriptionToBeCreated.getNumberOfMonths() != 0) {
            String numberOfWeeks                = String.valueOf(Math.round(subscriptionToBeCreated.getNumberOfMonths() * 4));
            setTextInEditText(editTextNumberOfWeeks, numberOfWeeks);
        }

        selectDays(subscriptionToBeCreated.getSelectedDays());

        Object dataObject                   = ((MainActivity) getActivity()).getObjectDataToPassBetweenFragments();
        if (dataObject != null && dataObject instanceof Plot) {
            subscriptionToBeCreated.setPlot((Plot) dataObject);
            ((MainActivity) getActivity()).setObjectDataToPassBetweenFragments(null);
        } else if (dataObject != null && dataObject instanceof Customer) {
            Customer customerSelected       = (Customer) dataObject;
            subscriptionToBeCreated.setCustomer(customerSelected);
            subscriptionToBeCreated.setPlot(customerSelected.getPlotDetails());
            ((MainActivity) getActivity()).setObjectDataToPassBetweenFragments(null);
        }

        if (subscriptionToBeCreated.getPlot() != null) {
            textViewPlot.setText(subscriptionToBeCreated.getPlot().getName());
        }

        if (subscriptionToBeCreated.getCustomer() != null) {
            textViewCustomerName.setText(subscriptionToBeCreated.getCustomer().getName());
            String parkingLot                   = subscriptionToBeCreated.getCustomer().getParkingLot();
            setTextInEditText(editTextParkingLot, parkingLot);
        }

        checkBoxPayment.setChecked(subscriptionToBeCreated.isPaymentReceived());

        Handler handler                         = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isInitialUiSetup                = false;
            }
        }, 1000);
    }

    @Override
    public void toDoOnDetach() {

    }

    private void getPackageList() {
        String[] packageList                    = AppConstants.Subscription.getAllPackages();

        int length                              = packageList.length;

        ArrayList<Package> packageArrayList     = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            Package aPackage                    = new Package();
            aPackage.setPackageText(packageList[i]);
            aPackage.setPackageId(i);
            packageArrayList.add(aPackage);
        }

        spinnerAdapter                          = new PackagesSpinnerAdapter(getCurrentContext(), packageArrayList);
        spinnerPackages.setAdapter(spinnerAdapter);

        onSpinnerSelection(0);
    }

    private void showDatePicker() {
        DatePickerFragment date                 = new DatePickerFragment();

        Calendar calender                       = Calendar.getInstance();
        Bundle args                             = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        date.setCallBack(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                Calendar calendar               = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);

                dateStart                       = calendar.getTime();
                SimpleDateFormat dateFormat     = new SimpleDateFormat("dd MMM, yyyy", Locale.US);

                textViewDate.setText(dateFormat.format(dateStart));
            }
        });
        date.show(((AppCompatActivity)getCurrentContext()).getSupportFragmentManager(), "Date Picker");
    }

    private void onSwitchActiveCheckedChange() {
        if (!switchActive.isChecked()) {
            textViewActive.setText("Not active");
        } else {
            textViewActive.setText("Active");
        }
    }

    private void onSpinnerSelection(int selectedPos) {
        int taggingPos                          = getTaggingPositionForSpinner(
                                                        AppConstants.Subscription.getAllPackages()[selectedPos]);
        Object oTag                             = spinnerPackages.getTag();
        if (oTag instanceof Integer) {
            int tag                             = (int) oTag;
            if (taggingPos == tag) {
                return;
            }
        }

        spinnerPackages.setTag(taggingPos);

        if (taggingPos == 7) {
            selectAllToggleButtons();
            editTextNumberOfWeeks.setEnabled(true);
        } else {
            unSelectAllToggleButtons();
            if (taggingPos == 0) {
                setTextInEditText(editTextNumberOfWeeks, "0");
                editTextNumberOfWeeks.setEnabled(false);
            } else {
                editTextNumberOfWeeks.setEnabled(true);
            }
        }

    }

    private void onSaveButtonClick() {
        int selectedPositionTagOfSpinner        = (int) spinnerPackages.getTag();
        if (subscriptionToBeCreated == null) {
            Toast.makeText(getCurrentContext(), "Fill complete details", Toast.LENGTH_LONG).show();
        } else if (subscriptionToBeCreated.getCustomer() == null) {
            Toast.makeText(getCurrentContext(), "Select customer details", Toast.LENGTH_LONG).show();
        } else  if (subscriptionToBeCreated.getPlot() == null) {
            Toast.makeText(getCurrentContext(), "Select plot details", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(editTextNumberOfWeeks.getText().toString())
                || (editTextNumberOfWeeks.getText().toString().equals("0")
                        && selectedPositionTagOfSpinner != 0)) {
            //It's not a single wash
            editTextNumberOfWeeks.setError("Add number of weeks");
        } else if (dateStart == null) {
            Toast.makeText(getCurrentContext(), "Select start date", Toast.LENGTH_LONG).show();
        } else {
            validateToggleButtonAndSaveSubscription();
        }
    }

    private void validateToggleButtonAndSaveSubscription() {
        int selectedNumberOfWash                = (int) spinnerPackages.getTag();
        int selectedDaysCount                   = getCountOfSelectedToggleButtons();

        if (selectedDaysCount != selectedNumberOfWash) {
            Toast.makeText(getCurrentContext(), "Select days of wash or change wash per week value", Toast.LENGTH_LONG).show();
        } else {
            if (subscriptionToBeCreated == null)
                subscriptionToBeCreated         = new Subscription();

            if (subscriptionToBeCreated.getCustomer() != null)
                subscriptionToBeCreated.getCustomer().setParkingLot(editTextParkingLot.getText().toString());

            String numberOfWeeksStr             = editTextNumberOfWeeks.getText().toString();
            SimpleDateFormat serverDateFormat   = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            subscriptionToBeCreated.setStartDate(serverDateFormat.format(dateStart));
            subscriptionToBeCreated.setNumberOfMonths(Float.parseFloat(numberOfWeeksStr) / 4);

            Calendar calendar                   = Calendar.getInstance();
            if (dateStart != null) {
                calendar.setTime(dateStart);
            }

            //Because if it is unchecked then next payment date is start date.
            //If start date is empty (null) the next payment date is set as today
            if (!checkBoxPayment.isChecked()) {
                subscriptionToBeCreated.setNextPaymentDate(serverDateFormat.format(calendar.getTime()));
            }

            calendar.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(numberOfWeeksStr));

            subscriptionToBeCreated.setEndDate(serverDateFormat.format(calendar.getTime()));

            String[] selectedDates              = AppController.getDatesBetweenRange(
                                                        dateStart, calendar.getTime(), getSelectedDays());
            int selectedSpinnerPos              = (int) spinnerPackages.getTag();
            if (selectedSpinnerPos == 0) {
                selectedDates                   = new String[1];
                SimpleDateFormat tempDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                selectedDates[0]                = tempDateFormat.format(dateStart);
            }
            subscriptionToBeCreated.setSelectedDates(selectedDates);

            if (checkBoxPayment.isChecked()) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);

                //If check box is checked then next payment date is 1 day after end date
                subscriptionToBeCreated.setNextPaymentDate(serverDateFormat.format(calendar.getTime()));
            }

            subscriptionToBeCreated.setWashPerWeek((int)spinnerPackages.getTag());
            subscriptionToBeCreated.setSelectedDays(getSelectedDays());


            addSubscription(subscriptionToBeCreated);
        }
    }

    private void selectToggle(ToggleButton toggleButton, boolean isChecked) {
        if (!isInitialUiSetup) {
            Object objectTag                    = spinnerPackages.getTag();
            if (objectTag != null && objectTag instanceof Integer) {
                int tag                         = (int) objectTag;
                int selectedCount               = getCountOfSelectedToggleButtons();

                if (tag == 7) {
                    toggleButton.setChecked(true);
                } else if (selectedCount <= tag) {
                    toggleButton.setChecked(isChecked);
                } else {
                    toggleButton.setChecked(false);
                }
            }
        }
    }

    private int getCountOfSelectedToggleButtons() {
        int count                               = 0;//Because the toggle button is already in not selected state
        if (toggleButtonSunday.isChecked())
            count++;
        if (toggleButtonMonday.isChecked())
            count++;
        if (toggleButtonTuesday.isChecked())
            count++;
        if (toggleButtonWednesday.isChecked())
            count++;
        if (toggleButtonThursday.isChecked())
            count++;
        if (toggleButtonFriday.isChecked())
            count++;
        if (toggleButtonSaturday.isChecked())
            count++;
        return count;
    }

    private void selectAllToggleButtons() {
        isInitialUiSetup                        = true;

        toggleButtonSunday.setChecked(true);
        toggleButtonMonday.setChecked(true);
        toggleButtonTuesday.setChecked(true);
        toggleButtonWednesday.setChecked(true);
        toggleButtonWednesday.setChecked(true);
        toggleButtonThursday.setChecked(true);
        toggleButtonFriday.setChecked(true);
        toggleButtonSaturday.setChecked(true);

        toggleButtonSunday.setSelected(true);
        toggleButtonMonday.setSelected(true);
        toggleButtonTuesday.setSelected(true);
        toggleButtonWednesday.setSelected(true);
        toggleButtonWednesday.setSelected(true);
        toggleButtonThursday.setSelected(true);
        toggleButtonFriday.setSelected(true);
        toggleButtonSaturday.setSelected(true);

        isInitialUiSetup                        = false;
    }

    private void unSelectAllToggleButtons() {
        isInitialUiSetup                        = true;

        toggleButtonSunday.setChecked(false);
        toggleButtonMonday.setChecked(false);
        toggleButtonTuesday.setChecked(false);
        toggleButtonWednesday.setChecked(false);
        toggleButtonThursday.setChecked(false);
        toggleButtonFriday.setChecked(false);
        toggleButtonSaturday.setChecked(false);

        toggleButtonSunday.setSelected(false);
        toggleButtonMonday.setSelected(false);
        toggleButtonTuesday.setSelected(false);
        toggleButtonWednesday.setSelected(false);
        toggleButtonThursday.setSelected(false);
        toggleButtonFriday.setSelected(false);
        toggleButtonSaturday.setSelected(false);

        isInitialUiSetup                        = false;
    }

    private int getTaggingPositionForSpinner(String text) {
        switch (text) {
            case AppConstants.Subscription.WEEKLY_1:
                return 1;
            case AppConstants.Subscription.WEEKLY_2:
                return 2;
            case AppConstants.Subscription.WEEKLY_3:
                return 3;
            case AppConstants.Subscription.WEEKLY_4:
                return 4;
            case AppConstants.Subscription.WEEKLY_5:
                return 5;
            case AppConstants.Subscription.WEEKLY_6:
                return 6;
            case AppConstants.Subscription.WEEKLY_7:
                return 7;
            case AppConstants.Subscription.SINGLE_WASH:
                return 0;
            default:
                return 7;
        }
    }

    private String[] getSelectedDays() {
        ArrayList<String> days                  = new ArrayList<>();
        if (toggleButtonMonday.isChecked()) {
            days.add("Mon");
        }
        if (toggleButtonTuesday.isChecked()) {
            days.add("Tue");
        }
        if (toggleButtonWednesday.isChecked()) {
            days.add("Wed");
        }
        if (toggleButtonThursday.isChecked()) {
            days.add("Thu");
        }
        if (toggleButtonFriday.isChecked()) {
            days.add("Fri");
        }
        if (toggleButtonSaturday.isChecked()) {
            days.add("Sat");
        }
        if (toggleButtonSunday.isChecked()) {
            days.add("Sun");
        }

        String[] daysArr                        = new String[days.size()];
        daysArr                                 = days.toArray(daysArr);

        return daysArr;
    }

    private void selectDays(String[] daysArr) {
        if (daysArr != null) {
            ArrayList<String> days              = new ArrayList<>();
            days.addAll(Arrays.asList(daysArr));

            toggleButtonMonday.setChecked(days.contains("Mon"));
            toggleButtonTuesday.setChecked(days.contains("Tue"));
            toggleButtonWednesday.setChecked(days.contains("Wed"));
            toggleButtonThursday.setChecked(days.contains("Thu"));
            toggleButtonFriday.setChecked(days.contains("Fri"));
            toggleButtonSaturday.setChecked(days.contains("Sat"));
            toggleButtonSunday.setChecked(days.contains("Sun"));

            toggleButtonMonday.setSelected(days.contains("Mon"));
            toggleButtonTuesday.setSelected(days.contains("Tue"));
            toggleButtonWednesday.setSelected(days.contains("Wed"));
            toggleButtonThursday.setSelected(days.contains("Thu"));
            toggleButtonFriday.setSelected(days.contains("Fri"));
            toggleButtonSaturday.setSelected(days.contains("Sat"));
            toggleButtonSunday.setSelected(days.contains("Sun"));
        }
    }

    private void getPlotDetails() {
        SimpleDateFormat dateFormat             = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String numberOfWeeksStr                 = editTextNumberOfWeeks.getText().toString();
        if (subscriptionToBeCreated == null) {
            subscriptionToBeCreated             = new Subscription();
        }

        subscriptionToBeCreated.setPaymentReceived(checkBoxPayment.isChecked());

        if (dateStart != null)
            subscriptionToBeCreated.setStartDate(dateFormat.format(dateStart));

        subscriptionToBeCreated.setWashPerWeek((int) spinnerPackages.getTag());
        subscriptionToBeCreated.setSelectedDays(getSelectedDays());

        if (!TextUtils.isEmpty(numberOfWeeksStr)) {
            subscriptionToBeCreated.setNumberOfMonths(Float.parseFloat(editTextNumberOfWeeks.getText().toString()) / 4);
            Calendar calendar                   = Calendar.getInstance();
            if (dateStart != null) {
                calendar.setTime(dateStart);
            }

            //Because if it is unchecked then next payment date is start date.
            //If start date is empty (null) the next payment date is set as today
            if (!checkBoxPayment.isChecked()) {
                subscriptionToBeCreated.setNextPaymentDate(dateFormat.format(calendar.getTime()));
            }

            calendar.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(numberOfWeeksStr));

            subscriptionToBeCreated.setEndDate(dateFormat.format(calendar.getTime()));

            if (checkBoxPayment.isChecked()) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);

                //If check box is checked then next payment date is 1 day after end date
                subscriptionToBeCreated.setNextPaymentDate(dateFormat.format(calendar.getTime()));
            }
        }

        ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(AppConstants.DataPass.KEY_ADD_SUBSCRIPTION_PLOT);
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"get_plot_for_user", subscriptionToBeCreated};
            oFIL.onFragmentInteraction(data);
        }
    }

    private void getCustomerDetails() {
        SimpleDateFormat dateFormat             = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String numberOfWeeksStr                 = editTextNumberOfWeeks.getText().toString();
        if (subscriptionToBeCreated == null) {
            subscriptionToBeCreated             = new Subscription();
        }

        subscriptionToBeCreated.setPaymentReceived(checkBoxPayment.isChecked());

        if (dateStart != null)
            subscriptionToBeCreated.setStartDate(dateFormat.format(dateStart));

        subscriptionToBeCreated.setWashPerWeek((int) spinnerPackages.getTag());
        subscriptionToBeCreated.setSelectedDays(getSelectedDays());

        if (!TextUtils.isEmpty(numberOfWeeksStr)) {
            subscriptionToBeCreated.setNumberOfMonths(Float.parseFloat(editTextNumberOfWeeks.getText().toString()) / 4);
            Calendar calendar                   = Calendar.getInstance();
            if (dateStart != null) {
                calendar.setTime(dateStart);
            }

            //Because if it is unchecked then next payment date is start date.
            //If start date is empty (null) the next payment date is set as today
            if (!checkBoxPayment.isChecked()) {
                subscriptionToBeCreated.setNextPaymentDate(dateFormat.format(calendar.getTime()));
            }

            calendar.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(numberOfWeeksStr));

            subscriptionToBeCreated.setEndDate(dateFormat.format(calendar.getTime()));

            if (checkBoxPayment.isChecked()) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);

                //If check box is checked then next payment date is 1 day after end date
                subscriptionToBeCreated.setNextPaymentDate(dateFormat.format(calendar.getTime()));
            }
        }

        ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(AppConstants.DataPass.KEY_ADD_SUBSCRIPTION_CUSTOMER);
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"get_customer_for_user", subscriptionToBeCreated};
            oFIL.onFragmentInteraction(data);
        }
    }

    private String changeDateFormat(String serverDateFormatString) {
        String appDateFormatString              = "";

        if (serverDateFormatString != null && serverDateFormatString.length() > 0) {
            SimpleDateFormat serverDateFormat   = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat appDateFormat      = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());

            try {
                Date date                       = serverDateFormat.parse(serverDateFormatString);
                appDateFormatString             = appDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return appDateFormatString;
    }

    private void setTextInEditText(final EditText editText, final String text) {
        editText.post(new Runnable() {
            @Override
            public void run() {
                editText.setText(text);
            }
        });
    }

    private void addSubscription(Subscription subscription) {
        AddSubscriptionTask task                = new AddSubscriptionTask(getCurrentContext(), subscription);
        task.setLoadingMessage("Saving details...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                showSuccessMessage();
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Created",
                "New subscription created successfully.");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess();
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess() {
        getActivity().onBackPressed();
    }
}
