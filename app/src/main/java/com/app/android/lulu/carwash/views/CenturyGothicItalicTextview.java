package com.app.android.lulu.carwash.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Santhosh on 13-09-2017.
 */

@SuppressLint("AppCompatCustomView")
public class CenturyGothicItalicTextview extends TextView {
    private static Typeface mTypeface;

    public CenturyGothicItalicTextview(Context context) {
        super(context);

        init(context);
    }

    public CenturyGothicItalicTextview(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public CenturyGothicItalicTextview(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CenturyGothicItalicTextview(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(context);
    }

    private void init(Context context) {
        if (isInEditMode()) {
            return;
        }
        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/century_gothic_italic.ttf");
        }
        setTypeface(mTypeface);
        setTextColor(0xffffffff);
    }
}
