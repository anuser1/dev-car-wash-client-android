package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.EmployeeListAdapter;
import com.app.android.lulu.carwash.background.GetEmployeesTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Employee;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class EmployeeListFragment extends CarWashFragmentBaseClass {

    private ListView listViewEmployees;
    private FloatingActionButton floatingActionButtonAddEmployee;

    private ArrayList<Employee> employeeArrayList;
    private EmployeeListAdapter adapter;

    public EmployeeListFragment() {
    }

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        listViewEmployees                       = (ListView) view.findViewById(R.id.persons_list_persons);
        floatingActionButtonAddEmployee         = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);

        return view;
    }

    @Override
    public void initListeners() {
        listViewEmployees.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Employee employee               = employeeArrayList.get(position);
                checkIfDataIsToBeReturned(employee);
            }
        });

        floatingActionButtonAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddEmployeeFragment();
            }
        });
    }

    @Override
    public void initData() {
        getAllEmployeesAndSuperVisors();
    }

    @Override
    public void toDoOnDetach() {

    }

    private void goToAddEmployeeFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_employee");
        }
    }

    private void checkIfDataIsToBeReturned(Employee employee) {
        Object dataToPassBetweenFragments       = ((MainActivity) getActivity()).getObjectDataToPassBetweenFragments();

        if (dataToPassBetweenFragments != null
                && dataToPassBetweenFragments instanceof String
                && dataToPassBetweenFragments.equals(AppConstants.DataPass.KEY_ASSIGN_SUBSCRIPTION_EMPLOYEE)) {
            ((MainActivity)getActivity()).updateDataInFragment(employee);
        } else {
            goToDetailsFragment(employee);
        }
    }

    private void goToDetailsFragment(Employee employee) {
        Object o                                = new Object[]{"superadmin_employee_details", employee};
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onFragmentInteraction(o);
        }
    }

    private void getAllEmployeesAndSuperVisors() {
        GetEmployeesTask task                   = new GetEmployeesTask(getCurrentContext());
        task.setLoadingMessage("Getting employees...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList) {
                    //noinspection unchecked
                    employeeArrayList           = (ArrayList<Employee>) o;

                    adapter                     = new EmployeeListAdapter(getCurrentContext(), employeeArrayList);
                    listViewEmployees.setAdapter(adapter);
                }
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        //noinspection unchecked
        task.execute();
    }
}
