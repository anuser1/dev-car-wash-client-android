package com.app.android.lulu.carwash.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.app.android.lulu.carwash.interfaces.OnDialogAction;

public class AlertDialogFragment extends DialogFragment {

    private OnDialogAction onDialogAction;

    public AlertDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    public static AlertDialogFragment newInstance(String... params) {
        AlertDialogFragment frag                = new AlertDialogFragment();
        Bundle args                             = new Bundle();

        args.putString("title", params[0]);
        args.putString("message", params[1]);
        if (params.length > 2) {
            args.putString("ok_text", params[2]);
        }
        if (params.length > 3) {
            args.putString("cancel_text", params[3]);
        }
        frag.setArguments(args);

        return frag;
    }

    public void setOnDialogAction(OnDialogAction onDialogAction) {
        this.onDialogAction                     = onDialogAction;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title                            = getArguments().getString("title");
        String message                          = getArguments().getString("message");
        String positiveButtonText               = "OK";
        String negativeButtonText               = "";
        if (getArguments().containsKey("ok_text")) {
            positiveButtonText                  = getArguments().getString("ok_text");
        }
        if (getArguments().containsKey("cancel_text")) {
            negativeButtonText                  = getArguments().getString("cancel_text");
        }
        AlertDialog.Builder alertDialogBuilder  = new AlertDialog.Builder(getActivity());

        if (title != null && title.length() > 0)
            alertDialogBuilder.setTitle(title);

        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(positiveButtonText,  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                if (onDialogAction!= null)
                    onDialogAction.onOk();
            }
        });

        if (negativeButtonText!= null && negativeButtonText.length() > 0) {
            alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (onDialogAction!= null)
                        onDialogAction.onCancel();
                }
            });
        }
        alertDialogBuilder.setCancelable(false);
        return alertDialogBuilder.create();
    }
}
