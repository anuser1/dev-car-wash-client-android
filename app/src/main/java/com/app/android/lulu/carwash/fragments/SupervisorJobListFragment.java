package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.adapters.JobStatusSpinnerAdapter;
import com.app.android.lulu.carwash.adapters.SupervisorJobListAdapter;
import com.app.android.lulu.carwash.background.GetAllJobsTask;
import com.app.android.lulu.carwash.background.SortJobArrayTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Job;
import com.app.android.lulu.carwash.model.JobStatus;

import java.util.ArrayList;
import java.util.HashMap;


public class SupervisorJobListFragment extends CarWashFragmentBaseClass {

    private TextView textViewLoading;
    private ListView listViewJobs;
    private Spinner spinnerStatusFilter;
    private FloatingActionButton floatingActionButtonAddJob;
    private RelativeLayout relativeLayoutLoadingContainer;
    private SearchView searchViewJobs;

    private JobStatusSpinnerAdapter spinnerAdapter;
    private SupervisorJobListAdapter listAdapterJobs;

    private ArrayList<JobStatus> jobStatusArrayList;
    private ArrayList<Job> jobsArrayList;
    private HashMap<String, ArrayList<Job>> hashMapSortedAndGrouped;

    private boolean flag_loading                = false;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_jobs, container, false);
        listViewJobs                            = (ListView) view.findViewById(R.id.listView_jobs_job);
        spinnerStatusFilter                     = (Spinner) view.findViewById(R.id.spinner_jobs_filter);
        floatingActionButtonAddJob              = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_jobs_add);
        relativeLayoutLoadingContainer          = (RelativeLayout) view.findViewById(R.id.relativeLayout_jobs_loading_container);
        textViewLoading                         = (TextView) view.findViewById(R.id.textView_jobs_loading);
        searchViewJobs                          = (SearchView) view.findViewById(R.id.searchView_jobs_search);

        return view;
    }

    @Override
    public void initListeners() {
        floatingActionButtonAddJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSubscriptionAddFragment();
            }
        });

        listViewJobs.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (totalItemCount != 0 && firstVisibleItem + visibleItemCount == (totalItemCount - 5)) {
                    if (!flag_loading) {
                        flag_loading = true;
                    }
                }
            }
        });

        searchViewJobs.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                listAdapterJobs.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchViewJobs.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listAdapterJobs.getFilter().filter("");
                return false;
            }
        });

        spinnerStatusFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item                     = jobStatusArrayList.get(i).getStatus();
                Log.d("skt", "item key = " + item);
                setAdapterForList(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void initData() {
        floatingActionButtonAddJob.setVisibility(View.GONE);
        getAllJobs();
        setFilteringSpinnerValues();

        textViewLoading.setTextColor(0xff006094);
    }

    @Override
    public void toDoOnDetach() {

    }

    private void getAllJobs() {
        GetAllJobsTask task                     = new GetAllJobsTask(getCurrentContext());
        task.setLoadingMessage("Getting jobs...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o instanceof ArrayList) {
                    //noinspection unchecked
                    onGettingAllJobs((ArrayList<Job>) o);
                }
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not get job list. " + AppServiceErrorHelper.getErrorString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //noinspection unchecked
        task.execute();
    }

    private void onGettingAllJobs(ArrayList<Job> arrayList) {
        SortJobArrayTask task                   = new SortJobArrayTask(getCurrentContext(), arrayList);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o instanceof HashMap) {
                    //noinspection unchecked
                    onSortingAndGroupingArrayList((HashMap<String, ArrayList<Job>>) o);
                }
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Something went wrong",
                        Toast.LENGTH_SHORT).show();
            }
        });
        task.setLoadingMessage("Calculating...");
        //noinspection unchecked
        task.execute();
    }

    private void onSortingAndGroupingArrayList(HashMap<String, ArrayList<Job>> hashMap) {
        hashMapSortedAndGrouped                 = hashMap;
        setAdapterForList(AppConstants.Job.STATUS_ALL);
    }

    private void setAdapterForList(String key) {
        if (hashMapSortedAndGrouped != null) {
            jobsArrayList                       = hashMapSortedAndGrouped.get(key);

            if (jobsArrayList == null) {
                jobsArrayList                   = new ArrayList<>();
            }

            Log.d("skt", "Len of arr = " + jobsArrayList.size());

            listAdapterJobs                     = new SupervisorJobListAdapter(getCurrentContext(), jobsArrayList);
            listViewJobs.setAdapter(listAdapterJobs);
        }
    }

    private void setFilteringSpinnerValues() {
        String[] status                         = {AppConstants.Job.STATUS_ALL,
                AppConstants.Job.STATUS_PENDING,
                AppConstants.Job.STATUS_UNASSIGNED,
                AppConstants.Job.STATUS_IN_PROGRESS,
                AppConstants.Job.STATUS_ASSIGNED,
                AppConstants.Job.STATUS_DONE,
                AppConstants.Job.STATUS_TODAY};

        jobStatusArrayList                      = new ArrayList<>();

        for (String stat : status) {
            JobStatus jobStatus                 = new JobStatus();
            jobStatus.setStatus(stat);
            jobStatusArrayList.add(jobStatus);
        }

        spinnerAdapter                          = new JobStatusSpinnerAdapter(getCurrentContext(), jobStatusArrayList);
        spinnerStatusFilter.setAdapter(spinnerAdapter);
    }

    private void goToSubscriptionAddFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_subscription");
        }
    }
}
