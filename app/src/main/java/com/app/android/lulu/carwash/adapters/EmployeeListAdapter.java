package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.model.Employee;

import java.util.ArrayList;


public class EmployeeListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Employee> employeeArrayList;
    private ArrayList<Employee> oEmployeeArrayList;

    private LayoutInflater inflater;

    public EmployeeListAdapter(Context context, ArrayList<Employee> employeeArrayList) {
        this.employeeArrayList                  = employeeArrayList;
        inflater                                = LayoutInflater.from(context);

        oEmployeeArrayList                      = new ArrayList<>();
        oEmployeeArrayList.addAll(employeeArrayList);
    }

    @Override
    public int getCount() {
        return employeeArrayList != null ? employeeArrayList.size() : 0;
    }

    @Override
    public Employee getItem(int position) {
        return (employeeArrayList != null && employeeArrayList.size() > position)? employeeArrayList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder                              = new ViewHolder();

            convertView                         = inflater.inflate(R.layout.list_item_person, null);
            holder.textViewPerson               = (TextView) convertView.findViewById(R.id.item_person_textview_name);
            convertView.setTag(holder);
        } else {
            holder                              = (ViewHolder) convertView.getTag();
        }

        Employee employee                       = employeeArrayList.get(position);
        holder.textViewPerson.setText(employee.getFullName());
        holder.textViewPerson.setTextColor(0xff676767);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Employee> filteredEmp = new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredEmp.addAll(oEmployeeArrayList);
                } else {
                    for (int i = 0; i < oEmployeeArrayList.size(); i++) {
                        Employee employee       = oEmployeeArrayList.get(i);
                        String customerName     = employee.getName();
                        if (customerName.toLowerCase().contains(constraint.toString())) {
                            filteredEmp.add(employee);
                        }
                    }
                }

                results.count                   = filteredEmp.size();
                results.values                  = filteredEmp;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                employeeArrayList               = (ArrayList<Employee>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        TextView textViewPerson;
    }
}
