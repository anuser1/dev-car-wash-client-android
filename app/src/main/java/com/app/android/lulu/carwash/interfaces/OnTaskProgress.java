package com.app.android.lulu.carwash.interfaces;


public interface OnTaskProgress extends OnTaskResult {
    void onTriedTaskSuccess(Object o);
    void onTriedTaskFailure(Object o);
}
