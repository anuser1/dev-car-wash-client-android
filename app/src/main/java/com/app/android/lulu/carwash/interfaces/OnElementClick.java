package com.app.android.lulu.carwash.interfaces;

/**
 * Created by Santhosh on 12-11-2017.
 */

public interface OnElementClick {
    void onClick(int index);
    void onLongClick(int index);
}
