package com.app.android.lulu.carwash.constants.enums;

public enum RequestMethod {
    GET, POST, PUT, DELETE, MULTI
}
