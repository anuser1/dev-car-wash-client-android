package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.activities.MainActivity;
import com.app.android.lulu.carwash.adapters.SubscriptionListAdapter;
import com.app.android.lulu.carwash.background.AssignEmployeeSubscriptionTask;
import com.app.android.lulu.carwash.background.GetSubscriptionDetailsTask;
import com.app.android.lulu.carwash.background.GetSubscriptionListTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskProgress;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Employee;
import com.app.android.lulu.carwash.model.Subscription;

import java.util.ArrayList;


public class SupervisorSubscriptionListFragment extends CarWashFragmentBaseClass {

    private TextView textViewLoading;
    private TextView textViewTitle;
    private ListView listViewSubscriptions;
    private FloatingActionButton floatingActionButtonAddSubscription;
    private FloatingActionButton floatingActionButtonAssign;
    //private RelativeLayout relativeLayoutLoadingContainer;
    private SearchView searchViewSubscriptions;
    private ImageView imageViewTitle;

    private SubscriptionListAdapter listAdapterSubscriptions;

    private ArrayList<Subscription> subscriptionArrayList;

    private boolean flag_loading                = false;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        listViewSubscriptions                   = (ListView) view.findViewById(R.id.persons_list_persons);
        floatingActionButtonAddSubscription     = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);
        floatingActionButtonAssign              = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_assign);
        //relativeLayoutLoadingContainer          = (RelativeLayout) view.findViewById(R.id.relativeLayout_persons_loading_container);
        textViewLoading                         = (TextView) view.findViewById(R.id.textView_persons_loading);
        searchViewSubscriptions                 = (SearchView) view.findViewById(R.id.searchView_persons_search);
        textViewTitle                           =  (TextView) view.findViewById(R.id.textView_persons_title);
        imageViewTitle                          = (ImageView) view.findViewById(R.id.imageView_persons_title);

        return view;
    }

    @Override
    public void initListeners() {
        floatingActionButtonAddSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSubscriptionAddFragment();
            }
        });

        floatingActionButtonAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAssignSubscriptionClick();
            }
        });

        listViewSubscriptions.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (totalItemCount != 0 && firstVisibleItem + visibleItemCount == (totalItemCount - 5)) {
                    if (!flag_loading) {
                        flag_loading = true;
                        //addItems();
                    }
                }
            }
        });

        listViewSubscriptions.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listAdapterSubscriptions.toggleSelect();
                showAssignSubscriptionButton();

                return true;
            }
        });

        listViewSubscriptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getDetailsOfSubscription(position);
            }
        });

        searchViewSubscriptions.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                listAdapterSubscriptions.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchViewSubscriptions.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listAdapterSubscriptions.getFilter().filter("");
                return false;
            }
        });
    }

    @Override
    public void initData() {
        getAllSubscriptionList();

        textViewLoading.setTextColor(0xff006094);
        textViewTitle.setText(R.string.subscription);
        imageViewTitle.setImageResource(R.mipmap.ic_subscribe);
    }

    @Override
    public void toDoOnDetach() {

    }

    @Override
    public void onTaskResult() {
        super.onTaskResult();

        Employee employee                       = (Employee) getData();

        ArrayList<Subscription> actual          = listAdapterSubscriptions.getActualSubscriptionList();
        if (actual != null) {
            ArrayList<Subscription> selected    = getSelectedItems(actual);
            for (Subscription item : selected) {
                item.setAssignedUserName(employee.getName());
                item.setAssignedUserId(employee.getId());
            }
            onAssigningSubscription(selected);
        }
    }

    private void getAllSubscriptionList() {
        GetSubscriptionListTask task            = new GetSubscriptionListTask(getCurrentContext());
        task.setLoadingMessage("Getting subscription list");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList)
                    //noinspection unchecked
                    populateListViewWithServerData((ArrayList<Subscription>) o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });
        //noinspection unchecked
        task.execute();
    }

    private void populateListViewWithServerData(ArrayList<Subscription> subscriptions) {
        subscriptionArrayList                   = subscriptions;
        listAdapterSubscriptions                = new SubscriptionListAdapter(getCurrentContext(), subscriptionArrayList);
        listViewSubscriptions.setAdapter(listAdapterSubscriptions);
    }

    /*private void addItems() {
        setLoadingVisible(true);

        Handler handler                         = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                addItemsToList();
                onListAddingTaskSuccess();
            }
        }, 5000);
    }

    private void addItemsToList() {
        int startIndex                          = subscriptionArrayList.size();
        int endIndex                            = startIndex + 12;

        for (int i = startIndex; i < endIndex; i++) {
            Subscription subscription           = new Subscription();
            Vehicle vehicle                     = new Vehicle();
            Plot plot                           = new Plot();
            Customer customer                   = new Customer();

            customer.setName("Cust --" + (i + 1));
            customer.setParkingLot("Park lot " + i + ", Near XYZ, Dubai");

            plot.setName("Dubai central mall");
            plot.setId(i % 3);
            customer.setPlotDetails(plot);
            subscription.setPlot(plot);

            vehicle.setMake("Volvo ");
            vehicle.setModel("Model " + (i + "D"));

            customer.setVehicle(vehicle);


            customer.setVehicle(vehicle);
            subscription.setCustomer(customer);

            subscriptionArrayList.add(subscription);
        }
    }

    private void onListAddingTaskSuccess() {
        setLoadingVisible(false);
        flag_loading                            = false;
        int currentPosition                     = listViewSubscriptions.getFirstVisiblePosition();

        listAdapterSubscriptions                = new SubscriptionListAdapter(getCurrentContext(), subscriptionArrayList);
        listViewSubscriptions.setAdapter(listAdapterSubscriptions);

        listViewSubscriptions.setSelection(currentPosition);
    }

    private void setLoadingVisible(boolean visible) {
        if (visible) {
            relativeLayoutLoadingContainer.setVisibility(View.VISIBLE);
        } else {
            relativeLayoutLoadingContainer.setVisibility(View.GONE);
        }
    }*/

    private void showAssignSubscriptionButton() {
        boolean show                            = listAdapterSubscriptions.isShowSelectCheckBox();

        if (show) {
            floatingActionButtonAssign.setVisibility(View.VISIBLE);
        } else {
            floatingActionButtonAssign.setVisibility(View.INVISIBLE);
        }
    }


    private void goToSubscriptionAddFragment() {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            oFIL.onChangingFragment("add_subscription");
        }
    }

    private void onAssignSubscriptionClick() {
        ArrayList<Subscription> actual          = listAdapterSubscriptions.getActualSubscriptionList();
        if (actual != null) {
            ArrayList<Subscription> selected    = getSelectedItems(actual);
            if (selected.size() == 0) {
                showSelectItemsToast();
            } else {
                getEmployeeToAssign(selected);
            }
        } else {
            showSelectItemsToast();
        }
    }

    private ArrayList<Subscription> getSelectedItems(ArrayList<Subscription> subscriptions) {
        ArrayList<Subscription> selectedItems   = new ArrayList<>();
        for(Subscription subscription : subscriptions) {
            if (subscription.isSelected()) {
                selectedItems.add(subscription);
            }
        }

        return selectedItems;
    }

    private void showSelectItemsToast() {
        Toast.makeText(getCurrentContext(), "Select subscriptions to assign", Toast.LENGTH_SHORT).show();
    }

    /**
     *
     * @param subscriptions selected subscriptions
     */
    private void onAssigningSubscription(ArrayList<Subscription> subscriptions) {
        AssignEmployeeSubscriptionTask task     = new AssignEmployeeSubscriptionTask(getCurrentContext(), subscriptions);
        task.setLoadingMessage("Assigning tasks");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                //noinspection unchecked
                if (o != null && o instanceof  ArrayList) {
                    //noinspection unchecked
                    afterAssigningEmployeesUpdateList((ArrayList<Subscription>) o);
                }
            }

            @Override
            public void onError(Object o) {
                showFailureMessage();
            }
        });

        task.setOnTaskProgress(new OnTaskProgress() {
            @Override
            public void onTriedTaskSuccess(Object o) {
                if (o != null && o instanceof  Subscription) {
                    //noinspection unchecked
                    showToast("Assigned subscription");
                }
            }

            @Override
            public void onTriedTaskFailure(Object o) {
                if (o != null && o instanceof Subscription) {
                    onFailureInAssigning((Subscription) o);
                }
            }

            @Override
            public void onSuccess(Object o) {

            }

            @Override
            public void onError(Object o) {

            }
        });

        //noinspection unchecked
        task.execute();

        Log.d("skt", "Going to assign tasks +++ " + subscriptions.size());

    }

    private void getEmployeeToAssign(ArrayList<Subscription> subscriptions) {
        ((MainActivity)getActivity()).setObjectDataToPassBetweenFragments(AppConstants.DataPass.KEY_ASSIGN_SUBSCRIPTION_EMPLOYEE);
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"get_employee_list_for_subscription", subscriptions};
            oFIL.onFragmentInteraction(data);
        }
    }

    private void onFailureInAssigning(Subscription subscription) {
        String message                          = "Could not assign the subscription of "
                                                        + subscription.getCustomer().getName()
                                                        + " with vehicle  "
                                                        + subscription.getCustomer().getVehicle().getModel()
                                                        + " to employee "
                                                        + subscription.getAssignedUserName();

        showToast(message);
    }

    private void showToast(String message) {
        Toast.makeText(getCurrentContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void afterAssigningEmployeesUpdateList(ArrayList<Subscription> subscriptions) {
        ArrayList<Subscription> originalList    = listAdapterSubscriptions.getActualSubscriptionList();
        for (Subscription subscription: subscriptions) {
            for (Subscription item : originalList) {
                if (item.getId() == subscription.getId()) {
                    item.setAssignedUserId(subscription.getAssignedUserId());
                    item.setAssignedUserName(subscription.getAssignedUserName());

                    break;
                }
            }
        }

        subscriptionArrayList                   = new ArrayList<>();
        subscriptionArrayList.addAll(originalList);

        populateListViewWithServerData(subscriptionArrayList);
    }

    private void showFailureMessage() {
        FragmentManager fm                      = ((AppCompatActivity)getCurrentContext()).getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Failed",
                "Could not assign subscriptions to the employee. Try after some time or contact admin");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {

            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void getDetailsOfSubscription(int index) {
        Subscription subscription               = subscriptionArrayList.get(index);
        GetSubscriptionDetailsTask task         = new GetSubscriptionDetailsTask(getCurrentContext(), subscription);
        task.setLoadingMessage("Getting details");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o instanceof Subscription) {
                    Subscription sub            = (Subscription) o;
                    goToSubscriptionDetailsFragment(sub);
                }
            }

            @Override
            public void onError(Object o) {

            }
        });
        //noinspection unchecked
        task.execute();
    }


    private void goToSubscriptionDetailsFragment(Subscription subscription) {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object o                            = new Object[] {"edit_subscription", subscription};
            oFIL.onFragmentInteraction(o);
        }
    }
}
