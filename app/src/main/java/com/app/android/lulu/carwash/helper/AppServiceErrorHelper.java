package com.app.android.lulu.carwash.helper;

import org.json.JSONException;
import org.json.JSONObject;


public class AppServiceErrorHelper {
    private static JSONObject jsonObjectError;
    private static String errorString;

    public static JSONObject getJsonObjectError() {
        return jsonObjectError;
    }

    public static void setJsonObjectError(JSONObject jsonObjectError) {
        AppServiceErrorHelper.jsonObjectError = jsonObjectError;
    }

    public static String getErrorString() {
        if (errorString == null)
            errorString = "";
        return errorString;
    }

    public static void setErrorString(String errorString) {
        AppServiceErrorHelper.errorString = errorString;
    }

    public static void setErrorResponseJson(String errorResponse) {
        try {
            JSONObject jsonObject               = new JSONObject(errorResponse);
            setJsonObjectError(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
