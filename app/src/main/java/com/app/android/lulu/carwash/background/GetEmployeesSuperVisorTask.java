package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.model.Employee;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetEmployeesSuperVisorTask extends CarwashAsyncTaskBaseClass {

    private Context context;

    public GetEmployeesSuperVisorTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        this.context                            = context;
    }

    @Override
    public Object tasksInBackground() {
        ServiceController controller            = new ServiceController();
        ArrayList<Employee> completeList        = null;
        ArrayList<Employee> employeeArrayList   = controller.getEmployees(context);
        ArrayList<Employee> supervisorArrayList = controller.getSupervisors(context);

        if (employeeArrayList != null) {
            completeList                    = new ArrayList<>();
            completeList.addAll(employeeArrayList);
        }

        if (supervisorArrayList != null) {
            if (completeList == null) {
                completeList                    = new ArrayList<>();
            }
            completeList.addAll(supervisorArrayList);
        }

        return completeList;
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        boolean isSuccess                       = o != null && o instanceof ArrayList;
        if (!isSuccess) {
            try {
                JSONObject errorResponseJson    = AppServiceErrorHelper.getJsonObjectError();
                if (errorResponseJson != null) {
                    JSONObject errorJsonObject  = errorResponseJson.getJSONObject("error");

                    if (errorJsonObject != null && errorJsonObject.has("message")) {
                        String errorMessage     = errorJsonObject.getString("message");
                        AppServiceErrorHelper.setErrorString(errorMessage);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
