package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.adapters.EmployeeJobListAdapter;
import com.app.android.lulu.carwash.background.GetEmployeeJobListTask;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.interfaces.OnFragmentInteractionListener;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;
import com.app.android.lulu.carwash.model.Job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class EmployeeJobListFragment extends CarWashFragmentBaseClass {

    private TextView textViewTitle;
    private TextView textViewListTile;
    private ImageView imageViewTitle;
    private ListView listViewJobs;
    private FloatingActionButton floatingActionButtonAdd;

    private ArrayList<Job> jobArrayList;

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_persons, container, false);

        listViewJobs                            = (ListView) view.findViewById(R.id.persons_list_persons);
        textViewTitle                           = (TextView) view.findViewById(R.id.textView_persons_title);
        textViewListTile                        = (TextView) view.findViewById(R.id.textView_persons_list_title);
        imageViewTitle                          = (ImageView) view.findViewById(R.id.imageView_persons_title);
        floatingActionButtonAdd                 = (FloatingActionButton) view.findViewById(R.id.floatingActionButton_persons_add);

        return view;
    }

    @Override
    public void initListeners() {
        listViewJobs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Job job                         = jobArrayList.get(position);
                goToTakeJobFragment(job);
            }
        });
    }

    @Override
    public void initData() {
        getAssignedJobs();

        floatingActionButtonAdd.setVisibility(View.GONE);
        textViewListTile.setVisibility(View.VISIBLE);

        textViewTitle.setText(R.string.jobs_assigned);
        setDateInListTitle();
        imageViewTitle.setImageDrawable(ContextCompat.getDrawable(getCurrentContext(),R.mipmap.ic_job));
    }

    @Override
    public void toDoOnDetach() {

    }

    private void setDateInListTitle() {
        Calendar calendar                       = Calendar.getInstance();
        SimpleDateFormat dateFormat             = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());

        String dateString                       = dateFormat.format(calendar.getTime());

        textViewListTile.setText("Start date : " + dateString);
    }

    private void getAssignedJobs() {
        GetEmployeeJobListTask task             = new GetEmployeeJobListTask(getCurrentContext());
        task.setLoadingMessage("Gettings assigned jobs...");
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                //noinspection unchecked
                if (o != null && o instanceof ArrayList)
                    //noinspection unchecked
                    loadListViewWithJobs((ArrayList<Job>) o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(getCurrentContext(), "Could not save details. "
                        + AppServiceErrorHelper.getErrorString(), Toast.LENGTH_SHORT).show();
            }
        });

        //noinspection unchecked
        task.execute();
    }

    private void loadListViewWithJobs(ArrayList<Job> jobs) {
        jobArrayList                            = jobs;
        EmployeeJobListAdapter adapter          = new EmployeeJobListAdapter(getCurrentContext(), jobArrayList);
        listViewJobs.setAdapter(adapter);
    }

    /*private void populateJobs() {
        ArrayList<Job> jobArrayList             = new ArrayList<>();

        String[] status                         = {AppConstants.Job.STATUS_DONE,
                                                        AppConstants.Job.STATUS_ASSIGNED,
                                                        AppConstants.Job.STATUS_IN_PROGRESS,
                                                        AppConstants.Job.STATUS_UNASSIGNED};

        Random rand                             = new Random();

        for (int i = 0; i < 12; i++) {
            Job job                             = new Job();
            Vehicle vehicle                     = new Vehicle();
            Plot plot                           = new Plot();
            Customer customer                   = new Customer();

            customer.setName("Cust --" + (i + 1));
            customer.setParkingLot("Park lot " + i + ", Near XYZ, Dubai");

            plot.setName("Dubai central mall");
            plot.setId(i % 3);
            customer.setPlotDetails(plot);

            vehicle.setMake("Volvo ");
            vehicle.setModel("Model " + (i + rand.nextInt(200) + "D"));

            job.setStatus(status[rand.nextInt(4)]);

            customer.setVehicle(vehicle);
            job.setCustomer(customer);

            jobArrayList.add(job);
        }

        EmployeeJobListAdapter adapter          = new EmployeeJobListAdapter(getCurrentContext(), jobArrayList);
        listViewJobs.setAdapter(adapter);
    }*/

    private void goToTakeJobFragment(Job job) {
        OnFragmentInteractionListener oFIL      = getOnFragmentInteractionListener();
        if (oFIL != null) {
            Object data                         = new Object[] {"employee_take_job", job};
            oFIL.onFragmentInteraction(data);
        }
    }
}
