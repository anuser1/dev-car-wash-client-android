package com.app.android.lulu.carwash.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.baseclasses.CarWashFragmentBaseClass;

/**
 * Created by Santhosh on 20-09-2017.
 */

public class SuperAdminAddSubscription extends CarWashFragmentBaseClass {

    @Override
    public void init() {

    }

    @Override
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                               = inflater.inflate(R.layout.fragment_subscription_add, container, false);
        return view;
    }

    @Override
    public void initListeners() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void toDoOnDetach() {

    }
}
