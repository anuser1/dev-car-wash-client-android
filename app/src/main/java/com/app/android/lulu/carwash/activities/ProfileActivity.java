package com.app.android.lulu.carwash.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.background.ChangePasswordTask;
import com.app.android.lulu.carwash.fragments.AlertDialogFragment;
import com.app.android.lulu.carwash.helper.AppPrefs;
import com.app.android.lulu.carwash.interfaces.OnDialogAction;
import com.app.android.lulu.carwash.interfaces.OnTaskResult;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private TextView textViewEmpId;//android:id="@+id/textView_profile_emp_id"
    private TextView textViewUsername;
    private TextView textViewEmpName;
    private TextView textViewPhoneNumber;
    private TextView textViewEmail;
    private EditText editTextPassword;
    private ImageView imageViewShowPassword;//imageView_profile_show_password
    private ImageView imageViewEditPassword;
    private Button buttonSave;

    private AppPrefs appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mToolbar                                = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mActionBar                              = getSupportActionBar();
        //noinspection ConstantConditions
        mToolbar.setLogo(R.drawable.actionbar_logo);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);

        initViews();
        initListeners();
        initData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        textViewEmpId                           = (TextView) findViewById(R.id.textView_profile_emp_id);
        textViewUsername                        = (TextView) findViewById(R.id.textView_profile_username);
        textViewEmpName                         = (TextView) findViewById(R.id.textView_profile_full_name);
        textViewPhoneNumber                     = (TextView) findViewById(R.id.textView_profile_phone_number);
        textViewEmail                           = (TextView) findViewById(R.id.textView_profile_email);

        editTextPassword                        = (EditText) findViewById(R.id.editText_profile_password);

        imageViewEditPassword                   = (ImageView) findViewById(R.id.imageView_profile_edit_password);
        imageViewShowPassword                   = (ImageView) findViewById(R.id.imageView_profile_show_password);

        buttonSave                              = (Button) findViewById(R.id.button_profile_save);
    }

    private void initListeners() {
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });

        imageViewShowPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return showOrHidePassword(event);
            }
        });

        imageViewEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enablePasswordField();
            }
        });
    }

    private void initData() {
        appPrefs                                = new AppPrefs(this);

        editTextPassword.setEnabled(false);


        editTextPassword.setText(appPrefs.getPassword());
        textViewUsername.setText(appPrefs.getUsername());
        textViewEmpId.setText(appPrefs.getEmployeeId());
        textViewEmpName.setText(appPrefs.getFullName());
        textViewPhoneNumber.setText(!appPrefs.getPhone().equals("null") ? appPrefs.getPhone() : "");
        textViewEmail.setText(!appPrefs.getEmail().equals("null") ? appPrefs.getEmail() : "");
    }

    private boolean showOrHidePassword(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            return false;
        }

        return true;
    }

    private void enablePasswordField() {
        editTextPassword.setEnabled(true);
        imageViewEditPassword.setVisibility(View.GONE);
    }

    private void onSaveButtonClick() {
        String password                         = editTextPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            editTextPassword.setError("Cannot be empty");
        } else if (password.length() < 6) {
            editTextPassword.setError("Minimum 6 characters");
        } else {
            changePassword(password);
        }
    }

    private void changePassword(String changedPassword) {
        ChangePasswordTask task                 = new ChangePasswordTask(this, changedPassword);
        task.setOnTaskResult(new OnTaskResult() {
            @Override
            public void onSuccess(Object o) {
                if (o != null)
                    showSuccessMessage((String) o);
            }

            @Override
            public void onError(Object o) {
                Toast.makeText(ProfileActivity.this, "Could not update. Please try again", Toast.LENGTH_LONG).show();
            }
        });
        task.setLoadingMessage("Updating details...");
        //noinspection unchecked
        task.execute();
    }

    private void showSuccessMessage(final String password) {
        FragmentManager fm                      = getSupportFragmentManager();
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance(
                "Updated",
                "Password updated successfully.");
        alertDialogFragment.setOnDialogAction(new OnDialogAction() {
            @Override
            public void onOk() {
                onTaskSuccess(password);
            }

            @Override
            public void onYes() {

            }

            @Override
            public void onNo() {

            }

            @Override
            public void onCancel() {

            }
        });
        alertDialogFragment.show(fm, "fragment_alert");
    }

    private void onTaskSuccess(String password) {
        appPrefs.setPassword(password);
        finish();
    }
}
