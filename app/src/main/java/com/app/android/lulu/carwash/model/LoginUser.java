package com.app.android.lulu.carwash.model;


import org.json.JSONException;
import org.json.JSONObject;

public class LoginUser {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        try {
            JSONObject object                   = new JSONObject();
            object.put("usernameOrEmailAddress", getUsername());
            object.put("password", getPassword());
            object.put("tenancyName", "Default");

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
