package com.app.android.lulu.carwash.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Subscription {
    private Customer customer;
    private Plot plot;
    private boolean paymentReceived;
    private String startDate;
    private String endDate;
    private String nextPaymentDate;
    private float numberOfMonths;
    private int washPerWeek;
    private String[] selectedDays;
    private String[] selectedDates;
    private long assignedUserId;
    private long id;
    private boolean isSelected;
    private String assignedUserName;
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Plot getPlot() {
        return plot;
    }

    public void setPlot(Plot plot) {
        this.plot = plot;
    }

    public boolean isPaymentReceived() {
        return paymentReceived;
    }

    public void setPaymentReceived(boolean paymentReceived) {
        this.paymentReceived = paymentReceived;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNextPaymentDate() {
        return nextPaymentDate;
    }

    public void setNextPaymentDate(String nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public float getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(float numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public int getWashPerWeek() {
        return washPerWeek;
    }

    public void setWashPerWeek(int washPerWeek) {
        this.washPerWeek = washPerWeek;
    }

    public String[] getSelectedDays() {
        return selectedDays;
    }

    public void setSelectedDays(String[] selectedDays) {
        this.selectedDays = selectedDays;
    }

    public long getAssignedUserId() {
        return assignedUserId;
    }

    public void setAssignedUserId(long assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    public String getAssignedUserName() {
        return assignedUserName;
    }

    public void setAssignedUserName(String assignedUserName) {
        this.assignedUserName = assignedUserName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String[] getSelectedDates() {
        return selectedDates;
    }

    public void setSelectedDates(String[] selectedDates) {
        this.selectedDates = selectedDates;
    }

    @Override
    public String toString() {
        JSONObject object                       = new JSONObject();

        try {
            object.put("customerId", getCustomer().getId());
            object.put("customerVehicleId", getCustomer().getVehicle().getCustomerVehicleId());
            object.put("plotId", getPlot().getId());
            object.put("noOfWashPerWeek", getWashPerWeek());
            object.put("noOfMonth", getNumberOfMonths());
            object.put("nextPaymentDate", getNextPaymentDate());
            object.put("subscriptionStartDate", getStartDate());
            object.put("subscriptionEndDate", getEndDate());

            if (getSelectedDays() != null) {
                JSONArray jsonArrayDays = new JSONArray();
                for (int i = 0; i < getSelectedDays().length; i++) {
                    jsonArrayDays.put(getSelectedDays()[i]);
                }

                object.put("subscribedDays", jsonArrayDays);
            }

            if (getSelectedDates() != null) {
                JSONArray jsonArrayDates = new JSONArray();
                for (int i = 0; i < getSelectedDates().length; i++) {
                    jsonArrayDates.put(getSelectedDates()[i]);
                }

                object.put("subscribedDateList", jsonArrayDates);
            }


            if (getAssignedUserId() != 0) {
                object.put("assignedUserId", getAssignedUserId());
            }

            if (getId() != 0) {
                object.put("Id", getId());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }
}
