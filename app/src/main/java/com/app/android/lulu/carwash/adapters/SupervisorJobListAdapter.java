package com.app.android.lulu.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.lulu.carwash.R;
import com.app.android.lulu.carwash.constants.AppConstants;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Employee;
import com.app.android.lulu.carwash.model.Job;
import com.app.android.lulu.carwash.model.Vehicle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class SupervisorJobListAdapter extends BaseAdapter implements Filterable{

    private ArrayList<Job> employeeJobs;
    private ArrayList<Job> oEmployeeJobArrayList;

    private Context context;
    private LayoutInflater inflater;

    private final SimpleDateFormat dateFormat;

    public SupervisorJobListAdapter(Context context, ArrayList<Job> supervisorJobArrayList) {
        this.employeeJobs                       = supervisorJobArrayList;
        oEmployeeJobArrayList                   = new ArrayList<>();
        oEmployeeJobArrayList.addAll(employeeJobs);
        this.context                            = context;
        inflater                                = LayoutInflater.from(context);

        dateFormat                              = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
    }

    @Override
    public int getCount() {
        return employeeJobs != null ? employeeJobs.size() : 0;
    }

    @Override
    public Job getItem(int i) {
        return (employeeJobs != null && employeeJobs.get(i) != null) ? employeeJobs.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        final ViewHolder holder;

        if(view == null) {
            holder                              = new ViewHolder();

            view                                = inflater.inflate(R.layout.list_item_assign_job, null);
            holder.customerName                 = (TextView) view.findViewById(R.id.textView_item_assign_job_cust_name);
            holder.vehicleName                  = (TextView) view.findViewById(R.id.textView_item_assign_job_cust_vehicle);
            holder.employeeName                 = (TextView) view.findViewById(R.id.textView_item_assign_job_emp_name);
            holder.status                       = (ImageView) view.findViewById(R.id.imageView_item_assign_job_status);
            holder.statusText                   = (TextView) view.findViewById(R.id.textView_item_assign_job_status);
            holder.delete                       = (ImageView) view.findViewById(R.id.imageView_item_assign_job_delete);
            holder.statusContainer              = (LinearLayout) view.findViewById(R.id.linearLayout_item_assign_job_status);
            holder.flatNum                      = (TextView) view.findViewById(R.id.textView_item_assign_job_flat_num);
            holder.flatNumLabel                 = (TextView) view.findViewById(R.id.textView_item_assign_job_flat_num_label);
            holder.assignedDate                 = (TextView) view.findViewById(R.id.textView_item_assign_job_assigned_date);
            holder.assignedDateLabel            = (TextView) view.findViewById(R.id.textView_item_assign_job_assigned_date_lbl);
            holder.isDelete                     = false;

            view.setTag(holder);
        } else {
            holder                              = (ViewHolder) view.getTag();
        }

        Job job                                 = employeeJobs.get(pos);
        Employee employee                       = job != null ? job.getAssignedEmployee() : null;
        Customer customer                       = job != null ? job.getCustomer() : null;
        Vehicle vehicle                         = customer != null ? customer.getVehicle() : null;

        if (vehicle != null)
            holder.vehicleName.setText(vehicle.getMake() + " " + vehicle.getModel());

        if (customer != null) {
            holder.customerName.setText(customer.getName());
            holder.flatNum.setText(customer.getFlatNumber());
        }

        if (employee != null)
            holder.employeeName.setText(employee.getName());

        if (job != null && job.getAssignedDate() != null) {
            String dateString                   = dateFormat.format(job.getAssignedDate());
            holder.assignedDate.setText(dateString);
        } else {
            holder.assignedDate.setText("NA");
        }

        holder.vehicleName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        holder.customerName.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.employeeName.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.statusText.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.flatNum.setTextColor(ContextCompat.getColor(context, R.color.blue_text));
        holder.flatNumLabel.setTextColor(ContextCompat.getColor(context, R.color.blue_text));
        holder.assignedDate.setTextColor(ContextCompat.getColor(context, R.color.black));
        holder.assignedDateLabel.setTextColor(ContextCompat.getColor(context, R.color.black));

        onLongClickView(holder);

        if (job != null && job.getStatus().equals(AppConstants.Job.STATUS_DONE)) {
            //view.setBackgroundColor(ContextCompat.getColor(context, R.color.green_mint));
            holder.assignedDate.setText(job.getTimeDuration());
            holder.assignedDateLabel.setText(R.string.duration);
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_complete));
            holder.statusText.setText(R.string.done);
        } else if (job != null && job.getStatus().equals(AppConstants.Job.STATUS_PENDING)) {
            //view.setBackgroundColor(ContextCompat.getColor(context, R.color.red_translucent));
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_pending));
            holder.statusText.setText(R.string.pending);
        } else if (job != null && job.getStatus().equals(AppConstants.Job.STATUS_IN_PROGRESS)) {
            //view.setBackgroundColor(ContextCompat.getColor(context, R.color.green_translucent));
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_in_progress));
            holder.statusText.setText(R.string.in_progress);
        } else if (job != null && job.getStatus().equals(AppConstants.Job.STATUS_ASSIGNED)) {
            //view.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_assigned));
            holder.statusText.setText(R.string.assigned);
        } else {
            //view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey));
            holder.status.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_unassigned));
            holder.statusText.setText(R.string.unassigned);
        }

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongClickView(holder);
                invertBoolean(holder);
                return false;
            }
        });

        return view;
    }

    private void onLongClickView(ViewHolder holder) {
        if (holder.isDelete) {
            holder.statusContainer.setVisibility(View.INVISIBLE);
            holder.delete.setVisibility(View.VISIBLE);
        } else {
            holder.statusContainer.setVisibility(View.VISIBLE);
            holder.delete.setVisibility(View.INVISIBLE);
        }
    }

    private void invertBoolean(ViewHolder holder) {
        holder.isDelete                         = !holder.isDelete;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results           = new FilterResults();
                ArrayList<Job> filteredJobs     = new ArrayList<>();

                // perform your search here using the searchConstraint String.
                constraint                      = constraint.toString().toLowerCase();
                if (constraint.length() == 0) {
                    filteredJobs.addAll(oEmployeeJobArrayList);
                } else {
                    for (int i = 0; i < oEmployeeJobArrayList.size(); i++) {
                        Job job                 = oEmployeeJobArrayList.get(i);
                        String model            = job.getCustomer().getVehicle().getModel();
                        if (model.toLowerCase().contains(constraint.toString())) {
                            filteredJobs.add(job);
                        }
                    }
                }

                results.count                   = filteredJobs.size();
                results.values                  = filteredJobs;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                //noinspection unchecked
                employeeJobs                    = (ArrayList<Job>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        TextView vehicleName;
        TextView customerName;
        TextView employeeName;
        ImageView status;
        TextView statusText;
        ImageView delete;
        LinearLayout statusContainer;
        TextView flatNum;
        TextView flatNumLabel;
        TextView assignedDate;
        TextView assignedDateLabel;
        boolean isDelete;
    }
}
