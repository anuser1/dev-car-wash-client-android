package com.app.android.lulu.carwash.constants;

/**
 * Created by Santhosh on 21-09-2017.
 */

public class PreferenceConstants {
    public static final String PREF_NAME        = "pref_cleanz";
    public static final String KEY_TOKEN        = "token";
    public static final String KEY_USERNAME     = "username";
    public static final String KEY_PASSWORD     = "password";
    public static final String KEY_ROLE         = "role";
    public static final String KEY_EMPLOYEE_ID  = "employee_id";
    public static final String KEY_FULL_NAME    = "full_name";
    public static final String KEY_PHONE        = "phone";
    public static final String KEY_EMAIL        = "email";
}
