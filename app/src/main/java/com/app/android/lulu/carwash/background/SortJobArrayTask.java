package com.app.android.lulu.carwash.background;

import android.content.Context;

import com.app.android.lulu.carwash.baseclasses.CarwashAsyncTaskBaseClass;
import com.app.android.lulu.carwash.controllers.AppController;
import com.app.android.lulu.carwash.controllers.ServiceController;
import com.app.android.lulu.carwash.helper.AppServiceErrorHelper;
import com.app.android.lulu.carwash.model.Customer;
import com.app.android.lulu.carwash.model.Job;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SortJobArrayTask extends CarwashAsyncTaskBaseClass {

    private ArrayList<Job> jobArrayList;

    public SortJobArrayTask(Context context, Object... objects) {
        super(context, objects);
    }

    @Override
    public void initTask(Context context, Object... o) {
        //noinspection unchecked
        jobArrayList                            = (ArrayList<Job>) o[0];
    }

    @Override
    public Object tasksInBackground() {
        return AppController.sortAndGroupJobsArrayList(jobArrayList);
    }

    @Override
    public boolean onTaskCompletion(Object o) {
        return o != null && o instanceof HashMap;
    }
}
